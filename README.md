# Atoperural

Proyecto fin de master para la creación de un portal de reservas de casas rurales.

Autores: Sergio García González, Jose Luis Pastor Rodríguez, Carlos Godoy Trapero, Daniel Crespo Ramírez

# Repositorio de código


https://bitbucket.org/lastmaster/toprural


```bash
git clone git@bitbucket.org:lastmaster/toprural.git
```

# Crear imagenes
## Imagen frontend web
```bash
docker build -t jpastorr/atoperural:latest . -f docker/Dockerfile.atoperural
```
## Imagen Mysql
```bash
docker build -t jpastorr/atoperural-db:latest docker -f docker/Dockerfile.mysql
```
## Nota
Necesitamos registro docker propio para atoperural, usando de momento el mío
# Lanzar chart y ver en navegador
```bash
helm install helm/atoperural -n atoperural
sudo ./publish
firefox https://www.atoperural.es &
```

# Test

```Bash
$ mvn test -Dspring.profiles.active=prod
```

```Bash
$ mvn test -Dspring.profiles.active=dev
```

e2e

```bash
mvn -f Atoperural/pom.xml clean; docker build -t jpastorr/atoperural:latest . -f docker/Dockerfile.atoperural; helm delete --purge atoperural; sleep 5; helm install helm/atoperural --name atoperural; watch kubectl get pods

mvn verify -Dskip.surefire.tests -Dspring.profiles.active=prod
```

UT y CT:

```bash
mvn clean test -Dspring.profiles.active=dev


mvn clean test '-Dtest=*.componenttest.*' -Dspring.profiles.active=dev -f Atoperural/pom.xml
```




## Circle CI
https://circleci.com/bb/lastmaster/toprural/tree/atoperural
