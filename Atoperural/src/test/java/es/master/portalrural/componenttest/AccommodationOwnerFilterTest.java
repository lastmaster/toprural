package es.master.portalrural.componenttest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.testcontainers.Testcontainers;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testcontainers.containers.Network;

import com.github.javafaker.Faker;

import es.master.portalrural.infrastructure.Application;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class AccommodationOwnerFilterTest {

	private static WebDriver driver;
	private Actions actionsGlobal;
	
	Faker faker = new Faker(new Locale("es"));
	
	// Owner variables
    private String name = faker.name().fullName(); // Miss Samanta Schmidt
	
	private String emailValid = faker.bothify("????##@gmail.com");
    private String emailInvalid = faker.bothify("????##gmail.com");
    
    private String password = "dfasa987*";
    
    private String phoneValid = faker.numerify("#########");
    private String phoneInvalid = faker.bothify("???#?##"); 
    
    private String description = "De pueblo y amante de la naturaleza";
	
    private String dniValid = "52415200V";
    private String dniInvalid = "52as4152f0fa0";
    private String dniInvalidLetter = "52415200X";
	
    private String streetAddress = faker.address().streetAddress(); // 60018 Sawayn Brooks Suite 449
	
	private String subscription = "activa";
	private String startDate = "01-01-2019";
	private String endDate = "31-12-2030";
	private String servicePlan = "silver";
	
    private String nameOwner = faker.name().fullName();// Miss Samanta Schmidt    
    private String descriptionOwner = "De pueblo y amante de la naturaleza";	
    private String streetAddressOwner = faker.address().streetAddress(); // 60018 Sawayn Brooks Suite 449
	
    // Accommodation variables
    private String nameAccommodation = faker.company().name();
	private String numberTourism = faker.company().buzzword();
	private int kindAccommodation = Integer.parseInt(faker.regexify("[0-9]"));
	private String web = faker.company().url();
	private String facebook = faker.internet().domainName();
	private String twitter = faker.internet().domainName();
	private String instagram = faker.internet().domainName();
	private String youtube = faker.internet().domainName();

	private int province = Integer.parseInt(faker.regexify("[0-9]"));
	private String village = faker.address().cityName();
	private String address = faker.address().toString();
	private String postalCode = faker.address().zipCode();
	private String country = faker.address().country();
	private String latitude = faker.address().latitude();
	private String longitude = faker.address().longitude();
	
	private String mobilePhone = faker.numerify("#########"); //faker.phoneNumber().cellPhone(); //This one add . between numbers
	private String landline = faker.numerify("#########"); //faker.phoneNumber().phoneNumber(); //This one add . between numbers

	private String people = String.valueOf(faker.number().numberBetween(1, 15));
	private String numberRooms = String.valueOf(faker.number().numberBetween(1, 15));

	private String shortDescription = faker.company().catchPhrase();
	private String longDescription = faker.company().buzzword();
	private String activities = faker.company().profession() + " " + faker.company().industry();
	
	private String weekendLowSeasonPrice = String.valueOf(faker.number().numberBetween(100, 1000));
	private String weekendMediumSeasonPrice = String.valueOf(faker.number().numberBetween(100, 1000));
	private String weekendHighSeasonPrice = String.valueOf(faker.number().numberBetween(100, 1000));
	
	private String weekLowSeasonPrice = String.valueOf(faker.number().numberBetween(1000, 3000));
	private String weekMediumSeasonPrice = String.valueOf(faker.number().numberBetween(1000, 3000));
	private String weekHighSeasonPrice = String.valueOf(faker.number().numberBetween(1000, 3000));
	
	private String characteristicPrice = String.valueOf(faker.number().numberBetween(20, 99));
	
    //@Rule
    public static BrowserWebDriverContainer chrome;
    
    private static WebAppTopRural webAppTopRural;
	
	
	@BeforeClass
	public static void setupClass() {
	    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, false);
        capabilities.setCapability(CapabilityType.SUPPORTS_NETWORK_CONNECTION, true);
        
        chrome = (BrowserWebDriverContainer) new BrowserWebDriverContainer()
                    .withCapabilities(capabilities)
                    .withNetwork(Network.SHARED);

        Testcontainers.exposeHostPorts(8081);

        Application.start();
        chrome.start();
	    webAppTopRural = new WebAppTopRural(chrome.getWebDriver());
        driver = webAppTopRural.getDriver();
	}
	
	@AfterClass
	public static void teardownClass() {
		if (driver != null) {
            driver.quit();
        }
        chrome.stop();
		Application.stop();
	}
	
	@Before
	public void setupTest() {
        /*chrome.start();
	    webAppTopRural = new WebAppTopRural(chrome.getWebDriver());
        driver = webAppTopRural.getDriver();*/
        webAppTopRural.setHomeWebDriver();
        actionsGlobal = new Actions(driver);
	}
	
	@After
	public void quitDriver() throws InterruptedException {
        /*if (driver != null
                && webAppTopRural != null) {
            webAppTopRural.setHomeWebDriver();
            depopulateOwners();
            driver.quit();
        }
        chrome.stop();*/
		webAppTopRural.setHomeWebDriver();
		depopulateOwners();
	}
	
	public void setupAddAccommodation() throws InterruptedException{
		addOwnerInfo();
		gotoAddAccommodationPage();
		addAccommodationInfo(true);
	}
	
	public void setupEditAccommodation() throws InterruptedException{
		addOwnerInfo();
		gotoAddAccommodationPage();
		addAccommodationInfo(true);
		saveAccommodation();
		editAccommodation();
		addAccommodationInfo(false);
	}
	
	public void setupDeleteAccommodation() throws InterruptedException{
		addOwnerInfo();
		gotoAddAccommodationPage();
		addAccommodationInfo(true);
		saveAccommodation();
		editAccommodation();
	}
	
	public void buttonSearchClick() throws InterruptedException {
	    Thread.sleep(2000);
	    
	    WebElement searchButton = driver.findElement(By.id("buttonSearch"));
	    assertThat(searchButton.isEnabled());
	    searchButton.click();
	    
	    Thread.sleep(3000);
	}
	
	public void depopulateAllOwnersAndAccommodations(int numberOwnersAndAccommodations) throws InterruptedException {
        webAppTopRural.setHomeWebDriver();
		
		for (int seed=1; seed<numberOwnersAndAccommodations+1; seed++) 
		{
			gotoEditOwnerPage(seed);
			deleteOwner();
		}
	}
	
	public void populateOwnerWithOneAccommodation(int numberOwnersAndAccommodations) throws InterruptedException {
		for (int seed=1; seed<numberOwnersAndAccommodations+1; seed++)
		{
			addOwnerAndAccommodation(seed);
		}
	}
	
	public void addOwnerAndAccommodation(int seed) throws InterruptedException {
		addOwnerInfo(seed);
		gotoAddAccommodationPage();
		addAccommodationInfo(seed);
		gotoHomePage();
	}	
	
	public void gotoOwnerPage() throws InterruptedException {
	    
		Thread.sleep(2000);
		List<WebElement> anchors = driver.findElements(By.tagName("a")); 
        Iterator<WebElement> i = anchors.iterator(); 
        String href = "";
        while(i.hasNext()) { 
                WebElement anchor = i.next(); 
                href = anchor.getAttribute("href"); 
                if (href.contains("propietario") && !href.contains("nuevo") && !href.contains("borrar"))
                { 
                	System.out.println("El que busco "+ href);
                	break;
                }
        } 
        assertTrue(!href.equals(""));
        
        //System.out.println(href.lastIndexOf("/"));
        String propietario = href.substring(href.lastIndexOf("/")+1);
        System.out.println("propietario vale "+ propietario);

	    WebElement elementGoOwner = driver.findElement(By.xpath("//a[@href='/propietario/"+ propietario +"']"));
	    Thread.sleep(2000);
	    actionsGlobal.moveToElement(elementGoOwner).click().build().perform();
	    Thread.sleep(2000);
	    
	}
	
	public void addOwnerInfo(int seed) throws InterruptedException {
		String nameOwnerSeed = nameOwner+String.valueOf(seed);
		    
		Thread.sleep(2000);
	    WebElement element = driver.findElement(By.id("addOwnerButton"));
	    actionsGlobal.moveToElement(element).click().build().perform();
		Thread.sleep(2000);

	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.sendKeys(nameOwnerSeed);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.sendKeys(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.sendKeys(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.sendKeys(descriptionOwner);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.sendKeys(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.sendKeys(streetAddressOwner);
	    
	    WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    gotoOwnerPage();
	}
	
	public void gotoHomePage() {
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();
	   
        webAppTopRural.setHomeWebDriver();
	}

	
	public void addAccommodationInfo(int seed) {
		String nameAccommodationSeed = nameAccommodation+String.valueOf(seed);
		
	    WebElement name = driver.findElement(By.id("name"));
	    name.sendKeys(nameAccommodationSeed);
	    
	    WebElement numbertourism = driver.findElement(By.id("numbertourism"));
	    numbertourism.sendKeys(numberTourism);
	    
	    Select kindaccommodation = new Select(driver.findElement(By.id("kindaccommodation")));
	    kindaccommodation.selectByIndex(kindAccommodation);
	    
	    WebElement webpage = driver.findElement(By.id("web"));
	    webpage.sendKeys(web);
	    
	    WebElement facebookpage = driver.findElement(By.id("facebook"));
	    facebookpage.sendKeys(facebook);
	    
	    WebElement twitterpage = driver.findElement(By.id("twitter"));
	    twitterpage.sendKeys(twitter);
	    
	    WebElement instagrampage = driver.findElement(By.id("instagram"));
	    instagrampage.sendKeys(instagram);
	    
	    WebElement youtubepage = driver.findElement(By.id("youtube"));
	    youtubepage.sendKeys(youtube);
	    
	    Select provinceAccommodation = new Select(driver.findElement(By.id("province")));
	    int provinceSeed = province+seed;
	    System.out.println("Seed+province" +seed + " " + provinceSeed);
	    provinceAccommodation.selectByIndex(provinceSeed);
	    
	    WebElement villageAccommodation = driver.findElement(By.id("village"));
	    villageAccommodation.sendKeys(village);
	    
	    WebElement addressAccommodation = driver.findElement(By.id("address"));
	    addressAccommodation.sendKeys(address);
	    
	    WebElement postalcode = driver.findElement(By.id("postalcode"));
	    postalcode.sendKeys(postalCode);
	    
	    WebElement countryAccommodation = driver.findElement(By.id("country"));
	    countryAccommodation.sendKeys(country);
	    
	    WebElement latitudeAccommodation = driver.findElement(By.id("latitude"));
	    latitudeAccommodation.sendKeys(latitude);
	    
	    WebElement longitudeAccommodation = driver.findElement(By.id("longitude"));
	    longitudeAccommodation.sendKeys(longitude);
	    
	    WebElement mobilephone = driver.findElement(By.id("mobilephone"));
	    mobilephone.sendKeys(mobilePhone);
	    
	    WebElement landlineAccommodation = driver.findElement(By.id("landline"));
	    landlineAccommodation.sendKeys(landline);
	    
	    Select peopleAccommodation = new Select(driver.findElement(By.id("people")));
	    int peopleSeedInteger = Integer.parseInt(people)+seed;
	    String peopleSeed = String.valueOf(peopleSeedInteger);
	    System.out.println(peopleSeed);
	    peopleAccommodation.selectByValue(peopleSeed);
	    
	    WebElement numberrooms = driver.findElement(By.id("numberrooms"));
	    numberrooms.sendKeys(numberRooms);
	    
	    WebElement shortdescription = driver.findElement(By.id("shortdescription"));
	    shortdescription.sendKeys(shortDescription);
	    
	    WebElement longdescription = driver.findElement(By.id("longdescription"));
	    longdescription.sendKeys(longDescription);
	        
	}
	
	public void depopulateOwners() throws InterruptedException {
		boolean anyOwner = gotoEditOwnerPage();
		
		while(anyOwner){			
		    deleteOwner();
		    anyOwner = gotoEditOwnerPage();
		}
	}
	
	public void gotoEditOwnerPage(int seed) throws InterruptedException {	
	    //find all anchor tags in the page
		Thread.sleep(2000);
		String nameOwnerSeed = nameOwner+String.valueOf(seed);
		WebElement ownerButton = driver.findElement(By.xpath("//a[contains(text(),'Propietarios')]"));
		ownerButton.click();
		Thread.sleep(2000);
		
		List<WebElement> refList = driver.findElements(By.xpath("//a[contains(text(),'"+nameOwnerSeed+"')]"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) 
        {      
            if (we.getText().equals(nameOwnerSeed)){
                we.click();
                return;
            }   
        }  
	}
	
	public boolean gotoEditOwnerPage() throws InterruptedException {
		boolean anyOwner = false;
	    //find all anchor tags in the page
		Thread.sleep(2000);
		WebElement ownerButton = driver.findElement(By.xpath("//a[contains(text(),'Propietarios')]"));
		ownerButton.click();
		Thread.sleep(2000);
		List<WebElement> refList = driver.findElements(By.xpath("//a[contains(text(),'"+nameOwner+"')]"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) 
        {      
            if (we.getText().equals(nameOwner)){
                we.click();
                anyOwner = true;
                break;
            }   
        }  
        return anyOwner;
	}
	
	public void deleteOwner() throws InterruptedException {
		Thread.sleep(2000);
	    WebElement deleteButton = driver.findElement(By.id("boton-borrar"));
	    assertThat(deleteButton.isEnabled());
	    deleteButton.click();
	    Thread.sleep(2000);
	    String deleteAlert = driver.switchTo().alert().getText();

		assertThat(deleteAlert).isEqualTo("¿deseas borrar el propietario? Clicka en Ok o Cancel");   

		driver.switchTo().alert().accept();
		
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/propietario/+\\d+/borrar$"));
	    Thread.sleep(2000);
	    
	    WebElement elementGoHome = driver.findElement(By.xpath("//a[@href='/']"));
	    Thread.sleep(2000);
	    actionsGlobal.moveToElement(elementGoHome).click().build().perform();
	    Thread.sleep(2000);
	}	
	
	public void addPictures() {
		
		WebElement bookedDay = driver.findElement(By.className("calendar-day-2019-06-20"));
		System.out.println(bookedDay.toString());
	}
	
	public void addBookedDates() {
		
		WebElement bookedDay = driver.findElement(By.className("calendar-day-2019-06-20"));
		System.out.println(bookedDay.toString());
	}
	
	public void addCharacteristicsInfo() {
		driver.findElement(By.name("features[6].checked")).click();
		
		WebElement weekendLowSeasonPriceAccommodation = driver.findElement(By.name("features[6].price"));
		weekendLowSeasonPriceAccommodation.sendKeys(weekendLowSeasonPrice);
		
		driver.findElement(By.name("features[7].checked")).click();
		
		WebElement weekendMediumSeasonPriceAccommodation = driver.findElement(By.name("features[7].price"));
		weekendMediumSeasonPriceAccommodation.sendKeys(weekendMediumSeasonPrice);
		
		driver.findElement(By.name("features[1].checked")).click();
		
		WebElement characteristicPriceAccommodation = driver.findElement(By.name("features[1].price"));
		characteristicPriceAccommodation.sendKeys(characteristicPrice);
	}
	
	public void editAccommodation() throws InterruptedException {
		//find all anchor tags in the page
	    List<WebElement> refList = driver.findElements(By.tagName("a"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) {
        	
            if (we.getText().equals(nameAccommodation))
            {
            	assertEquals(we.getText(), nameAccommodation);
        		Thread.sleep(2000);
        	    actionsGlobal.moveToElement(we).click().build().perform();
        		Thread.sleep(2000);
            	//we.click();
        		WebElement updateButton = driver.findElement(By.id("actualizaralojamiento"));
        	    assertThat(updateButton.isEnabled());
        	    updateButton.click();
            	return;
            }
        }
	}
	public void saveAccommodation() throws InterruptedException {
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    gotoOwnerPage();
	}
	
	public void addAccommodationInfo(boolean newInfo) throws InterruptedException {
		
	    
	    WebElement name = driver.findElement(By.id("name"));
	    if (!newInfo)
	    	name.clear();
	    name.sendKeys(nameAccommodation);
	    
	    WebElement numbertourism = driver.findElement(By.id("numbertourism"));
	    if (!newInfo)
	    	numbertourism.clear();
	    numbertourism.sendKeys(numberTourism);
	    
	    
	    Select peopleAccommodation = new Select(driver.findElement(By.id("people")));
	    peopleAccommodation.selectByValue(people);
	    
	    //WebElement kindaccommodationElement = driver.findElement(By.id("kindaccommodation"));
	    //WebElement zelect = kindaccommodationElement.findElement(By.className("zelected"));
	    //zelect.click();
	    Select kindaccommodation = new Select(driver.findElement(By.id("kindaccommodation")));
	    kindaccommodation.selectByIndex(kindAccommodation);
	    
	    WebElement webpage = driver.findElement(By.id("web"));
	    if (!newInfo)
	    	webpage.clear();
	    webpage.sendKeys(web);
	    
	    WebElement facebookpage = driver.findElement(By.id("facebook"));
	    if (!newInfo)
	    	facebookpage.clear();
	    facebookpage.sendKeys(facebook);
	    
	    WebElement twitterpage = driver.findElement(By.id("twitter"));
	    if (!newInfo)
	    	twitterpage.clear();
	    twitterpage.sendKeys(twitter);
	    
	    WebElement instagrampage = driver.findElement(By.id("instagram"));
	    if (!newInfo)
	    	instagrampage.clear();
	    instagrampage.sendKeys(instagram);
	    
	    WebElement youtubepage = driver.findElement(By.id("youtube"));
	    if (!newInfo)
	    	youtubepage.clear();
	    youtubepage.sendKeys(youtube);
	    
	    Select provinceAccommodation = new Select(driver.findElement(By.id("province")));
	    provinceAccommodation.selectByIndex(province);
	    
	    WebElement villageAccommodation = driver.findElement(By.id("village"));
	    if (!newInfo)
	    	villageAccommodation.clear();
	    villageAccommodation.sendKeys(village);
	    
	    WebElement addressAccommodation = driver.findElement(By.id("address"));
	    if (!newInfo)
	    	addressAccommodation.clear();
	    addressAccommodation.sendKeys(address);
	    
	    WebElement postalcode = driver.findElement(By.id("postalcode"));
	    if (!newInfo)
	    	postalcode.clear();
	    postalcode.sendKeys(postalCode);
	    
	    WebElement countryAccommodation = driver.findElement(By.id("country"));
	    if (!newInfo)
	    	countryAccommodation.clear();
	    countryAccommodation.sendKeys(country);
	    
	    WebElement latitudeAccommodation = driver.findElement(By.id("latitude"));
	    if (!newInfo)
	    	latitudeAccommodation.clear();
	    latitudeAccommodation.sendKeys(latitude);
	    
	    WebElement longitudeAccommodation = driver.findElement(By.id("longitude"));
	    if (!newInfo)
	    	longitudeAccommodation.clear();
	    longitudeAccommodation.sendKeys(longitude);
	    
	    WebElement mobilephone = driver.findElement(By.id("mobilephone"));
	    if (!newInfo)
	    	mobilephone.clear();
	    mobilephone.sendKeys(mobilePhone);
	    
	    WebElement landlineAccommodation = driver.findElement(By.id("landline"));
	    if (!newInfo)
	    	landlineAccommodation.clear();
	    landlineAccommodation.sendKeys(landline);
	    
	    WebElement numberrooms = driver.findElement(By.id("numberrooms"));
	    if (!newInfo)
	    	numberrooms.clear();
	    numberrooms.sendKeys(numberRooms);
	    
	    WebElement shortdescription = driver.findElement(By.id("shortdescription"));
	    if (!newInfo)
	    	shortdescription.clear();
	    shortdescription.sendKeys(shortDescription);
	    
	    WebElement longdescription = driver.findElement(By.id("longdescription"));
	    if (!newInfo)
	    	longdescription.clear();
	    longdescription.sendKeys(longDescription);
	        
	}
	
	public void gotoAddAccommodationPage() throws InterruptedException {
		Thread.sleep(1000);
		WebElement addAccommodationButton = driver.findElement(By.id("addaccommodation"));
	    assertThat(addAccommodationButton.isEnabled());
	    actionsGlobal.moveToElement(addAccommodationButton).click().build().perform();
	    Thread.sleep(1000);
	    
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/nuevo_alojamiento\\?propietario=+\\d+$"));	
	}
	
	public void addOwnerInfo() throws InterruptedException {

		Thread.sleep(2000);
	    WebElement addOwnerButton = driver.findElement(By.id("addOwnerButton"));
	    actionsGlobal.moveToElement(addOwnerButton).click().build().perform();
		Thread.sleep(2000);
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.sendKeys(nameOwner);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.sendKeys(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.sendKeys(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.sendKeys(descriptionOwner);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.sendKeys(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.sendKeys(streetAddressOwner);
	    
	    WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    gotoOwnerPage(); 
	}
	
	//Add Owner
	@Test
	public void givenHomeWhenAddOwnerIsLaunchedThenEmptyFormIsRetrieved() throws InterruptedException {

		Thread.sleep(2000);
	    WebElement element = driver.findElement(By.id("addOwnerButton"));
	    actionsGlobal.moveToElement(element).click().build().perform();
		Thread.sleep(2000);
	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo("");
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo("");
	    assertThat(driver.findElement(By.id("contrasena")).getAttribute("value"))
           .isEqualTo("");
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo("");
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo("");
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo("");
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
        	.isEqualTo("");
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	}
	
	@Test
	public void givenHomeWhenAddOwnerIsLaunchedAndFillAllFieldsThenSaveButtonIsActiveAndAddOwnerSuccessfulFormIsRetrieved() throws InterruptedException {
	
		Thread.sleep(2000);
	    WebElement element = driver.findElement(By.id("addOwnerButton"));
	    actionsGlobal.moveToElement(element).click().build().perform();
		Thread.sleep(2000);
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.sendKeys(emailValid);
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.sendKeys(phoneValid);
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.sendKeys(dniValid);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.sendKeys(streetAddress);
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();
	    
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/propietario/nuevo$"));
	}
	
	@Test
	public void givenHomeWhenAddOwnerIsLaunchedAndNotFillAllFieldsThenSaveButtonIsNotActive() throws InterruptedException {
	
		Thread.sleep(2000);
	    WebElement element = driver.findElement(By.id("addOwnerButton"));
	    actionsGlobal.moveToElement(element).click().build().perform();
		Thread.sleep(2000);
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    /*
	     * Email is not filled 
	    WebElement email = driver.findElement(By.id("email"));
	    email.sendKeys(emailValid);
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo(emailValid);*/
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.sendKeys(phoneValid);
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.sendKeys(dniValid);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.sendKeys(streetAddress);
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(!sendButton.isEnabled());   
	}
	
	@Test
	public void givenHomeWhenAddOwnerIsLaunchedAndFillBadEmailFieldThenValidationMessageIsRetrieved() throws InterruptedException {
	
		Thread.sleep(2000);
	    WebElement element = driver.findElement(By.id("addOwnerButton"));
	    actionsGlobal.moveToElement(element).click().build().perform();
		Thread.sleep(2000);
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.sendKeys(emailInvalid);
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo(emailInvalid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.sendKeys(phoneValid);
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.sendKeys(dniValid);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.sendKeys(streetAddress);
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();
	    
	    String validationMessage = "Please include an '@' in the email address. '"+ emailInvalid+"' is missing an '@'.";
	    assertThat(validationMessage).isEqualTo(email.getAttribute("validationMessage")); 
	    
	    /*String emailAlert = driver.switchTo().alert().getText();

		assertThat(emailAlert).isEqualTo("Contenido del email no es un correo electrónico válido.");
		driver.switchTo().alert().accept();*/
	}
	
	@Test
	public void givenHomeWhenAddOwnerIsLaunchedAndFillBadPhoneFieldThenAlertIsRetrieved() throws InterruptedException {
	
		Thread.sleep(2000);
	    WebElement element = driver.findElement(By.id("addOwnerButton"));
	    actionsGlobal.moveToElement(element).click().build().perform();
		Thread.sleep(2000);
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.sendKeys(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.sendKeys(phoneInvalid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.sendKeys(dniValid);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.sendKeys(streetAddress);
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();
	    
	    String phoneAlert = driver.switchTo().alert().getText();

		assertThat(phoneAlert).isEqualTo("El teléfono debe ser un número");   
		driver.switchTo().alert().accept();
	}
	
	@Test
	public void givenHomeWhenAddOwnerIsLaunchedAndFillBadDniFieldThenAlertIsRetrieved() throws InterruptedException {
	
		Thread.sleep(2000);
	    WebElement element = driver.findElement(By.id("addOwnerButton"));
	    actionsGlobal.moveToElement(element).click().build().perform();
		Thread.sleep(2000);
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.sendKeys(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.sendKeys(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.sendKeys(dniInvalid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.sendKeys(streetAddress);
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();
	    
	    String dniAlert = driver.switchTo().alert().getText();
		assertThat(dniAlert).isEqualTo("Dni erroneo, formato no válido"); 
		driver.switchTo().alert().accept();
	}
	
	@Test
	public void givenHomeWhenAddOwnerIsLaunchedAndFillBadDniLetterFieldThenAlertIsRetrieved() throws InterruptedException {
	
		Thread.sleep(2000);
	    WebElement element = driver.findElement(By.id("addOwnerButton"));
	    actionsGlobal.moveToElement(element).click().build().perform();
		Thread.sleep(2000);
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.sendKeys(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.sendKeys(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.sendKeys(dniInvalidLetter);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.sendKeys(streetAddress);
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();
	    
	    String dniAlert = driver.switchTo().alert().getText();
		assertThat(dniAlert).isEqualTo("Dni erroneo, la letra del NIF no se corresponde"); 
		driver.switchTo().alert().accept();
	}
	// Edit Owner
	
	@Test
	public void givenOwnerWhenEditIsLaunchedThenSuccessfulEditPageIsRetrieved() throws InterruptedException {   
		addOwnerInfo();
		
		Thread.sleep(2000);
		
	    WebElement updateButton = driver.findElement(By.id("actualizarpropietario"));
	    assertThat(updateButton.isEnabled());
	    updateButton.click();
	    
	    WebElement saveButton = driver.findElement(By.id("guardar"));
	    assertThat(saveButton.isEnabled());
	    saveButton.click();
	    
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/propietario/+\\d+/actualizar$"));	
	}
	
	@Test
	public void givenEditOwnerWhenAddAccommodationIsLaunchedThenSuccessfulNewAccommoddationPageIsRetrieved() throws InterruptedException {   
		addOwnerInfo();
		Thread.sleep(2000);
	    WebElement addAccommodationButton = driver.findElement(By.id("addaccommodation"));
	    assertThat(addAccommodationButton.isEnabled());
	    addAccommodationButton.click();
	    
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/nuevo_alojamiento\\?propietario=+\\d+$"));	

	}
	
	@Test
	public void givenEditOwnerWhenFillAllFieldsThenSaveButtonIsActiveAndAddOwnerSuccessfulFormIsRetrieved() throws InterruptedException {
		addOwnerInfo();
		Thread.sleep(2000);
	    WebElement updateButton = driver.findElement(By.id("actualizarpropietario"));
	    assertThat(updateButton.isEnabled());
	    updateButton.click();
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.clear();
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.clear();
	    email.sendKeys(emailValid);
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.clear();
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.clear();
	    telefono.sendKeys(phoneValid);
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.clear();
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.clear();
	    dni.sendKeys(dniValid);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.clear();
	    direccion.sendKeys(streetAddress);
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement saveButton = driver.findElement(By.id("guardar"));
	    assertThat(saveButton.isEnabled());
	    saveButton.click();
	    
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/propietario/+\\d+/actualizar$"));	   
	}
	
	@Test
	public void givenEditOwnerWhenNotFillAllFieldsThenSaveButtonIsNotActive() throws InterruptedException {
		addOwnerInfo();
		Thread.sleep(2000);
	    WebElement updateButton = driver.findElement(By.id("actualizarpropietario"));
	    assertThat(updateButton.isEnabled());
	    updateButton.click();
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.clear();	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo("");
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.clear();
	    email.sendKeys(emailValid);
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.clear();
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.clear();
	    telefono.sendKeys(phoneValid);
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.clear();
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.clear();
	    dni.sendKeys(dniValid);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.clear();
	    direccion.sendKeys(streetAddress);
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement saveButton = driver.findElement(By.id("guardar"));
	    assertThat(!saveButton.isEnabled());
	}
	
	@Test
	public void givenEditOwnerWhenFillBadEmailFieldThenValidationMessageIsRetrieved() throws InterruptedException {
		addOwnerInfo();
		Thread.sleep(2000);
	    WebElement updateButton = driver.findElement(By.id("actualizarpropietario"));
	    assertThat(updateButton.isEnabled());
	    updateButton.click();
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.clear();
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.clear();
	    email.sendKeys(emailInvalid);
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo(emailInvalid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.clear();
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.clear();
	    telefono.sendKeys(phoneValid);
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.clear();
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.clear();
	    dni.sendKeys(dniValid);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.clear();
	    direccion.sendKeys(streetAddress);
	    
	    assertThat(driver.findElement(By.id("direccion")).getAttribute("value"))
           .isEqualTo(streetAddress);
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement saveButton = driver.findElement(By.id("guardar"));
	    assertThat(saveButton.isEnabled());
	    saveButton.click();
	    
	    String validationMessage = "Please include an '@' in the email address. '"+ emailInvalid+"' is missing an '@'.";
	    assertThat(validationMessage).isEqualTo(email.getAttribute("validationMessage")); 
	}
	
	@Test
	public void givenEditOwnerWhenFillBadPhoneFieldThenAlertIsRetrieved() throws InterruptedException {
		addOwnerInfo();
		Thread.sleep(2000);
	    WebElement updateButton = driver.findElement(By.id("actualizarpropietario"));
	    assertThat(updateButton.isEnabled());
	    updateButton.click();
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.clear();
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.clear();
	    email.sendKeys(emailValid);
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.clear();
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.clear();
	    telefono.sendKeys(phoneInvalid);
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo(phoneInvalid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.clear();
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.clear();
	    dni.sendKeys(dniValid);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniValid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.clear();
	    direccion.sendKeys(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement saveButton = driver.findElement(By.id("guardar"));
	    assertThat(saveButton.isEnabled());
	    saveButton.click();
	    
	    String phoneAlert = driver.switchTo().alert().getText();

		assertThat(phoneAlert).isEqualTo("El teléfono debe ser un número");  
		driver.switchTo().alert().accept();
	}
	
	@Test
	public void givenEditOwnerWhenFillBadDniFieldThenAlertIsRetrieved() throws InterruptedException {
		addOwnerInfo();
		Thread.sleep(2000);
	    WebElement updateButton = driver.findElement(By.id("actualizarpropietario"));
	    assertThat(updateButton.isEnabled());
	    Thread.sleep(500);
	    updateButton.click();
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.clear();
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.clear();
	    email.sendKeys(emailValid);
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.clear();
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.clear();
	    telefono.sendKeys(phoneValid);
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.clear();
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.clear();
	    dni.sendKeys(dniInvalid);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniInvalid);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.clear();
	    direccion.sendKeys(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement saveButton = driver.findElement(By.id("guardar"));
	    assertThat(saveButton.isEnabled());
	    saveButton.click();
	    
	    String dniAlert = driver.switchTo().alert().getText();
		assertThat(dniAlert).isEqualTo("Dni erroneo, formato no válido"); 
		driver.switchTo().alert().accept();
	}
	
	@Test
	public void givenEditOwnerWhenFillBadDniLetterFieldThenAlertIsRetrieved() throws InterruptedException {
		addOwnerInfo();
		Thread.sleep(2000);
	    WebElement updateButton = driver.findElement(By.id("actualizarpropietario"));
	    assertThat(updateButton.isEnabled());
	    updateButton.click();
	    
	    WebElement nombreyapellidos = driver.findElement(By.id("nombreyapellidos"));
	    nombreyapellidos.clear();
	    nombreyapellidos.sendKeys(name);	    
	    assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value"))
           .isEqualTo(name);
	    
	    WebElement email = driver.findElement(By.id("email"));
	    email.clear();
	    email.sendKeys(emailValid);
	    assertThat(driver.findElement(By.id("email")).getAttribute("value"))
	        .isEqualTo(emailValid);
	    
	    WebElement contrasena = driver.findElement(By.id("contrasena"));
	    contrasena.clear();
	    contrasena.sendKeys(password);
	    
	    WebElement telefono = driver.findElement(By.id("telefono"));
	    telefono.clear();
	    telefono.sendKeys(phoneValid);
	    assertThat(driver.findElement(By.id("telefono")).getAttribute("value"))
           .isEqualTo(phoneValid);
	    
	    WebElement descripcion = driver.findElement(By.id("descripcion"));
	    descripcion.clear();
	    descripcion.sendKeys(description);
	    assertThat(driver.findElement(By.id("descripcion")).getAttribute("value"))
           .isEqualTo(description);
	    
	    WebElement dni = driver.findElement(By.id("dni"));
	    dni.clear();
	    dni.sendKeys(dniInvalidLetter);
	    assertThat(driver.findElement(By.id("dni")).getAttribute("value"))
           .isEqualTo(dniInvalidLetter);
	    
	    WebElement direccion = driver.findElement(By.id("direccion"));
	    direccion.clear();
	    direccion.sendKeys(streetAddress);
	    
	    assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value"))
	       .isEqualTo(subscription);
	    assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value"))
	       .isEqualTo(startDate);
	    assertThat(driver.findElement(By.id("fechafin")).getAttribute("value"))
	       .isEqualTo(endDate);
	    assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value"))
           .isEqualTo(servicePlan);
	    
	    WebElement saveButton = driver.findElement(By.id("guardar"));
	    assertThat(saveButton.isEnabled());
	    saveButton.click();
	    
	    String dniAlert = driver.switchTo().alert().getText();
		assertThat(dniAlert).isEqualTo("Dni erroneo, la letra del NIF no se corresponde"); 
		driver.switchTo().alert().accept();
	}
	
	// Remove Owner
	
	@Test
	public void givenOwnerWhenRemoveIsLaunchedAndAcceptedThenSuccessfulRemovePageIsRetrieved() throws InterruptedException {   
		addOwnerInfo();
		Thread.sleep(2000);
		WebElement deleteButton = driver.findElement(By.id("boton-borrar"));
	    assertThat(deleteButton.isEnabled());
	    deleteButton.click();
	    
	    String deleteAlert = driver.switchTo().alert().getText();

		assertThat(deleteAlert).isEqualTo("¿deseas borrar el propietario? Clicka en Ok o Cancel");   

		driver.switchTo().alert().accept();
		
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/propietario/+\\d+/borrar$"));
	    
	    WebElement elementGoHome = driver.findElement(By.xpath("//a[@href='/']"));
	    Thread.sleep(2000);
	    actionsGlobal.moveToElement(elementGoHome).click().build().perform();
	    Thread.sleep(2000); 
	}
	
	@Test
	public void givenOwnerWhenRemoveIsLaunchedAndNotAcceptedThenEditPageIsRetrieved() throws InterruptedException {
		addOwnerInfo();   
	    
	    WebElement deleteButton = driver.findElement(By.id("boton-borrar"));
	    assertThat(deleteButton.isEnabled());
	    deleteButton.click();
	    
	    String deleteAlert = driver.switchTo().alert().getText();

		assertThat(deleteAlert).isEqualTo("¿deseas borrar el propietario? Clicka en Ok o Cancel");   

		driver.switchTo().alert().dismiss();
		
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/propietario/+\\d+$"));
	}	

	// Add Accommodation
	@Test
	public void givenAddAccommodationWhenFillAllMandatoryFieldsThenSuccessfulNewAccommoddationFormIsRetrieved() throws InterruptedException {   
		setupAddAccommodation();
		
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    gotoOwnerPage();
	    
	    //find all anchor tags in the page
	    List<WebElement> refList = driver.findElements(By.tagName("a"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) {
        	
            if (we.getText().equals(nameAccommodation))
            {
            	assertEquals(we.getText(), nameAccommodation);
            }
        }	
	}
	
	@Test
	public void givenAddAccommodationWhenFillAllMandatoryFieldsAndPriceAndSomeCharacteristicsThenSuccessfulNewAccommoddationFormIsRetrieved() throws InterruptedException {   
		setupAddAccommodation();
		
        addCharacteristicsInfo();
        
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    gotoOwnerPage();
	    
	    //find all anchor tags in the page
	    List<WebElement> refList = driver.findElements(By.tagName("a"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) {
        	
            if (we.getText().equals(nameAccommodation))
            {
            	assertEquals(we.getText(), nameAccommodation);
            }
        }	
	}
	
	@Test
	public void givenAddAccommodationWhenFillCalendarThenSuccessfulNewAccommoddationFormIsRetrieved() throws InterruptedException {   
		setupAddAccommodation();
		
        addCharacteristicsInfo();
        //TODO
        addBookedDates();
        
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    gotoOwnerPage();
	    
	    //find all anchor tags in the page
	    List<WebElement> refList = driver.findElements(By.tagName("a"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) {
        	
            if (we.getText().equals(nameAccommodation))
            {
            	assertEquals(we.getText(), nameAccommodation);
            }
        }	
	}
	
	@Test
	public void givenAddAccommodationWhenAddPicturesThenSuccessfulNewAccommoddationFormIsRetrieved() throws InterruptedException {   
		setupAddAccommodation();
		
        addCharacteristicsInfo();
        //TODO
        addPictures();
        
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    gotoOwnerPage();
	    
	    //find all anchor tags in the page
	    List<WebElement> refList = driver.findElements(By.tagName("a"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) {
        	
            if (we.getText().equals(nameAccommodation))
            {
            	assertEquals(we.getText(), nameAccommodation);
            }
        }	
	}
	
	@Test
	public void givenAddAccommodationWhenFillCalendarAndAddPicturesThenSuccessfulNewAccommoddationFormIsRetrieved() throws InterruptedException {   
		setupAddAccommodation();
		
        addCharacteristicsInfo();
        //TODO
        addBookedDates();
        addPictures();
        
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    gotoOwnerPage();
	    
	    //find all anchor tags in the page
	    List<WebElement> refList = driver.findElements(By.tagName("a"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) {
        	
            if (we.getText().equals(nameAccommodation))
            {
            	assertEquals(we.getText(), nameAccommodation);
            }
        }	
	}	
	
	// Edit Accommodation
	
	@Test
	public void givenEditAccommodationWhenFillAllFieldsThenSaveButtonIsActiveAndAddAccommodationSuccessfulFormIsRetrieved() throws InterruptedException {   
		setupEditAccommodation();
		
	    WebElement saveButton = driver.findElement(By.id("guardar"));
	    assertThat(saveButton.isEnabled());
	    saveButton.click();
	    
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/propietario/+\\d+/alojamiento/+\\d+/actualizar$"));	   
	}
	
	@Test
	public void givenEditAccommodationWhenNotFillAllFieldsThenSaveButtonIsNotActive() throws InterruptedException {
		setupEditAccommodation();
		
		//clear mandatory field    
	    WebElement numbertourism = driver.findElement(By.id("numbertourism"));
	    numbertourism.clear();
		
	    WebElement saveButton = driver.findElement(By.id("guardar"));
		assertThat(!saveButton.isEnabled());
	}
	
	@Test
	public void givenEditAccommodationWithoutPicturesWhenAddNewPictureAndFillAllFieldsThenSaveButtonIsActive() throws InterruptedException {
		setupEditAccommodation();
		
		//TODO: add picture
	    WebElement saveButton = driver.findElement(By.id("guardar"));
		assertThat(!saveButton.isEnabled());
	}
	
	@Test
	public void givenEditAccommodationWithPicturesWhenAddNewPictureAndFillAllFieldsThenSaveButtonIsActive() throws InterruptedException {
		setupEditAccommodation();
		
		//TODO: add another picture (at least one before)
	    WebElement saveButton = driver.findElement(By.id("guardar"));
		assertThat(!saveButton.isEnabled());
	}
	
	@Test
	public void givenEditAccommodationWithPicturesWhenAddRemovePictureAndFillAllFieldsThenSaveButtonIsActive() throws InterruptedException {
		setupEditAccommodation();
		
		//TODO: remove a picture (at least one before)
	    WebElement saveButton = driver.findElement(By.id("guardar"));
		assertThat(!saveButton.isEnabled());
	}
	
	@Test
	public void givenEditAccommodationWithFilledCalendarWhenRefillCalendarAndFillAllFieldsThenSaveButtonIsActive() throws InterruptedException {
		setupEditAccommodation();
		
		//TODO: book new dates in the calendar
	    WebElement saveButton = driver.findElement(By.id("guardar"));
		assertThat(!saveButton.isEnabled());
	}
	
	@Test
	public void givenAccommodationWhenRemoveIsLaunchedAndAcceptedThenSuccessfulRemovePageIsRetrieved() throws InterruptedException {   
		setupDeleteAccommodation();
		
		WebElement deleteButton = driver.findElement(By.id("boton-borrar"));
	    assertThat(deleteButton.isEnabled());
	    deleteButton.click();
	    
	    String deleteAlert = driver.switchTo().alert().getText();

		assertThat(deleteAlert).isEqualTo("¿deseas borrar el alojamiento? Clicka en Ok o Cancel");   

		driver.switchTo().alert().accept();
		
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/propietario/+\\d+/alojamiento/+\\d+/borrar$"));
	    
	    gotoOwnerPage();
	}
	
	@Test
	public void givenAccommodationWhenRemoveIsLaunchedAndNotAcceptedThenEditPageIsRetrieved() throws InterruptedException {
		setupDeleteAccommodation();
	    
	    WebElement deleteButton = driver.findElement(By.id("boton-borrar"));
	    assertThat(deleteButton.isEnabled());
	    deleteButton.click();
	    
	    String deleteAlert = driver.switchTo().alert().getText();

		assertThat(deleteAlert).isEqualTo("¿deseas borrar el alojamiento? Clicka en Ok o Cancel");   

		driver.switchTo().alert().dismiss();
		
	    String URL = driver.getCurrentUrl();
	    Assert.assertThat(URL, Matchers.matchesPattern("^http://.+/propietario/+\\d+/alojamiento/+\\d+/editar$"));
	}
	
	// Filter test
	@Test
	public void givenThreeAccommodationsWhenFilterByProvinceThenOneAccommoddationIsRetrieved() throws InterruptedException {   
		populateOwnerWithOneAccommodation(3);
		
		System.out.println("givenThreeAccommodationsWhenFilterByProvinceThenOneAccommoddationIsRetrieved");
	    Select provinceAccommodation = new Select(driver.findElement(By.id("province")));
	    //Get only second province
	    int seedToLookFor = 2;
	    int provinceSeed = province + (seedToLookFor-1);
	    System.out.println(provinceSeed);
	    provinceAccommodation.selectByIndex(provinceSeed);
	    
	    buttonSearchClick();
	    
	    //find all anchor tags in the page
	    //a[contains(@class, 'alojamientofiltrado')]
	    List<WebElement> refList = driver.findElements(By.xpath("//a[contains(@class,'alojamientofiltrado')]"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
	    assertEquals(1, refList.size());
        for(WebElement we : refList) 
        {      
            assertEquals(we.getText(), nameAccommodation+String.valueOf(seedToLookFor));
        }
        
        depopulateAllOwnersAndAccommodations(3);
	}
	
	@Test
	public void givenThreeAccommodationsWhenFilterByPeopleThenOneAccommoddationIsRetrieved() throws InterruptedException {   
		populateOwnerWithOneAccommodation(3);
		
		System.out.println("givenThreeAccommodationsWhenFilterByPeopleThenOneAccommoddationIsRetrieved");
	    Select peopleAccommodation = new Select(driver.findElement(By.id("people")));
	    //Get only third accommodation in people
	    int seedToLookFor = 3;
	    int peopleSeedInteger = Integer.parseInt(people)+seedToLookFor;
	    String peopleSeedString = String.valueOf(peopleSeedInteger);
	    System.out.println(peopleSeedString);
	    peopleAccommodation.selectByValue(peopleSeedString);
	    
	    buttonSearchClick();
	    
	    //find all anchor tags in the page
	    //a[contains(@class, 'alojamientofiltrado')]
	    List<WebElement> refList = driver.findElements(By.xpath("//a[contains(@class, 'alojamientofiltrado')]"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
	    assertEquals(1, refList.size());
        for(WebElement we : refList) 
        {      
            assertEquals(we.getText(), nameAccommodation+String.valueOf(seedToLookFor));           
        }	
        
        depopulateAllOwnersAndAccommodations(3);
	}
	
	@Test
	public void givenThreeAccommodationsWhenFilterByFreeDatesThenThreeAccommoddationsAreRetrieved() throws InterruptedException {   
		populateOwnerWithOneAccommodation(3);
		
		System.out.println("givenThreeAccommodationsWhenFilterByFreeDatesThenThreeAccommoddationsAreRetrieved");
	    
		WebElement startDate = driver.findElement(By.id("datetimepicker1"));
		System.out.println(faker.date().future(5, TimeUnit.DAYS).toString());
	    startDate.sendKeys("04-06-2019");
	    
		WebElement endDate = driver.findElement(By.id("datetimepicker2"));
		System.out.println(faker.date().future(7, TimeUnit.DAYS).toString());
	    endDate.sendKeys("06-06-2019");
	    
	    buttonSearchClick();
	    
	    //find all anchor tags in the page
	    //a[contains(@class, 'alojamientofiltrado')]
	    List<WebElement> refList = driver.findElements(By.xpath("//a[contains(@class, 'alojamientofiltrado')]"));
	    assertEquals(3, refList.size());
	    
	    depopulateAllOwnersAndAccommodations(3);
	}
	
	@Test
	public void givenThreeAccommodationsWhenFilterByProvinceAndPeopleThenOneAccommoddationIsRetrieved() throws InterruptedException {   
		populateOwnerWithOneAccommodation(3);
		
		System.out.println("givenThreeAccommodationsWhenFilterByProvinceAndPeopleThenOneAccommoddationIsRetrieved");
	    Select provinceAccommodation = new Select(driver.findElement(By.id("province")));
	    //Get only second province
	    int seedToLookFor = 1;
	    int provinceSeed = province + seedToLookFor-1;
	    System.out.println(provinceSeed);
	    provinceAccommodation.selectByIndex(provinceSeed);
	    
	    Select peopleAccommodation = new Select(driver.findElement(By.id("people")));
	    //Get only third accommodation in people
	    int peopleSeedInteger = Integer.parseInt(people)+seedToLookFor;
	    String peopleSeedString = String.valueOf(peopleSeedInteger);
	    System.out.println(peopleSeedString);
	    peopleAccommodation.selectByValue(peopleSeedString);
	    
	    buttonSearchClick();
	    
	    //find all anchor tags in the page
	    //a[contains(@class, 'alojamientofiltrado')]
	    List<WebElement> refList = driver.findElements(By.xpath("//a[contains(@class, 'alojamientofiltrado')]"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
	    assertEquals(1, refList.size());
	    
        for(WebElement we : refList) 
        {   
        	assertEquals(we.getText(), nameAccommodation+String.valueOf(seedToLookFor));
        }		
        
        depopulateAllOwnersAndAccommodations(3);
	}
	
	/*@Test
	public void givenThreeAccommodationsWhenFilterByProvinceAndDatesThenOneAccommoddationIsRetrieved() throws InterruptedException {   
	    
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    driver.findElement(By.xpath("//a[@href='/']")).click();
	    
	    //find all anchor tags in the page
	    List<WebElement> refList = driver.findElements(By.tagName("a"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) {
        	
            if (we.getText().equals(nameAccommodation))
            {
            	assertEquals(we.getText(), nameAccommodation);
            	//we.click();
            }
        }	
	}
	
	@Test
	public void givenThreeAccommodationsWhenFilterByDatesAndPeopleThenOneAccommoddationIsRetrieved() throws InterruptedException {   
	    
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    driver.findElement(By.xpath("//a[@href='/']")).click();
	    
	    //find all anchor tags in the page
	    List<WebElement> refList = driver.findElements(By.tagName("a"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) {
        	
            if (we.getText().equals(nameAccommodation))
            {
            	assertEquals(we.getText(), nameAccommodation);
            	//we.click();
            }
        }	
	}*/
	
	@Test
	public void givenThreeAccommodationsWhenNoFiltersThenAllAccommoddationsAreRetrieved() throws InterruptedException {   
		populateOwnerWithOneAccommodation(3);
		
	    buttonSearchClick();
	    
		//find all anchor tags in the page
	    //a[contains(@class, 'alojamientofiltrado')]
	    List<WebElement> refList = driver.findElements(By.xpath("//a[contains(@class, 'alojamientofiltrado')]"));
	    
        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
	    
	    //Only with this count should be enough
	    assertEquals(3, refList.size());	
	    
	    depopulateAllOwnersAndAccommodations(3);
	}
	
	@Test
	public void givenThreeAccommodationsWhenFilterByProvinceNotMatchThenNoAccommoddationIsRetrieved() throws InterruptedException {   
		populateOwnerWithOneAccommodation(3);
		
		System.out.println("givenThreeAccommodationsWhenFilterByProvinceNotMatchThenNoAccommoddationIsRetrieved");
	    Select provinceAccommodation = new Select(driver.findElement(By.id("province")));
	    //Try to get only fourth accommodation in province (only 3)
	    int seedToLookFor = 4;
	    int provinceSeed = province + seedToLookFor;
	    System.out.println(provinceSeed);
	    provinceAccommodation.selectByIndex(provinceSeed);
	     
	    buttonSearchClick();
	    
	    //find all anchor tags in the page
	    //a[contains(@class, 'alojamientofiltrado')]
	    List<WebElement> refList = driver.findElements(By.xpath("//a[contains(@class, 'alojamientofiltrado')]"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
	    assertEquals(0, refList.size());
	    
	    depopulateAllOwnersAndAccommodations(3);
	}
	
	@Test
	public void givenThreeAccommodationsWhenFilterByPeopleNotMatchThenNoAccommoddationIsRetrieved() throws InterruptedException {   
		populateOwnerWithOneAccommodation(3);
		
		System.out.println("givenThreeAccommodationsWhenFilterByPeopleThenOneAccommoddationIsRetrieved");
	    Select peopleAccommodation = new Select(driver.findElement(By.id("people")));
	    //Try to get only fourth accommodation in people (only 3)
	    int seedToLookFor = 4;
	    int peopleSeedInteger = Integer.parseInt(people)+seedToLookFor;
	    String peopleSeedString = String.valueOf(peopleSeedInteger);
	    System.out.println(peopleSeedString);
	    peopleAccommodation.selectByValue(peopleSeedString);
	    
	    buttonSearchClick();
	    
	    //find all anchor tags in the page
	    //a[contains(@class, 'alojamientofiltrado')]
	    List<WebElement> refList = driver.findElements(By.xpath("//a[contains(@class, 'alojamientofiltrado')]"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
	    assertEquals(0, refList.size());
	    
	    depopulateAllOwnersAndAccommodations(3);
	}
	
	
	
	/*@Test
	public void givenThreeAccommodationsWhenFilterByDatesNotMatchThenNoAccommoddationIsRetrieved() throws InterruptedException {   
	    
		WebElement sendButton = driver.findElement(By.id("sendButton"));
	    assertThat(sendButton.isEnabled());
	    sendButton.click();

	    driver.findElement(By.xpath("//a[@href='/']")).click();
	    
	    //find all anchor tags in the page
	    List<WebElement> refList = driver.findElements(By.tagName("a"));

        //iterate over web elements and use the getAttribute method to 
        //find the hypertext reference's value.
        for(WebElement we : refList) {
        	
            if (we.getText().equals(nameAccommodation))
            {
            	assertEquals(we.getText(), nameAccommodation);
            	//we.click();
            }
        }	
	}*/
	
	@Test
	public void givenOneAccommodationsWhenClickOnItThenGuestWiewOfThisAccommodationIsRetrieved() throws InterruptedException {   
		populateOwnerWithOneAccommodation(1);
		
	    buttonSearchClick();
	    
		//find all anchor tags in the page
	    //a[contains(@class, 'alojamientofiltrado')]
	    WebElement guestViewOfAccommodation = driver.findElement(By.xpath("//a[contains(@class, 'alojamientofiltrado')]"));
	    guestViewOfAccommodation.click();

	    Thread.sleep(2000);
	    
	    List<WebElement> guestViewOfAccommodationName = driver.findElements(By.tagName("a")); 
        Iterator<WebElement> i = guestViewOfAccommodationName.iterator(); 
        String href = "";
        while(i.hasNext()) { 
                WebElement anchor = i.next(); 
                href = anchor.getAttribute("href"); 
                //System.out.println(href); 
                if (href.contains(web))
                { 
                	//System.out.println("El que busco "+ href);
                	assertTrue(href.contains(web));
                	break;
                }
        } 
        assertTrue(!href.equals(""));
	    
	    depopulateAllOwnersAndAccommodations(1);
	}
	
}

