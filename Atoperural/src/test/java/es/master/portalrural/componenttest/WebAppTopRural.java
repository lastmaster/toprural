package es.master.portalrural.componenttest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

public class WebAppTopRural {
    private RemoteWebDriver driver;
    
    public WebAppTopRural(RemoteWebDriver driver) {
        this.driver = driver;
        this.driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // this.driver.get("https://127.0.0.1:8543");
    }
    
    public void setHomeWebDriver() {
        if (this.driver != null)
            this.driver.get("http://host.testcontainers.internal:8081/");
    }

    public RemoteWebDriver getDriver() {
        return this.driver;
    }
    
    public String getAlert() {
        return this.driver.switchTo().alert().getText();
        
    }
}
