package es.master.portalrural.e2e;

import static org.assertj.core.api.Assertions.assertThat;
import java.net.MalformedURLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class HomePortalRuralIT {

    private static WebDriver driver;
    
    @BeforeClass
    public static void setupClass() {
        System.setProperty("webdriver.chrome.driver",
                "/usr/bin/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");

        driver = new ChromeDriver();
    }
    
    @AfterClass
    public static void teardownClass() {
        driver.close();
    }
    
    
    @Before
    public void setupTest() throws MalformedURLException {
        driver.get("https://www.atoperural.es");
    }

    @After
    public void teardown() {}
    

    @Test
    public void givenHomePortalRuralWhenAddOwnerIsClickThenAnEmptyFormIsShown () 
            throws MalformedURLException, InterruptedException {
        
        Thread.sleep(1000);
        WebElement element = driver.findElement(By.id("addOwnerButton"));
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().build().perform();
        Thread.sleep(1000);
        
        assertThat(driver.findElement(By.id("nombreyapellidos")).getAttribute("value")).isEqualTo("");
        assertThat(driver.findElement(By.id("email")).getAttribute("value")).isEqualTo("");
        assertThat(driver.findElement(By.id("contrasena")).getAttribute("value")).isEqualTo("");
        assertThat(driver.findElement(By.id("telefono")).getAttribute("value")).isEqualTo("");
        assertThat(driver.findElement(By.id("descripcion")).getAttribute("value")).isEqualTo("");
        assertThat(driver.findElement(By.id("dni")).getAttribute("value")).isEqualTo("");
        assertThat(driver.findElement(By.id("direccion")).getAttribute("value")).isEqualTo("");
        assertThat(driver.findElement(By.id("estadosubscripcion")).getAttribute("value")).isEqualTo("activa");
        assertThat(driver.findElement(By.id("fechainicio")).getAttribute("value")).isEqualTo("01-01-2019");
        assertThat(driver.findElement(By.id("fechafin")).getAttribute("value")).isEqualTo("31-12-2030");
        assertThat(driver.findElement(By.id("tiposervicio")).getAttribute("value")).isEqualTo("silver");
    }
}

