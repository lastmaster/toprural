package es.master.portalrural.infrastructure.webform;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class CheckableItemCollectionManipulatorTest
{
  private static CheckableItem item1 = checkedItem();
  private static CheckableItem item2 = uncheckedItem();
  private static CheckableItem item3 = checkedItem();
  private static CheckableItem item4 = uncheckedItem();

  private CheckableItemCollectionManipulator<CheckableItem> checkableItemCollectionManipulator;

  @Before
  public void setUp()
  {
    checkableItemCollectionManipulator = new CheckableItemCollectionManipulator<>();
  }

  @Test
  public void preserveCheckedItems()
  {
    Collection<CheckableItem> theCheckedItems =
      checkableItemCollectionManipulator.preserveCheckedItems(
        Arrays.asList(item1, item2, item3, item4));

    assertThat(theCheckedItems, containsInAnyOrder(item1, item3));
  }

  @Test
  public void dropCheckedItems()
  {

    Collection<CheckableItem> theCheckedItems =
      checkableItemCollectionManipulator.dropCheckedItems(
        Arrays.asList(item1, item2, item3, item4));

    assertThat(theCheckedItems, containsInAnyOrder(item2, item4));
  }

  private static CheckableItem checkedItem()
  {
    return new CheckableItem()
    {
      @Override
      public boolean isChecked()
      {
        return true;
      }
    };
  }

  private static CheckableItem uncheckedItem()
  {
    return new CheckableItem()
    {
      @Override
      public boolean isChecked()
      {
        return false;
      }
    };
  }
}