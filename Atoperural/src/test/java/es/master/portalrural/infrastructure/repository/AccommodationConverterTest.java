package es.master.portalrural.infrastructure.repository;

import es.master.portalrural.domain.model.AccommodationFeature;
import es.master.portalrural.domain.model.Picture;
import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationFeatureTestImpl;
import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.domain.model.PictureTestImpl;
import es.master.portalrural.domain.model.TestAcommodationDataGenerator;
import es.master.portalrural.infrastructure.repository.database.entities.Alojamiento;
import es.master.portalrural.infrastructure.repository.database.entities.BookedDate;
import es.master.portalrural.infrastructure.repository.database.entities.Fotografia;
import es.master.portalrural.infrastructure.repository.database.entities.PrecioCaracteristica;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.valueOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccommodationConverterTest
{
  @Mock
  private AccommodationCalendarsConverter accommodationCalendarsConverter;

  private AccommodationConverter accommodationConverter;

  @Before
  public void setUp()
  {
    accommodationConverter = new AccommodationConverter(accommodationCalendarsConverter);
    when(accommodationCalendarsConverter.from(bookedDates())).thenReturn(accommodationCalendars());
  }

  @Test
  public void givenAlojamiento_thenReturnAccommodation()
  {
    assertThat(
      accommodationConverter.from(alojamientoToConvert(55L)),
      is(expectedAccommodation(55L))
    );
  }

  @Test
  public void givenCollectionOfAlojamiento_thenReturnCollectionOfAccommodation()
  {
    assertThat(
      accommodationConverter.from(Arrays.asList(
        alojamientoToConvert(111L),
        alojamientoToConvert(222L),
        alojamientoToConvert(333L)
      )),
      containsInAnyOrder(
        expectedAccommodation(111L),
        expectedAccommodation(222L),
        expectedAccommodation(333L)
      )
    );
  }

  private Accommodation expectedAccommodation(Long anAccommodationId)
  {
    return TestAcommodationDataGenerator.generate(anAccommodationId).getAccommodation(
      features(),
      pictures(),
      accommodationCalendars()
    );
  }

  private Alojamiento alojamientoToConvert(Long anAccommodationId)
  {
    return TestAcommodationDataGenerator.generate(anAccommodationId).getAlojamiento(
      precioCaracteristicas(),
      fotos(),
      bookedDates()
    );
  }

  private static final Picture picture1 = new PictureTestImpl(1L, "nombre1", "desc1", "tamanio1", "tipo1", 1);
  private static final Picture picture2 = new PictureTestImpl(2L, "nombre2", "desc2", "tamanio2", "tipo2", 2);
  private static final Picture picture3 = new PictureTestImpl(3L, "nombre3", "desc3", "tamanio3", "tipo3", 3);

  private static final Fotografia foto1 = fotografia(1L, "nombre1", "desc1", "tamanio1", "tipo1", 1);
  private static final Fotografia foto2 = fotografia(2L, "nombre2", "desc2", "tamanio2", "tipo2", 2);
  private static final Fotografia foto3 = fotografia(3L, "nombre3", "desc3", "tamanio3", "tipo3", 3);

  private static final AccommodationFeature feature1 = new AccommodationFeatureTestImpl(11L, valueOf(1));
  private static final AccommodationFeature feature2 = new AccommodationFeatureTestImpl(22L, valueOf(2));
  private static final AccommodationFeature feature3 = new AccommodationFeatureTestImpl(33L, valueOf(3));

  private static PrecioCaracteristica precioCaracteristica1 = precioCaracteristica(1L, 11L, 1.0);
  private static PrecioCaracteristica precioCaracteristica2 = precioCaracteristica(2L, 22L, 2.0);
  private static PrecioCaracteristica precioCaracteristica3 = precioCaracteristica(3L, 33L, 3.0);

  
  // Instantiate a Date object
  private static final Date today = new Date();
  private static final Date date2 = new Date(2323223232L);
  @SuppressWarnings("deprecation")
  private static final Date date3 = new Date(2019, 05, 28);
  
  private static final AccommodationCalendar accommodationCalendar1 = new AccommodationCalendar(1L, today, today.toString());
  private static final AccommodationCalendar accommodationCalendar2 = new AccommodationCalendar(1L, date2, date2.toString());
  private static final AccommodationCalendar accommodationCalendar3 = new AccommodationCalendar(1L, date3, date3.toString());
  
  private static final BookedDate bookedDate1 = bookDate(1L, today);
  private static final BookedDate bookedDate2 = bookDate(1L, date2);
  private static final BookedDate bookedDate3 = bookDate(1L, date3);
  
  private static List<Fotografia> fotos()
  {
    return Arrays.asList(foto1, foto2, foto3);
  }

  private static Collection<PrecioCaracteristica> precioCaracteristicas()
  {
    return Arrays.asList(precioCaracteristica1, precioCaracteristica2, precioCaracteristica3);
  }
  
  private static List<BookedDate> bookedDates()
  {
    return Arrays.asList(bookedDate1, bookedDate2, bookedDate3);
  }

  private static List<Picture> pictures()
  {
    return Arrays.asList(picture1, picture2, picture3);
  }
  
  private static List<AccommodationCalendar> accommodationCalendars()
  {
    return Arrays.asList(accommodationCalendar1, accommodationCalendar2, accommodationCalendar3);
  }

  private static List<AccommodationFeature> features()
  {
    return Arrays.asList(feature1, feature2, feature3);
  }

  private static Fotografia fotografia(long id,
                                       String nombre,
                                       String descripcion,
                                       String tamano,
                                       String tipo,
                                       int orden)
  {
    Fotografia fotografia = new Fotografia();
    fotografia.setId(id);
    fotografia.setNombreFoto(nombre);
    fotografia.setDescripcion(descripcion);
    fotografia.setTamano(tamano);
    fotografia.setTipoFichero(tipo);
    fotografia.setOrden(orden);
    return fotografia;
  }

  private static PrecioCaracteristica precioCaracteristica(Long id, Long idCaracteristica, double precio)
  {
    PrecioCaracteristica precioCaracteristica = new PrecioCaracteristica();
    precioCaracteristica.setId(id);
    precioCaracteristica.setIdCaracteristica(idCaracteristica);
    precioCaracteristica.setPrecio(precio);
    return precioCaracteristica;
  }
  
  private static BookedDate bookDate(Long id,
          Date dayToBook)
  {
	BookedDate bookedDate = new BookedDate();
	bookedDate.setId(id);
	bookedDate.setBookedDate(dayToBook);
	return bookedDate;
  }
}