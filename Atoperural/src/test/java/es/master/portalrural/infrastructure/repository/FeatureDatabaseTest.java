package es.master.portalrural.infrastructure.repository;

import es.master.portalrural.domain.model.Feature;
import es.master.portalrural.infrastructure.repository.database.entities.Caracteristica;
import es.master.portalrural.infrastructure.repository.database.repositories.CaracteristicaRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FeatureDatabaseTest
{
  private static final Long A_FEATURE_ID_1 = 4567L;
  private static final Long A_FEATURE_ID_2 = 7890L;
  private static final Long A_FEATURE_ID_3 = 5432L;

  @Mock
  private CaracteristicaRepository caracteristicaRepository;

  private FeatureDatabase featureDatabase;

  @Before
  public void setUp()
  {
    featureDatabase = new FeatureDatabase(caracteristicaRepository);
  }

  @Test
  public void get()
  {
    when(caracteristicaRepository.findOne(A_FEATURE_ID_1)).thenReturn(caracteristica(A_FEATURE_ID_1));

    assertThat(featureDatabase.get(A_FEATURE_ID_1), is(expectedFeature(A_FEATURE_ID_1)));
  }

  @Test
  public void findAll()
  {
    when(caracteristicaRepository.findAll()).thenReturn(Arrays.asList(
      caracteristica(A_FEATURE_ID_1),
      caracteristica(A_FEATURE_ID_2),
      caracteristica(A_FEATURE_ID_3)
    ));

    assertThat(featureDatabase.findAll(),
      containsInAnyOrder(
        expectedFeature(A_FEATURE_ID_1),
        expectedFeature(A_FEATURE_ID_2),
        expectedFeature(A_FEATURE_ID_3)));
  }

  private static Caracteristica caracteristica(Long aFeatureId1)
  {
    Caracteristica caracteristica = new Caracteristica();
    caracteristica.setId(aFeatureId1);
    caracteristica.setDescripcion(someDescription(aFeatureId1));
    return caracteristica;
  }

  private Feature expectedFeature(Long aFeatureId)
  {
    return new Feature(aFeatureId, someDescription(aFeatureId));
  }

  private static String someDescription(Long aLong)
  {
    return String.format(":description_%d:", aLong);
  }

}