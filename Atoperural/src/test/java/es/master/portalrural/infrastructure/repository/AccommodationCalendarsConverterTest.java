package es.master.portalrural.infrastructure.repository;

import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.infrastructure.repository.database.entities.BookedDate;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class AccommodationCalendarsConverterTest
{
  // Instantiate a Date object
  private static final Date today = new Date();
  private static final Date date2 = new Date(2323223232L);
  @SuppressWarnings("deprecation")
  private static final Date date3 = new Date(2019, 05, 28);
  
  private static final AccommodationCalendar accommodationCalendar1 = new AccommodationCalendar(1L, today, today.toString());
  private static final AccommodationCalendar accommodationCalendar2 = new AccommodationCalendar(1L, date2, date2.toString());
  private static final AccommodationCalendar accommodationCalendar3 = new AccommodationCalendar(1L, date3, date3.toString());
  
  private static final BookedDate bookedDate1 = bookDate(1L, today);
  private static final BookedDate bookedDate2 = bookDate(1L, date2);
  private static final BookedDate bookedDate3 = bookDate(1L, date3);
  
  private AccommodationCalendarsConverter accommodationCalendarsConverter;

  @Before
  public void setUp()
  {
    accommodationCalendarsConverter = new AccommodationCalendarsConverter();
  }

  @Test
  public void givenCollectionOfBookedDate_thenReturnCollectionOfCalendar()
  {
    assertThat(
      accommodationCalendarsConverter.from(Arrays.asList(bookedDate1, bookedDate2, bookedDate3)),
      containsInAnyOrder(accommodationCalendar1, accommodationCalendar2, accommodationCalendar3)
    );
  }

  private static BookedDate bookDate(Long id,
          Date dayToBook)
  {
	BookedDate bookedDate = new BookedDate();
	bookedDate.setId(id);
	bookedDate.setBookedDate(dayToBook);
	return bookedDate;
  }

}