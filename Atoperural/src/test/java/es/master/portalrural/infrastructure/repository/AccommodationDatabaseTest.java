package es.master.portalrural.infrastructure.repository;

import es.master.portalrural.domain.model.AccommodationFeature;
import es.master.portalrural.domain.model.Picture;
import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.domain.model.TestAccommodationData;
import es.master.portalrural.domain.model.TestAcommodationDataGenerator;
import es.master.portalrural.infrastructure.repository.database.entities.Alojamiento;
import es.master.portalrural.infrastructure.repository.database.entities.BookedDate;
import es.master.portalrural.infrastructure.repository.database.entities.Fotografia;
import es.master.portalrural.infrastructure.repository.database.entities.PrecioCaracteristica;
import es.master.portalrural.infrastructure.repository.database.entities.Propietario;
import es.master.portalrural.infrastructure.repository.database.repositories.AlojamientoRepository;
import es.master.portalrural.infrastructure.repository.database.repositories.BookedDateRepository;
import es.master.portalrural.infrastructure.repository.database.repositories.PropietarioRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

@RunWith(MockitoJUnitRunner.class)
public class AccommodationDatabaseTest
{
  @Mock
  private AlojamientoRepository alojamientoRepository;
  @Mock
  private PropietarioRepository propietarioRepository;
  @Mock
  private BookedDateRepository bookedDateRepository;
  @Mock
  private AccommodationConverter accommodationConverter;

  private AccommodationDatabase accommodationDatabase;

  @Before
  public void setUp()
  {
    accommodationDatabase = new AccommodationDatabase(
      alojamientoRepository,
      propietarioRepository,
      bookedDateRepository,
      accommodationConverter);
  }

  @Test
  public void findAll()
  {
    when(alojamientoRepository.findAll()).thenReturn(Arrays.asList(
      accommodationData1.getAlojamiento(EMPTY_PRECIO_CARACTERISTICAS_COLLECTION, EMPTY_FOTOGRAFIAS_COLLECTION, EMPTY_BOOKEDDATES_COLLECTION),
      accommodationData2.getAlojamiento(EMPTY_PRECIO_CARACTERISTICAS_COLLECTION, EMPTY_FOTOGRAFIAS_COLLECTION, EMPTY_BOOKEDDATES_COLLECTION),
      accommodationData3.getAlojamiento(EMPTY_PRECIO_CARACTERISTICAS_COLLECTION, EMPTY_FOTOGRAFIAS_COLLECTION, EMPTY_BOOKEDDATES_COLLECTION)
    ));

    assertThat(accommodationDatabase.findAll(), containsInAnyOrder(
      accommodationData1.getAccommodationReduced(),
      accommodationData2.getAccommodationReduced(),
      accommodationData3.getAccommodationReduced()
    ));
  }

  @Test
  public void get()
  {
    Alojamiento alojamiento = accommodationData1.getAlojamiento(
      EMPTY_PRECIO_CARACTERISTICAS_COLLECTION,
      EMPTY_FOTOGRAFIAS_COLLECTION,
      EMPTY_BOOKEDDATES_COLLECTION);
    Accommodation accommodation = accommodationData1.getAccommodation(
      EMPTY_FEATURES_COLLECTION,
      EMPTY_PICTURES_COLLECTION,
      EMPTY_ACCOMODATIONCALENDARS_COLLECTION);

    when(alojamientoRepository.getOne(ACCOMMODATION_ID_1)).thenReturn(alojamiento);
    when(accommodationConverter.from(alojamiento)).thenReturn(accommodation);

    assertThat(accommodationDatabase.get(ACCOMMODATION_ID_1), is(accommodation));
  }

  @Test
  public void delete()
  {
    Alojamiento alojamiento = accommodationData1.getAlojamiento(
      EMPTY_PRECIO_CARACTERISTICAS_COLLECTION,
      EMPTY_FOTOGRAFIAS_COLLECTION,
      EMPTY_BOOKEDDATES_COLLECTION);
    when(alojamientoRepository.getOne(ACCOMMODATION_ID_1)).thenReturn(alojamiento);

    assertThat(accommodationDatabase.delete(ACCOMMODATION_ID_1),
      is(accommodationData1.getAccommodationReduced()));
    verify(alojamientoRepository, times(1)).delete(ACCOMMODATION_ID_1);
  }

  @Test
  public void savePreservingOwner()
  {
    Alojamiento alojamiento = accommodationData1.getAlojamiento(
      EMPTY_PRECIO_CARACTERISTICAS_COLLECTION,
      EMPTY_FOTOGRAFIAS_COLLECTION,
      EMPTY_BOOKEDDATES_COLLECTION);
    alojamiento.setOwner(aPropietario());

    Accommodation accommodation = accommodationData1.getAccommodation(
      EMPTY_FEATURES_COLLECTION,
      EMPTY_PICTURES_COLLECTION,
      EMPTY_ACCOMODATIONCALENDARS_COLLECTION);

    when(alojamientoRepository.findOne(ACCOMMODATION_ID_1)).thenReturn(alojamiento);
    when(alojamientoRepository.save(alojamiento)).thenReturn(alojamiento);
    when(accommodationConverter.from(alojamiento)).thenReturn(accommodation);

    assertThat(accommodationDatabase.savePreservingOwner(accommodation), is(accommodation));
  }

  @Test
  public void saveAssigningOwner()
  {
    Long propietarioId = 555L;
    Propietario propietario = aPropietario();

    Alojamiento alojamientoWithPropietario = accommodationData1.getAlojamiento(
      EMPTY_PRECIO_CARACTERISTICAS_COLLECTION,
      EMPTY_FOTOGRAFIAS_COLLECTION,
      EMPTY_BOOKEDDATES_COLLECTION);
    alojamientoWithPropietario.setOwner(propietario);

    Accommodation accommodation = accommodationData1.getAccommodation(
      EMPTY_FEATURES_COLLECTION,
      EMPTY_PICTURES_COLLECTION,
      EMPTY_ACCOMODATIONCALENDARS_COLLECTION);

    when(propietarioRepository.findOne(propietarioId)).thenReturn(propietario);
    when(alojamientoRepository.save(alojamientoWithPropietario)).thenReturn(alojamientoWithPropietario);
    when(accommodationConverter.from(alojamientoWithPropietario)).thenReturn(accommodation);

    assertThat(accommodationDatabase.saveAssigningOwner(accommodation, propietarioId), is(accommodation));
  }


  @Test
  public void givenAnId_whenNoAlojamientoWithId_thenGetReturnsNull()
  {
    when(alojamientoRepository.getOne(ACCOMMODATION_ID_1)).thenReturn(null);

    assertThat(accommodationDatabase.get(ACCOMMODATION_ID_1), nullValue());
    verifyZeroInteractions(accommodationConverter);
  }

  @Test
  public void givenAnId_whenNoAlojamientoWithId_thenDeleteReturnsNull()
  {
    when(alojamientoRepository.getOne(ACCOMMODATION_ID_1)).thenReturn(null);

    assertThat(accommodationDatabase.delete(ACCOMMODATION_ID_1), nullValue());
    verify(alojamientoRepository, never()).delete(ACCOMMODATION_ID_1);
  }

  private static final Long ACCOMMODATION_ID_1 = 49L;
  private static final Long ACCOMMODATION_ID_2 = 666L;
  private static final Long ACCOMMODATION_ID_3 = 5454L;

  private static TestAccommodationData accommodationData1 = TestAcommodationDataGenerator.generate(ACCOMMODATION_ID_1);
  private static TestAccommodationData accommodationData2 = TestAcommodationDataGenerator.generate(ACCOMMODATION_ID_2);
  private static TestAccommodationData accommodationData3 = TestAcommodationDataGenerator.generate(ACCOMMODATION_ID_3);

  private static final Collection<PrecioCaracteristica> EMPTY_PRECIO_CARACTERISTICAS_COLLECTION =
    Collections.emptyList();
  private static final Collection<Fotografia> EMPTY_FOTOGRAFIAS_COLLECTION =
    Collections.emptyList();
  private static final Collection<BookedDate> EMPTY_BOOKEDDATES_COLLECTION =
    Collections.emptyList();

  private static final Collection<AccommodationFeature> EMPTY_FEATURES_COLLECTION =
    Collections.emptyList();
  private static final Collection<Picture> EMPTY_PICTURES_COLLECTION =
    Collections.emptyList();  
  private static final Collection<AccommodationCalendar> EMPTY_ACCOMODATIONCALENDARS_COLLECTION =
    Collections.emptyList();

  private static Propietario aPropietario()
  {
    Propietario propietario = new Propietario();
    propietario.setNombreYapellidos("Nombre Apellido");
    return propietario;
  }
}
