package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.AccommodationReduced;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DeleteAccommodationUseCaseTest
{
  private static final Long AN_ID = 5L;
  private static final String A_NAME = ":accommodation_name:";

  @Mock
  private AccommodationRepository accommodationRepository;

  private DeleteAccommodationUseCase deleteAccommodationUseCase;

  @Before
  public void setUp() {
    deleteAccommodationUseCase = new DeleteAccommodationUseCase(accommodationRepository);
  }

  @Test
  public void successfulExecution() {
    when(accommodationRepository.delete(AN_ID)).thenReturn(expectedAccommodationReduced());

    assertThat(deleteAccommodationUseCase.execute(AN_ID), is(expectedAccommodationReduced()));
  }

  private static AccommodationReduced expectedAccommodationReduced() {
    return new AccommodationReduced(AN_ID, A_NAME);
  }
}