package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationFeatureTestImpl;
import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.domain.model.PictureTestImpl;
import es.master.portalrural.domain.model.TestAcommodationDataGenerator;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateAccommodationUseCaseTest
{
  private static final Accommodation accommodation = TestAcommodationDataGenerator.generate(4L).getAccommodation(
    Collections.<AccommodationFeatureTestImpl>emptyList(), Collections.<PictureTestImpl>emptyList(), Collections.<AccommodationCalendar>emptyList());

  @Mock
  private AccommodationRepository accommodationRepository;

  private UpdateAccommodationUseCase updateAccommodationUseCase;

  @Before
  public void setUp()
  {
    updateAccommodationUseCase = new UpdateAccommodationUseCase(accommodationRepository);
  }

  @Test
  public void successfulExecution()
  {
    when(accommodationRepository.savePreservingOwner(accommodation)).thenReturn(accommodation);

    assertThat(updateAccommodationUseCase.execute(accommodation), is(accommodation));
  }
}