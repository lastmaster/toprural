package es.master.portalrural.domain.model;

public class TestAcommodationDataGenerator
{
  private static final String A_NAME = ":name_%d:";
  private static final String A_REGISTRATION_NUMBER = ":reg_number_%d:";
  private static final int AN_ACCOMMODATION_TYPE_BASE = 10;
  private static final String A_WEB = ":web_%d:";
  private static final String A_FACEBOOK = ":facebook_%d:";
  private static final String A_TWITTER = ":twitter_%d:";
  private static final String AN_INSTAGRAM = ":instagram_%d:";
  private static final String A_YOUTUBE = ":youtube_%d:";
  private static final String A_PROVINCE = ":province_%d:";
  private static final String A_CITY = ":city_%d:";
  private static final String AN_ADDRESS = ":address_%d:";
  private static final String A_POSTAL_CODE = ":postalCode_%d:";
  private static final String A_COUNTRY = ":country_%d:";
  private static final String A_LATITUDE = ":latitude_%d:";
  private static final String A_LONGITUDE = ":longitude_%d:";
  private static final String A_MOBILE_PHONE = ":mobile_phone_%d:";
  private static final String A_PHONE = ":phone_%d:";
  private static final String A_NUMBER_OF_PEOPLE = ":num_people_%d:";
  private static final String A_NUMBER_OF_ROOMS = ":num_rooms_%d:";
  private static final String A_SHORT_DESCRIPTION = ":short_desc_%d:";
  private static final String A_LONG_DESCRIPTION = ":long_desc_%d:";
  private static final String SOME_ACTIVITIES = ":activities_%d:";

  public static TestAccommodationData generate(Long id)
  {
    return new TestAccommodationData(id,
      String.format(A_NAME, id),
      String.format(A_REGISTRATION_NUMBER, id),
      AN_ACCOMMODATION_TYPE_BASE + id.intValue(),
      String.format(A_WEB, id),
      String.format(A_FACEBOOK, id),
      String.format(A_TWITTER, id),
      String.format(AN_INSTAGRAM, id),
      String.format(A_YOUTUBE, id),
      String.format(A_PROVINCE, id),
      String.format(A_CITY, id),
      String.format(AN_ADDRESS, id),
      String.format(A_POSTAL_CODE, id),
      String.format(A_COUNTRY, id),
      String.format(A_LATITUDE, id),
      String.format(A_LONGITUDE, id),
      String.format(A_MOBILE_PHONE, id),
      String.format(A_PHONE, id),
      String.format(A_NUMBER_OF_PEOPLE, id),
      String.format(A_NUMBER_OF_ROOMS, id),
      String.format(A_SHORT_DESCRIPTION, id),
      String.format(A_LONG_DESCRIPTION, id),
      String.format(SOME_ACTIVITIES, id));
  }
}
