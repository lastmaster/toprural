package es.master.portalrural.domain.model;

import es.master.portalrural.infrastructure.repository.database.entities.Alojamiento;
import es.master.portalrural.infrastructure.repository.database.entities.BookedDate;
import es.master.portalrural.infrastructure.repository.database.entities.Fotografia;
import es.master.portalrural.infrastructure.repository.database.entities.PrecioCaracteristica;

import java.util.Collection;

public class TestAccommodationData
{
  private final Long id;
  private final String name;
  private final String regNumber;
  private final int accommodationType;
  private final String web;
  private final String facebook;
  private final String twitter;
  private final String instagram;
  private final String youtube;
  private final String province;
  private final String city;
  private final String address;
  private final String postalCode;
  private final String country;
  private final String latitude;
  private final String longitude;
  private final String mobilePhone;
  private final String phone;
  private final String numberOfPeople;
  private final String numberOfRooms;
  private final String shortDescription;
  private final String longDescription;
  private final String activities;

  

  public TestAccommodationData(Long id, String name, String regNumber, int accommodationType, String web, String facebook,
		String twitter, String instagram, String youtube, String province, String city, String address, String postalCode,
		String country, String latitude, String longitude,String mobilePhone, String phone,
		String numberOfPeople, String numberOfRooms, String shortDescription, String longDescription,
		String activities) {
	super();
	this.id = id;
	this.name = name;
	this.regNumber = regNumber;
	this.accommodationType = accommodationType;
	this.web = web;
	this.facebook = facebook;
	this.twitter = twitter;
	this.instagram = instagram;
	this.youtube = youtube;
	this.province = province;
	this.city = city;
	this.postalCode = postalCode;
	this.country = country;
	this.latitude = latitude;
	this.longitude = longitude;
	this.address = address;
	this.mobilePhone = mobilePhone;
	this.phone = phone;
	this.numberOfPeople = numberOfPeople;
	this.numberOfRooms = numberOfRooms;
	this.shortDescription = shortDescription;
	this.longDescription = longDescription;
	this.activities = activities;
}

public Accommodation getAccommodation(
    Collection<? extends AccommodationFeature> features,
    Collection<? extends Picture> pictures,
    Collection<AccommodationCalendar> accommodationCalendars)
  {
    return new Accommodation(id,
      name,
      regNumber,
      accommodationType,
      web,
      facebook,
      twitter,
      instagram,
      youtube,
      province,
      city,
      address,
      postalCode,
      country,
      latitude,
      longitude,
      mobilePhone,
      phone,
      numberOfPeople,
      numberOfRooms,
      shortDescription,
      longDescription,
      activities,
      features,
      pictures,
      accommodationCalendars);
  }

  public AccommodationReduced getAccommodationReduced()
  {
    return new AccommodationReduced(id, name);
  }

  public Alojamiento getAlojamiento(Collection<PrecioCaracteristica> precioCaracteristicas,
                                    Collection<Fotografia> fotografias,
                                    Collection<BookedDate> bookedDates)
  {
    Alojamiento alojamiento = new Alojamiento();
    alojamiento.setId(id);
    alojamiento.setName(name);
    alojamiento.setNumberTourism(regNumber);
    alojamiento.setKindAccommodation(accommodationType);
    alojamiento.setWeb(web);
    alojamiento.setFacebook(facebook);
    alojamiento.setTwitter(twitter);
    alojamiento.setInstagram(instagram);
    alojamiento.setYoutube(youtube);
    alojamiento.setProvince(province);
    alojamiento.setVillage(city);
    alojamiento.setAddress(address);
    alojamiento.setPostalCode(postalCode);
    alojamiento.setCountry(country);
    alojamiento.setLatitude(latitude);
    alojamiento.setLongitude(longitude);
    alojamiento.setMobilePhone(mobilePhone);
    alojamiento.setLandline(phone);
    alojamiento.setPeople(numberOfPeople);
    alojamiento.setNumberRooms(numberOfRooms);
    alojamiento.setShortDescription(shortDescription);
    alojamiento.setLongDescription(longDescription);
    alojamiento.setActivities(activities);
    alojamiento.setListFeatures(precioCaracteristicas);
    alojamiento.setListPictures(fotografias);
    alojamiento.setBookedDates(bookedDates);
    return alojamiento;
  }
}
