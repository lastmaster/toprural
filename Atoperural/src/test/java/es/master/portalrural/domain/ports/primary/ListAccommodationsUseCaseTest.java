package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.AccommodationReduced;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static es.master.portalrural.domain.model.TestAcommodationDataGenerator.generate;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ListAccommodationsUseCaseTest
{
  private static AccommodationReduced accommodation1 = generate(1L).getAccommodationReduced();
  private static AccommodationReduced accommodation2 = generate(2L).getAccommodationReduced();
  private static AccommodationReduced accommodation3 = generate(3L).getAccommodationReduced();

  @Mock
  private AccommodationRepository accommodationRepository;

  private ListAccommodationsUseCase listAccommodationsUseCase;

  @Before
  public void setUp()
  {
    listAccommodationsUseCase = new ListAccommodationsUseCase(accommodationRepository);
  }

  @Test
  public void successfulExecution()
  {
    when(accommodationRepository.findAll()).thenReturn(Arrays.asList(accommodation1, accommodation2, accommodation3));

    assertThat(listAccommodationsUseCase.execute(), containsInAnyOrder(accommodation1, accommodation2, accommodation3));
  }
}