package es.master.portalrural.domain.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TestOwnerDataGenerator
{
  private static final String NAME_AND_SURNAME = ":name_and_surname_%d:";
  private static final String AN_EMAIL = "user_%d@example.com";
  private static final String A_PASSWORD = ":password_%d:";
  private static final String A_PHONE_NUM = "+3490000000_%d";
  private static final String A_DESCRIPTION = ":description_%d:";
  private static final String AN_ID_CARD_NUMBER = ":id_card_number_%d:";
  private static final String AN_ADDRESS = ":address_%d:";
  private static final String A_SUBSCRIPTION_STATUS = ":subscription_status_%d:";
  private static final String A_START_DATE = ":start_date_%d:";
  private static final String AN_END_DATE = ":end_date_%d:";
  private static final String A_SERVICE_PLAN = ":service_plan_%d:";

  public static TestOwnerData generate(long id)
  {
    return generate(id, id, Collections.<Accommodation>emptyList());
  }

  public static TestOwnerData generate(long id, long seed, Collection<Accommodation> accommodations)
  {
    return new TestOwnerData(
      id,
      String.format(NAME_AND_SURNAME, seed),
      String.format(AN_EMAIL, seed),
      String.format(A_PASSWORD, seed),
      String.format(A_PHONE_NUM, seed),
      String.format(A_DESCRIPTION, seed),
      String.format(AN_ID_CARD_NUMBER, seed),
      String.format(AN_ADDRESS, seed),
      String.format(A_SUBSCRIPTION_STATUS, seed),
      String.format(A_START_DATE, seed),
      String.format(AN_END_DATE, seed),
      String.format(A_SERVICE_PLAN, seed),
      accommodations
    );
  }

  public static TestOwnerDataCollection generateCollection(int size)
  {
    List<TestOwnerData> dataList = new ArrayList<>();
    for (int i = 0 ; i < size ; i++)
    {
      dataList.add(generate(i));
    }
    return new TestOwnerDataCollection(dataList);
  }
}
