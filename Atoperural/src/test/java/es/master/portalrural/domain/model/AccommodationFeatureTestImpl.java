package es.master.portalrural.domain.model;

import java.math.BigDecimal;

public class AccommodationFeatureTestImpl extends AccommodationFeature
{
  private final Long featureId;
  private final ScaledBigDecimal price;

  public AccommodationFeatureTestImpl(Long featureId, BigDecimal price)
  {
    this.featureId = featureId;
    this.price = ScaledBigDecimal.of(price);
  }

  @Override
  public Long getFeatureId()
  {
    return featureId;
  }

  @Override
  public ScaledBigDecimal getPrice()
  {
    return price;
  }

}
