package es.master.portalrural.domain.model;

import java.util.ArrayList;
import java.util.Collection;

public class TestOwnerDataCollection
{
  private final Collection<TestOwnerData> testOwnerDataCollection;

  public TestOwnerDataCollection(
    Collection<TestOwnerData> testOwnerDataCollection)
  {
    this.testOwnerDataCollection = testOwnerDataCollection;
  }

  public Collection<Owner> getOwners() {
    ArrayList<Owner> owners = new ArrayList<>();
    for (TestOwnerData testOwnerData : testOwnerDataCollection) {
      owners.add(testOwnerData.getOwner());
    }
    return owners;
  }

  public Collection<OwnerInfo> getOwnerInfos() {
    ArrayList<OwnerInfo> owners = new ArrayList<>();
    for (TestOwnerData testOwnerData : testOwnerDataCollection) {
      owners.add(testOwnerData.getOwnerInfo());
    }
    return owners;
  }

  public Collection<OwnerReduced> getOwnersReduced() {
    ArrayList<OwnerReduced> owners = new ArrayList<>();
    for (TestOwnerData testOwnerData : testOwnerDataCollection) {
      owners.add(testOwnerData.getOwnerReduced());
    }
    return owners;
  }
}
