package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationFeatureTestImpl;
import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.model.OwnerInfo;
import es.master.portalrural.domain.model.PictureTestImpl;
import es.master.portalrural.domain.model.TestAcommodationDataGenerator;
import es.master.portalrural.domain.model.TestOwnerDataGenerator;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateOwnerInfoUseCaseTest
{
  private static final Accommodation accommodation1 = TestAcommodationDataGenerator.generate(1L).getAccommodation(
    Collections.<AccommodationFeatureTestImpl>emptyList(), Collections.<PictureTestImpl>emptyList(), Collections.<AccommodationCalendar>emptyList());
  private static final Accommodation accommodation2 = TestAcommodationDataGenerator.generate(2L).getAccommodation(
    Collections.<AccommodationFeatureTestImpl>emptyList(), Collections.<PictureTestImpl>emptyList(), Collections.<AccommodationCalendar>emptyList());
  private static final Accommodation accommodation3 = TestAcommodationDataGenerator.generate(3L).getAccommodation(
    Collections.<AccommodationFeatureTestImpl>emptyList(), Collections.<PictureTestImpl>emptyList(), Collections.<AccommodationCalendar>emptyList());

  private static final Collection<Accommodation> existingAccommodations = Arrays.asList(accommodation1,
    accommodation2,
    accommodation3);

  private static final Long AN_OWNER_ID = 10L;
  private static final Owner existingOwner = TestOwnerDataGenerator.generate(AN_OWNER_ID, 5555L, existingAccommodations).getOwner();

  private static final OwnerInfo updatedOwnerInfo = TestOwnerDataGenerator.generate(AN_OWNER_ID).getOwnerInfo();

  private static final Owner expectedUpdatedOwner = new Owner(updatedOwnerInfo, existingAccommodations);

  @Mock
  private OwnerRepository ownerRepository;

  private UpdateOwnerInfoUseCase updateOwnerInfoUseCase;

  @Before
  public void setUp()
  {
    updateOwnerInfoUseCase = new UpdateOwnerInfoUseCase(ownerRepository);
  }

  @Test
  public void successfulExecution()
  {
    when(ownerRepository.get(AN_OWNER_ID)).thenReturn(existingOwner);

    assertThat(updateOwnerInfoUseCase.execute(updatedOwnerInfo), is(expectedUpdatedOwner));
  }
}