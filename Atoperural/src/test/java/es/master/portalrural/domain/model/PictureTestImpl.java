package es.master.portalrural.domain.model;

public class PictureTestImpl extends Picture
{
  private final Long id;
  private final String nombreFoto;
  private final String descripcion;
  private final String tamano;
  private final String tipoFichero;
  private final int orden;

  public PictureTestImpl(long id, String nombreFoto, String descripcion, String tamano, String tipoFichero, int orden)
  {
    this.id = id;
    this.nombreFoto = nombreFoto;
    this.descripcion = descripcion;
    this.tamano = tamano;
    this.tipoFichero = tipoFichero;
    this.orden = orden;
  }

  @Override
  public Long getId()
  {
    return id;
  }

  @Override
  public String getNombreFoto()
  {
    return nombreFoto;
  }

  @Override
  public String getDescripcion()
  {
    return descripcion;
  }

  @Override
  public String getTamano()
  {
    return tamano;
  }

  @Override
  public String getTipoFichero()
  {
    return tipoFichero;
  }

  @Override
  public int getOrden()
  {
    return orden;
  }

}
