package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.AccommodationFeature;
import es.master.portalrural.domain.model.Picture;
import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationFeatureTestImpl;
import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.domain.model.PictureTestImpl;
import es.master.portalrural.domain.model.TestAcommodationDataGenerator;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Date;

import static java.math.BigDecimal.valueOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ShowAccommodationUseCaseTest
{
  private static final Long AN_ACCOMMODATION_ID = 20L;

  private static final Picture picture1 = new PictureTestImpl(1L, "nombre1", "desc1", "tamanio1", "tipo1", 1);
  private static final Picture picture2 = new PictureTestImpl(2L, "nombre2", "desc2", "tamanio2", "tipo2", 2);
  private static final Picture picture3 = new PictureTestImpl(3L, "nombre3", "desc3", "tamanio3", "tipo3", 3);

  private static final AccommodationFeature feature1 = new AccommodationFeatureTestImpl(1L, valueOf(1));
  private static final AccommodationFeature feature2 = new AccommodationFeatureTestImpl(2L, valueOf(2));
  private static final AccommodationFeature feature3 = new AccommodationFeatureTestImpl(3L, valueOf(3));

  // Instantiate a Date object
  private static final Date today = new Date();

  //2nd way: current time and date using SimpleDateFormat
  //private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  //System.out.println("Today's date is: "+dateFormat.format(date));
  //Wed Jan 28 02:50:23 IST 1970
  private static final Date date2 = new Date(2323223232L);
  @SuppressWarnings("deprecation")
  private static final Date date3 = new Date(2019, 05, 28);
  
  private static final AccommodationCalendar accommodationCalendar1 = new AccommodationCalendar(1L, today, today.toString());
  private static final AccommodationCalendar accommodationCalendar2 = new AccommodationCalendar(1L, date2, date2.toString());
  private static final AccommodationCalendar accommodationCalendar3 = new AccommodationCalendar(1L, date3, date3.toString());
  private static final Accommodation accommodation = TestAcommodationDataGenerator.generate(AN_ACCOMMODATION_ID)
    .getAccommodation(
      Arrays.asList(feature1, feature2, feature3),
      Arrays.asList(picture1, picture2, picture3),
      Arrays.asList(accommodationCalendar1, accommodationCalendar2, accommodationCalendar3)
    );


  @Mock
  private AccommodationRepository accommodationRepository;

  private ShowAccommodationUseCase showAccommodationUseCase;

  @Before
  public void setUp()
  {
    showAccommodationUseCase = new ShowAccommodationUseCase(accommodationRepository);
  }

  @Test
  public void successfulExxecution()
  {
    when(accommodationRepository.get(AN_ACCOMMODATION_ID)).thenReturn(accommodation);

    assertThat(showAccommodationUseCase.execute(AN_ACCOMMODATION_ID), is(accommodation));
  }
}