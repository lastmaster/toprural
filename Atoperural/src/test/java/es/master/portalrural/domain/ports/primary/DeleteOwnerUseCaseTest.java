package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.OwnerReduced;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DeleteOwnerUseCaseTest
{
  private static final Long AN_ID = 99L;
  private static final String A_NAME = ":owner_name:";

  @Mock
  private OwnerRepository ownerRepository;

  private DeleteOwnerUseCase deleteOwnerUseCase;

  @Before
  public void setUp()
  {
    deleteOwnerUseCase = new DeleteOwnerUseCase(ownerRepository);
  }

  @Test
  public void successfulExecution()
  {
    when(ownerRepository.delete(AN_ID)).thenReturn(expectedOwnerReduced());

    assertThat(deleteOwnerUseCase.execute(AN_ID), is(expectedOwnerReduced()));
  }

  private static OwnerReduced expectedOwnerReduced()
  {
    return new OwnerReduced(AN_ID, A_NAME);
  }
}