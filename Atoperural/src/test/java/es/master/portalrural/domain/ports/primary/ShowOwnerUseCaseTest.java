package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static es.master.portalrural.domain.model.TestOwnerDataGenerator.generate;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ShowOwnerUseCaseTest
{
  private static final Long AN_ID = 6L;

  private static final Owner expectedOwner = generate(AN_ID).getOwner();

  @Mock
  private OwnerRepository ownerRepository;

  private ShowOwnerUseCase showOwnerUseCase;

  @Before
  public void setUp()
  {
    showOwnerUseCase = new ShowOwnerUseCase(ownerRepository);
  }

  @Test
  public void successfulExecution()
  {
    when(ownerRepository.get(AN_ID)).thenReturn(expectedOwner);

    assertThat(showOwnerUseCase.execute(AN_ID), is(expectedOwner));
  }

}