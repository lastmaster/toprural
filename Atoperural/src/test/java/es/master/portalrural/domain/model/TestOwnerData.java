package es.master.portalrural.domain.model;

import java.util.Collection;

public class TestOwnerData
{
  private final Owner owner;
  private final OwnerInfo ownerInfo;
  private final OwnerReduced ownerReduced;

  public Owner getOwner()
  {
    return owner;
  }

  public OwnerInfo getOwnerInfo()
  {
    return ownerInfo;
  }

  public OwnerReduced getOwnerReduced()
  {
    return ownerReduced;
  }


  public TestOwnerData(Long anId,
                       String nameAndSurname,
                       String anEmail,
                       String aPassword,
                       String aPhoneNum,
                       String aDescription,
                       String anIdCardNumber,
                       String anAddress,
                       String aSubscriptionStatus,
                       String aStartDate,
                       String anEndDate,
                       String aServicePlan,
                       Collection<Accommodation> accommodations)
  {
    ownerInfo = new OwnerInfo(anId,
      nameAndSurname,
      anEmail,
      aPassword,
      aPhoneNum,
      aDescription,
      anIdCardNumber,
      anAddress,
      aSubscriptionStatus,
      aStartDate,
      anEndDate,
      aServicePlan);
    owner = new Owner(ownerInfo, accommodations);
    ownerReduced = new OwnerReduced(anId, nameAndSurname);
  }
}
