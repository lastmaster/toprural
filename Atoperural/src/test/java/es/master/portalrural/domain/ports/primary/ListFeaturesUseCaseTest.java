package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Feature;
import es.master.portalrural.domain.ports.secondary.FeatureRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ListFeaturesUseCaseTest
{
  private static final Feature feature1 = new Feature(1L, "desc1");
  private static final Feature feature2 = new Feature(2L, "desc2");
  private static final Feature feature3 = new Feature(3L, "desc3");

  @Mock
  private FeatureRepository featureRepository;

  private ListFeaturesUseCase listFeaturesUseCase;

  @Before
  public void setUp()
  {
    listFeaturesUseCase = new ListFeaturesUseCase(featureRepository);
  }

  @Test
  public void successfulExecution()
  {
    when(featureRepository.findAll()).thenReturn(Arrays.asList(feature1, feature2, feature3));

    assertThat(listFeaturesUseCase.execute(), containsInAnyOrder(feature1, feature2, feature3));
  }

}