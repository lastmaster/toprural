package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.OwnerReduced;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static es.master.portalrural.domain.model.TestOwnerDataGenerator.generate;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ListOwnersUseCaseTest
{
  private static OwnerReduced owner1 = generate(1L).getOwnerReduced();
  private static OwnerReduced owner2 = generate(2L).getOwnerReduced();
  private static OwnerReduced owner3 = generate(3L).getOwnerReduced();

  @Mock
  private OwnerRepository ownerRepository;

  private ListOwnersUseCase listOwnersUseCase;

  @Before
  public void setUp()
  {
    listOwnersUseCase = new ListOwnersUseCase(ownerRepository);
  }

  @Test
  public void successfulExecution()
  {
    when(ownerRepository.findAll()).thenReturn(Arrays.asList(owner1, owner2, owner3));

    assertThat(listOwnersUseCase.execute(), containsInAnyOrder(owner1, owner2, owner3));
  }
}