package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.model.OwnerInfo;
import es.master.portalrural.domain.model.OwnerReduced;
import es.master.portalrural.domain.model.TestOwnerData;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static es.master.portalrural.domain.model.TestOwnerDataGenerator.generate;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddOwnerUseCaseTest
{
  private static TestOwnerData testOwnerData = generate(0L);

  @Mock
  private OwnerRepository ownerRepository;

  private AddOwnerUseCase addOwnerUseCase;

  @Before
  public void setUp()
  {
    addOwnerUseCase = new AddOwnerUseCase(ownerRepository);
  }

  @Test
  public void successfulExecution()
  {
    when(ownerRepository.save(anOwner())).thenReturn(expectedOwnerReduced());

    assertThat(addOwnerUseCase.execute(anOwnerInfo()), is(expectedOwnerReduced()));
  }

  private static OwnerInfo anOwnerInfo()
  {
    return testOwnerData.getOwnerInfo();
  }

  private static Owner anOwner()
  {
    return testOwnerData.getOwner();
  }

  private OwnerReduced expectedOwnerReduced()
  {
    return testOwnerData.getOwnerReduced();
  }
}