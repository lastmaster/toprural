package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.AccommodationFeature;
import es.master.portalrural.domain.model.Picture;
import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationReduced;
import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddAccommodationUseCaseTest
{
  private static final Long AN_OWNER_ID = 3L;
  private static final Long AN_ACCOMMODATION_ID = 6L;
  private static final String A_NAME = ":name:";
  private static final String A_REGISTRATION_NUMBER = ":reg_number:";
  private static final int AN_ACCOMMODATION_TYPE = 10;
  private static final String A_WEB = ":web:";
  private static final String A_FACEBOOK = ":facebook:";
  private static final String A_TWITTER = ":twitter:";
  private static final String AN_INSTAGRAM = ":instagram:";
  private static final String A_YOUTUBE = ":youtube:";
  private static final String A_PROVINCE = ":province:";
  private static final String A_CITY = ":city:";
  private static final String AN_ADDRESS = ":address:";
  private static final String A_POSTAL_CODE = ":postalCode:";
  private static final String A_COUNTRY = ":country:";
  private static final String A_LATITUDE = ":latitude:";
  private static final String A_LONGITUDE = ":longitude:";
  private static final String A_MOBILE_PHONE = ":mobile_phone:";
  private static final String A_PHONE = ":phone:";
  private static final String A_NUMBER_OF_PEOPLE = ":num_people:";
  private static final String A_NUMBER_OF_ROOMS = ":num_rooms:";
  private static final String A_SHORT_DESCRIPTION = ":short_desc:";
  private static final String A_LONG_DESCRIPTION = ":long_desc:";
  private static final String SOME_ACTIVITIES = ":activities:";
  private static final Collection<AccommodationFeature> FEATURES = Collections.emptyList();
  private static final Collection<Picture> PICTURES = Collections.emptyList();
  private static final Collection<AccommodationCalendar> CALENDAR = Collections.emptyList();
  @Mock
  private AccommodationRepository accommodationRepository;

  private AddAccommodationUseCase addAccommodationUseCase;

  @Before
  public void setUp()
  {
    addAccommodationUseCase = new AddAccommodationUseCase(accommodationRepository);
  }

  @Test
  public void successfulExecution()
  {
    when(accommodationRepository.saveAssigningOwner(anAccommodation(), AN_OWNER_ID)).thenReturn(anAccommodation());

    assertThat(addAccommodationUseCase.execute(anAccommodation(), AN_OWNER_ID), is(expectedAccommodation()));
  }

  private static Accommodation anAccommodation()
  {
    return new Accommodation(AN_ACCOMMODATION_ID,
      A_NAME,
      A_REGISTRATION_NUMBER,
      AN_ACCOMMODATION_TYPE,
      A_WEB,
      A_FACEBOOK,
      A_TWITTER,
      AN_INSTAGRAM,
      A_YOUTUBE,
      A_PROVINCE,
      A_CITY,
      AN_ADDRESS,
      A_POSTAL_CODE,
      A_COUNTRY,
      A_LATITUDE,
      A_LONGITUDE,
      A_MOBILE_PHONE,
      A_PHONE,
      A_NUMBER_OF_PEOPLE,
      A_NUMBER_OF_ROOMS,
      A_SHORT_DESCRIPTION,
      A_LONG_DESCRIPTION,
      SOME_ACTIVITIES,
      FEATURES,
      PICTURES,
      CALENDAR
    );
  }

  private static AccommodationReduced expectedAccommodation()
  {
    return new AccommodationReduced(AN_ACCOMMODATION_ID, A_NAME);
  }
}