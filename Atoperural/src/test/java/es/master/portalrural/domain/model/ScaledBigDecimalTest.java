package es.master.portalrural.domain.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class ScaledBigDecimalTest
{

  @Test
  public void sameValueButDifferentScale() {
    ScaledBigDecimal scaledBigDecimal1 = ScaledBigDecimal.of(BigDecimal.ONE.setScale(4));
    ScaledBigDecimal scaledBigDecimal2 = ScaledBigDecimal.of(BigDecimal.ONE.setScale(0));

    assertThat(scaledBigDecimal1, is(scaledBigDecimal2));
  }
}