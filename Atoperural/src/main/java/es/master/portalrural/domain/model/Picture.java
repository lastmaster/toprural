package es.master.portalrural.domain.model;

import java.util.Objects;

public abstract class Picture
{
  public abstract Long getId();

  public abstract String getNombreFoto();

  public abstract String getDescripcion();

  public abstract String getTamano();

  public abstract String getTipoFichero();

  public abstract int getOrden();

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || !(o instanceof Picture))
    {
      return false;
    }
    Picture picture = (Picture) o;
    return getId() == picture.getId() &&
      getOrden() == picture.getOrden() &&
      Objects.equals(getNombreFoto(), picture.getNombreFoto()) &&
      Objects.equals(getDescripcion(), picture.getDescripcion()) &&
      Objects.equals(getTamano(), picture.getTamano()) &&
      Objects.equals(getTipoFichero(), picture.getTipoFichero());
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(getId(), getNombreFoto(), getDescripcion(), getTamano(), getTipoFichero(), getOrden());
  }
}
