package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;

public class ShowAccommodationUseCase
{
  private final AccommodationRepository accommodationRepository;

  public ShowAccommodationUseCase(AccommodationRepository accommodationRepository)
  {
    this.accommodationRepository = accommodationRepository;
  }

  public Accommodation execute(Long accommodationId)
  {
    return accommodationRepository.get(accommodationId);
  }
}
