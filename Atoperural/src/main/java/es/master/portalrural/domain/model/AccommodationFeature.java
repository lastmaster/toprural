package es.master.portalrural.domain.model;

import java.util.Objects;

public abstract class AccommodationFeature
{
  public abstract Long getFeatureId();

  public abstract ScaledBigDecimal getPrice();

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || !(o instanceof AccommodationFeature))
    {
      return false;
    }
    AccommodationFeature that = (AccommodationFeature) o;
    return Objects.equals(getFeatureId(), that.getFeatureId()) &&
      Objects.equals(getPrice(), that.getPrice());
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(getFeatureId(), getPrice());
  }
}
