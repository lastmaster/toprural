package es.master.portalrural.domain.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class ScaledBigDecimal
{
  private static final int SCALE = 2;
  private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;

  private final BigDecimal value;

  private ScaledBigDecimal(BigDecimal value)
  {
    this.value = value.setScale(SCALE, ROUNDING_MODE);
  }

  public double doubleValue()
  {
    return value.doubleValue();
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || getClass() != o.getClass())
    {
      return false;
    }
    ScaledBigDecimal that = (ScaledBigDecimal) o;
    return Objects.equals(value, that.value);
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(value);
  }

  @Override
  public String toString()
  {
    return value.toString();
  }

  public static ScaledBigDecimal of(BigDecimal value)
  {
    return new ScaledBigDecimal(value);
  }

}
