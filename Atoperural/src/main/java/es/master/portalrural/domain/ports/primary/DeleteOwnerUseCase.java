package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.OwnerReduced;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;

public class DeleteOwnerUseCase
{
  private final OwnerRepository ownerRepository;

  public DeleteOwnerUseCase(OwnerRepository ownerRepository)
  {
    this.ownerRepository = ownerRepository;
  }

  public OwnerReduced execute(long id)
  {
    return ownerRepository.delete(id);
  }
}
