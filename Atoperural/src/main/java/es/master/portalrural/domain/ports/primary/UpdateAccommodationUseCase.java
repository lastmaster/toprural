package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;

public class UpdateAccommodationUseCase
{
  private final AccommodationRepository accommodationRepository;

  public UpdateAccommodationUseCase(AccommodationRepository accommodationRepository)
  {
    this.accommodationRepository = accommodationRepository;
  }

  public Accommodation execute(Accommodation accommodation)
  {
    //
    // TODO fail if accommodation does NOT exist
    //

    return accommodationRepository.savePreservingOwner(accommodation);
  }
}
