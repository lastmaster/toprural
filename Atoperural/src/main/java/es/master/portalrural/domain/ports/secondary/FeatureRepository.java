package es.master.portalrural.domain.ports.secondary;

import es.master.portalrural.domain.model.Feature;

import java.util.Collection;

public interface FeatureRepository
{
  Collection<Feature> findAll();

  Feature get(Long featureId);
}
