package es.master.portalrural.domain.model;

import java.util.Objects;

public class Feature
{
  private final Long id;
  private final String description;

  public Feature(Long id, String description)
  {
    this.id = id;
    this.description = description;
  }

  public Long getId()
  {
    return id;
  }

  public String getDescription()
  {
    return description;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || getClass() != o.getClass())
    {
      return false;
    }
    Feature feature = (Feature) o;
    return Objects.equals(id, feature.id) &&
      Objects.equals(description, feature.description);
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(id, description);
  }

  @Override
  public String toString()
  {
    return "Feature{" +
      "id=" + id +
      ", description='" + description + '\'' +
      '}';
  }
}
