package es.master.portalrural.domain.model;

import java.util.Date;

public class AccommodationCalendar {
	private final Long id;
	private final Date bookedDayFormatted;
	private final String bookedDay;
	//bookedDateFormatted;
	  
	public AccommodationCalendar(Long id, Date bookedDayFormatted, String bookedDay) {
		super();
		this.id = id;
		this.bookedDayFormatted = bookedDayFormatted;
		this.bookedDay = bookedDay;
	}
	
	public Long getId() {
		return id;
	}

	public Date getBookedDayFormatted() {
		return bookedDayFormatted;
	}
	
	public String getBookedDay() {
		return bookedDay;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookedDay == null) ? 0 : bookedDay.hashCode());
		result = prime * result + ((bookedDayFormatted == null) ? 0 : bookedDayFormatted.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccommodationCalendar other = (AccommodationCalendar) obj;
		if (bookedDay == null) {
			if (other.bookedDay != null)
				return false;
		} else if (!bookedDay.equals(other.bookedDay))
			return false;
		if (bookedDayFormatted == null) {
			if (other.bookedDayFormatted != null)
				return false;
		} else if (!bookedDayFormatted.equals(other.bookedDayFormatted))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "AccommodationCalendar [" + (id != null ? "id=" + id + ", " : "")
				+ (bookedDayFormatted != null ? "bookedDayFormatted=" + bookedDayFormatted + ", " : "") 
				+ (bookedDay != null ? "bookedDay=" + bookedDay : "") +"]";
	}


}


