package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.model.OwnerInfo;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;

public class UpdateOwnerInfoUseCase
{
  private final OwnerRepository ownerRepository;

  public UpdateOwnerInfoUseCase(OwnerRepository ownerRepository)
  {
    this.ownerRepository = ownerRepository;
  }

  public Owner execute(OwnerInfo ownerInfo)
  {
    Owner storedOwner = ownerRepository.get(ownerInfo.getId());

    Owner ownerWithUpdatedOwnerInfo = new Owner(
      ownerInfo,
      storedOwner.getListaAlojamientos());

    ownerRepository.save(ownerWithUpdatedOwnerInfo);

    return ownerWithUpdatedOwnerInfo;
  }
}
