package es.master.portalrural.domain.model;

import java.util.Collection;
import java.util.Objects;

public class Owner
{
  private final OwnerInfo ownerInfo;

  private final Collection<Accommodation> listaAlojamientos;

  public Owner(OwnerInfo ownerInfo, Collection<Accommodation> listaAlojamientos)
  {
    this.ownerInfo = ownerInfo;
    this.listaAlojamientos = listaAlojamientos;
  }

  public Owner(Long id, String nombreYapellidos, String email, String contraseña, String telefono,
               String descripcion, String dni, String direccion, String estadoSubscripcion, String fechaInicio,
               String fechaFin, String tipoServicio, Collection<Accommodation> listaAlojamientos)
  {
    this(new OwnerInfo(
        id,
        nombreYapellidos,
        email,
        contraseña,
        telefono,
        descripcion,
        dni,
        direccion,
        estadoSubscripcion,
        fechaInicio,
        fechaFin,
        tipoServicio),
      listaAlojamientos);
  }

  public Long getId()
  {
    return ownerInfo.getId();
  }

  public String getNombreYapellidos()
  {
    return ownerInfo.getNombreYapellidos();
  }

  public String getEmail()
  {
    return ownerInfo.getEmail();
  }

  public String getContrasena()
  {
    return ownerInfo.getContrasena();
  }

  public String getTelefono()
  {
    return ownerInfo.getTelefono();
  }

  public String getDescripcion()
  {
    return ownerInfo.getDescripcion();
  }

  public String getDni()
  {
    return ownerInfo.getDni();
  }

  public String getDireccion()
  {
    return ownerInfo.getDireccion();
  }

  public String getEstadoSubscripcion()
  {
    return ownerInfo.getEstadoSubscripcion();
  }

  public String getFechaInicio()
  {
    return ownerInfo.getFechaInicio();
  }

  public String getFechaFin()
  {
    return ownerInfo.getFechaFin();
  }

  public String getTipoServicio()
  {
    return ownerInfo.getTipoServicio();
  }

  public Collection<Accommodation> getListaAlojamientos()
  {
    return listaAlojamientos;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || getClass() != o.getClass())
    {
      return false;
    }
    Owner owner = (Owner) o;
    return Objects.equals(ownerInfo, owner.ownerInfo) &&
      Objects.equals(listaAlojamientos, owner.listaAlojamientos);
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(ownerInfo, listaAlojamientos);
  }

  @Override
  public String toString()
  {
    return "Owner{" +
      "ownerInfo=" + ownerInfo +
      ", listaAlojamientos=" + listaAlojamientos +
      '}';
  }
}
