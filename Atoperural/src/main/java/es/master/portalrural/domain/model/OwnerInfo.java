package es.master.portalrural.domain.model;

import java.util.Objects;

public class OwnerInfo
{
  private final Long id;

  //Datos personales
  private final String nombreYapellidos;

  private final String email;
  private final String contrasena;
  private final String telefono;
  private final String descripcion;

  //Datos de facturación
  private final String dni;
  private final String direccion;

  //Datos de la subscripción
  private String estadoSubscripcion = "activa";
  private String fechaInicio = "01-01-2019";
  private String fechaFin = "31-12-2030";
  private String tipoServicio = "silver";

  public OwnerInfo(Long id, String nombreYapellidos, String email, String contraseña, String telefono,
                   String descripcion, String dni, String direccion, String estadoSubscripcion, String fechaInicio,
                   String fechaFin, String tipoServicio)
  {
    this.id = id;
    this.nombreYapellidos = nombreYapellidos;
    this.email = email;
    this.contrasena = contraseña;
    this.telefono = telefono;
    this.descripcion = descripcion;
    this.dni = dni;
    this.direccion = direccion;
    this.estadoSubscripcion = estadoSubscripcion;
    this.fechaInicio = fechaInicio;
    this.fechaFin = fechaFin;
    this.tipoServicio = tipoServicio;
  }

  public Long getId()
  {
    return id;
  }

  public String getNombreYapellidos()
  {
    return nombreYapellidos;
  }

  public String getEmail()
  {
    return email;
  }

  public String getContrasena()
  {
    return contrasena;
  }

  public String getTelefono()
  {
    return telefono;
  }

  public String getDescripcion()
  {
    return descripcion;
  }

  public String getDni()
  {
    return dni;
  }

  public String getDireccion()
  {
    return direccion;
  }

  public String getEstadoSubscripcion()
  {
    return estadoSubscripcion;
  }

  public String getFechaInicio()
  {
    return fechaInicio;
  }

  public String getFechaFin()
  {
    return fechaFin;
  }

  public String getTipoServicio()
  {
    return tipoServicio;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || getClass() != o.getClass())
    {
      return false;
    }
    OwnerInfo ownerInfo = (OwnerInfo) o;
    return Objects.equals(id, ownerInfo.id) &&
      Objects.equals(nombreYapellidos, ownerInfo.nombreYapellidos) &&
      Objects.equals(email, ownerInfo.email) &&
      Objects.equals(contrasena, ownerInfo.contrasena) &&
      Objects.equals(telefono, ownerInfo.telefono) &&
      Objects.equals(descripcion, ownerInfo.descripcion) &&
      Objects.equals(dni, ownerInfo.dni) &&
      Objects.equals(direccion, ownerInfo.direccion) &&
      Objects.equals(estadoSubscripcion, ownerInfo.estadoSubscripcion) &&
      Objects.equals(fechaInicio, ownerInfo.fechaInicio) &&
      Objects.equals(fechaFin, ownerInfo.fechaFin) &&
      Objects.equals(tipoServicio, ownerInfo.tipoServicio);
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(id, nombreYapellidos, email, contrasena, telefono, descripcion, dni, direccion,
      estadoSubscripcion, fechaInicio, fechaFin, tipoServicio);
  }

  @Override
  public String toString()
  {
    return "OwnerInfo{" +
      "id=" + id +
      ", nombreYapellidos='" + nombreYapellidos + '\'' +
      ", email='" + email + '\'' +
      ", contrasena='" + contrasena + '\'' +
      ", telefono='" + telefono + '\'' +
      ", descripcion='" + descripcion + '\'' +
      ", dni='" + dni + '\'' +
      ", direccion='" + direccion + '\'' +
      ", estadoSubscripcion='" + estadoSubscripcion + '\'' +
      ", fechaInicio='" + fechaInicio + '\'' +
      ", fechaFin='" + fechaFin + '\'' +
      ", tipoServicio='" + tipoServicio + '\'' +
      '}';
  }
}