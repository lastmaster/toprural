package es.master.portalrural.domain.model;

import java.util.Collection;
import java.util.Objects;

public class Accommodation
{
  private final AccommodationInfo accommodationInfo;

  private final Collection<? extends AccommodationFeature> listFeatures;

  private final Collection<? extends Picture> listPictures;

  private final Collection<AccommodationCalendar> listAccommodationCalendar;

  public Accommodation(AccommodationInfo accommodationInfo,
                       Collection<? extends AccommodationFeature> listFeatures,
                       Collection<? extends Picture> listaFotografias,
                       Collection<AccommodationCalendar> listAccomodationCalendar)
  {
    this.accommodationInfo = accommodationInfo;
    this.listFeatures = listFeatures;
    this.listPictures = listaFotografias;
    this.listAccommodationCalendar = listAccomodationCalendar;
  }

  public Accommodation(Long id, String name, String numberTourism, int kindAccommodation,
                       String web, String facebook, String twitter, String instagram, String youtube,
                       String province, String village, String address, String postalCode, String country, 
                       String latitude, String longitude, String mobilePhone, String landline, 
                       String people, String numberRooms, String shortDescription, String longDescription, String activities,
                       Collection<? extends AccommodationFeature> listFeatures,
                       Collection<? extends Picture> listPictures,
                       Collection<AccommodationCalendar> listAccommodationCalendar)
  {

    this(new AccommodationInfo(
        id,
        name,
        numberTourism,
        kindAccommodation,
        web,
        facebook,
        twitter,
        instagram,
        youtube,
        province,
        village,
        address,
        postalCode,
        country,
        latitude,
        longitude,
        mobilePhone,
        landline,
        people,
        numberRooms,
        shortDescription,
        longDescription,
        activities),
      listFeatures,
      listPictures,
      listAccommodationCalendar);
  }

  public Long getId()
  {
    return accommodationInfo.getId();
  }

  public String getNumberTourism()
  {
    return accommodationInfo.getNumberTourism();
  }

  public int getKindAccommodation()
  {
    return accommodationInfo.getKindAccommodation();
  }

  public String getWeb()
  {
    return accommodationInfo.getWeb();
  }

  public String getFacebook()
  {
    return accommodationInfo.getFacebook();
  }

  public String getTwitter()
  {
    return accommodationInfo.getTwitter();
  }

  public String getInstagram()
  {
    return accommodationInfo.getInstagram();
  }

  public String getYoutube()
  {
    return accommodationInfo.getYoutube();
  }

  public String getProvince()
  {
    return accommodationInfo.getProvince();
  }

  public String getVillage()
  {
    return accommodationInfo.getVillage();
  }

  public String getAddress()
  {
    return accommodationInfo.getAddress();
  }

  public String getPostalCode()
  {
    return accommodationInfo.getPostalCode();
  }
  
  public String getCountry()
  {
    return accommodationInfo.getCountry();
  }
  
  public String getLatitude()
  {
    return accommodationInfo.getLatitude();
  }
  
  public String getLongitude()
  {
    return accommodationInfo.getLongitude();
  }
  
  public String getMobilePhone()
  {
    return accommodationInfo.getMobilePhone();
  }

  public String getLandline()
  {
    return accommodationInfo.getLandline();
  }

  public String getPeople()
  {
    return accommodationInfo.getPeople();
  }

  public String getShortDescription()
  {
    return accommodationInfo.getShortDescription();
  }

  public String getLongDescription()
  {
    return accommodationInfo.getLongDescription();
  }

  public String getActivities()
  {
    return accommodationInfo.getActivities();
  }

  public String getNumberRooms()
  {
    return accommodationInfo.getNumberRooms();
  }

  public String getName()
  {
    return accommodationInfo.getName();
  }

  public Collection<? extends AccommodationFeature> getListFeatures()
  {
    return listFeatures;
  }

  public Collection<? extends Picture> getListPictures()
  {
    return listPictures;
  }

  public Collection<AccommodationCalendar> getListAccommodationCalendar()
  {
    return listAccommodationCalendar;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || getClass() != o.getClass())
    {
      return false;
    }
    Accommodation that = (Accommodation) o;
    return Objects.equals(accommodationInfo, that.accommodationInfo) &&
      Objects.equals(listFeatures, that.listFeatures) &&
      Objects.equals(listPictures, that.listPictures) &&
      Objects.equals(listAccommodationCalendar, that.listAccommodationCalendar);
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(accommodationInfo, listFeatures, listPictures, listAccommodationCalendar);
  }

  @Override
  public String toString()
  {
    return "Accommodation{" +
      "accommodationInfo=" + accommodationInfo +
      ", listFeatures=" + listFeatures +
      ", listPictures=" + listPictures +
      ", listAccommodationCalendar=" + listAccommodationCalendar +
      '}';
  }
}
