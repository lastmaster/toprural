package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationReduced;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;

public class AddAccommodationUseCase
{
  private final AccommodationRepository accommodationRepository;

  public AddAccommodationUseCase(AccommodationRepository accommodationRepository)
  {
    this.accommodationRepository = accommodationRepository;
  }

  public AccommodationReduced execute(Accommodation accommodation, Long ownerId)
  {
    //
    // TODO fail if accommodation ALREADY exists
    //

    Accommodation savedAccommodation = accommodationRepository.saveAssigningOwner(accommodation, ownerId);
    return new AccommodationReduced(savedAccommodation.getId(), savedAccommodation.getName());
  }
}
