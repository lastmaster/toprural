package es.master.portalrural.domain.ports.secondary;

import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.model.OwnerReduced;

import java.util.Collection;

public interface OwnerRepository
{
  Collection<OwnerReduced> findAll();

  OwnerReduced save(Owner owner);

  Owner get(Long ownerId);

  OwnerReduced delete(long id);
}
