package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.AccommodationReduced;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;

import java.util.Collection;

public class ListAccommodationsUseCase
{
  private final AccommodationRepository accomodationRepository;

  public ListAccommodationsUseCase(AccommodationRepository accommodationRepository)
  {
    this.accomodationRepository = accommodationRepository;
  }

  public Collection<AccommodationReduced> execute()
  {
    return accomodationRepository.findAll();
  }
}
