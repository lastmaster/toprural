package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.OwnerReduced;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;

import java.util.Collection;

public class ListOwnersUseCase
{
  private final OwnerRepository ownerRepository;

  public ListOwnersUseCase(OwnerRepository ownerRepository) {
    this.ownerRepository = ownerRepository;
  }

  public Collection<OwnerReduced> execute()
  {
    return ownerRepository.findAll();
  }
}
