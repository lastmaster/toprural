package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.model.OwnerInfo;
import es.master.portalrural.domain.model.OwnerReduced;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;

import java.util.Collections;

public class AddOwnerUseCase
{
  private final OwnerRepository ownerRepository;

  public AddOwnerUseCase(OwnerRepository ownerRepository)
  {
    this.ownerRepository = ownerRepository;
  }

  public OwnerReduced execute(OwnerInfo ownerInfo)
  {
    return ownerRepository.save(new Owner(ownerInfo, Collections.<Accommodation>emptyList()));
  }
}
