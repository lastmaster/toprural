package es.master.portalrural.domain.model;

import java.util.Objects;

/**
 * @author eseogaz
 *
 */
public class AccommodationInfo {
	private final Long id;

	private final String name;
	private final String numberTourism;
	private final int kindAccommodation;
	private final String web;
	private final String facebook;
	private final String twitter;
	private final String instagram;
	private final String youtube;

	private final String province;
	private final String village;
	private final String address;
	private final String postalCode;
	private final String country;
	private final String latitude;
	private final String longitude;

	private final String mobilePhone;
	private final String landline;

	private final String people;
	private final String numberRooms;

	private final String shortDescription;
	private final String longDescription;
	private final String activities;

	public AccommodationInfo(Long id, String name, String numberTourism, int kindAccommodation, String web,
			String facebook, String twitter, String instagram, String youtube, String province, String village,
			String address, String postalCode, String country, String latitude, String longitude, String mobilePhone,
			String landline, String people, String numberRooms, String shortDescription, String longDescription,
			String activities) {
		super();
		this.id = id;
		this.name = name;
		this.numberTourism = numberTourism;
		this.kindAccommodation = kindAccommodation;
		this.web = web;
		this.facebook = facebook;
		this.twitter = twitter;
		this.instagram = instagram;
		this.youtube = youtube;
		this.province = province;
		this.village = village;
		this.address = address;
		this.postalCode = postalCode;
		this.country = country;
		this.latitude = latitude;
		this.longitude = longitude;
		this.mobilePhone = mobilePhone;
		this.landline = landline;
		this.people = people;
		this.numberRooms = numberRooms;
		this.shortDescription = shortDescription;
		this.longDescription = longDescription;
		this.activities = activities;
	}

	public Long getId() {
		return id;
	}

	public String getNumberTourism() {
		return numberTourism;
	}

	public int getKindAccommodation() {
		return kindAccommodation;
	}

	public String getWeb() {
		return web;
	}

	public String getFacebook() {
		return facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public String getInstagram() {
		return instagram;
	}

	public String getYoutube() {
		return youtube;
	}

	public String getProvince() {
		return province;
	}

	public String getVillage() {
		return village;
	}

	public String getAddress() {
		return address;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getCountry() {
		return country;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public String getLandline() {
		return landline;
	}

	public String getPeople() {
		return people;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public String getActivities() {
		return activities;
	}

	public String getNumberRooms() {
		return numberRooms;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AccommodationInfo that = (AccommodationInfo) o;
		return kindAccommodation == that.kindAccommodation && Objects.equals(id, that.id)
				&& Objects.equals(name, that.name) && Objects.equals(numberTourism, that.numberTourism)
				&& Objects.equals(web, that.web) && Objects.equals(facebook, that.facebook)
				&& Objects.equals(twitter, that.twitter) && Objects.equals(instagram, that.instagram)
				&& Objects.equals(youtube, that.youtube) && Objects.equals(province, that.province)
				&& Objects.equals(village, that.village) && Objects.equals(address, that.address)
				&& Objects.equals(postalCode, that.postalCode) && Objects.equals(country, that.country)
				&& Objects.equals(latitude, that.latitude) && Objects.equals(longitude, that.longitude)
				&& Objects.equals(mobilePhone, that.mobilePhone) && Objects.equals(landline, that.landline)
				&& Objects.equals(people, that.people) && Objects.equals(numberRooms, that.numberRooms)
				&& Objects.equals(shortDescription, that.shortDescription)
				&& Objects.equals(longDescription, that.longDescription) && Objects.equals(activities, that.activities);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, numberTourism, kindAccommodation, web, facebook, twitter, instagram, youtube,
				province, village, address, postalCode, country, latitude, longitude, mobilePhone, landline, people,
				numberRooms, shortDescription, longDescription, activities);
	}

	@Override
	public String toString() {
		return "AccommodationInfo{" + "id=" + id + ", name='" + name + '\'' + ", numberTourism='" + numberTourism + '\''
				+ ", kindAccommodation=" + kindAccommodation + ", web='" + web + '\'' + ", facebook='" + facebook + '\''
				+ ", twitter='" + twitter + '\'' + ", instagram='" + instagram + '\'' + ", youtube='" + youtube + '\''
				+ ", province='" + province + '\'' + ", village='" + village + '\'' + ", address='" + address + '\''
				+ ", postalCode='" + postalCode + '\'' + ", country='" + country + '\'' + ", latitude='" + latitude
				+ '\'' + ", longitude='" + longitude + '\'' + ", mobilePhone='" + mobilePhone + '\'' + ", landline='"
				+ landline + '\'' + ", people='" + people + '\'' + ", numberRooms='" + numberRooms + '\''
				+ ", shortDescription='" + shortDescription + '\'' + ", longDescription='" + longDescription + '\''
				+ ", activities='" + activities + '\'' + '}';
	}
}
