package es.master.portalrural.domain.model;

public class OwnerReduced
{
  private long id;

  private final String fullName;

  public OwnerReduced(long id, String fullName)
  {
    this.id = id;
    this.fullName = fullName;
  }

  public long getId()
  {
    return id;
  }

  public String getFullName()
  {
    return fullName;
  }

  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + (int) (id ^ (id >>> 32));
    result = prime * result + ((fullName == null) ? 0 : fullName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
    {
      return true;
    }
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    OwnerReduced other = (OwnerReduced) obj;
    if (id != other.id)
    {
      return false;
    }
    if (fullName == null)
    {
      if (other.fullName != null)
      {
        return false;
      }
    }
    else if (!fullName.equals(other.fullName))
    {
      return false;
    }
    return true;
  }

  @Override
  public String toString()
  {
    return "Propietario [id=" + id + ", "
      + (fullName != null ? "fullName=" + fullName : "")
      + "]";
  }
}
