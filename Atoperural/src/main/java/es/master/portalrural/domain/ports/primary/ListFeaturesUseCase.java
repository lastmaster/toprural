package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Feature;
import es.master.portalrural.domain.ports.secondary.FeatureRepository;

import java.util.Collection;

public class ListFeaturesUseCase
{
  private final FeatureRepository featureRepository;

  public ListFeaturesUseCase(FeatureRepository featureRepository)
  {
    this.featureRepository = featureRepository;
  }

  public Collection<Feature> execute()
  {
    return featureRepository.findAll();
  }
}
