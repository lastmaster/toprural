package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.AccommodationReduced;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;

public class DeleteAccommodationUseCase
{
  private final AccommodationRepository accommodationRepository;

  public DeleteAccommodationUseCase(AccommodationRepository accommodationRepository)
  {
    this.accommodationRepository = accommodationRepository;
  }

  public AccommodationReduced execute(Long id)
  {
    return accommodationRepository.delete(id);
  }
}
