package es.master.portalrural.domain.ports.primary;

import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;

public class ShowOwnerUseCase
{
  private final OwnerRepository ownerRepository;

  public ShowOwnerUseCase(OwnerRepository ownerRepository)
  {
    this.ownerRepository = ownerRepository;
  }

  public Owner execute(long id)
  {
    return ownerRepository.get(id);
  }
}
