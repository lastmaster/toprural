package es.master.portalrural.domain.ports.secondary;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationReduced;

import java.util.Collection;

public interface AccommodationRepository
{
  Collection<AccommodationReduced> findAll();

  Accommodation get(Long accommodationId);

  Accommodation savePreservingOwner(Accommodation accommodation);

  Accommodation saveAssigningOwner(Accommodation accommodation, Long ownerId);

  AccommodationReduced delete(Long id);

}
