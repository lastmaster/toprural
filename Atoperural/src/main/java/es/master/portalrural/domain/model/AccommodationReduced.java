package es.master.portalrural.domain.model;

public class AccommodationReduced
{
  private final Long id;

  private final String name;

  public AccommodationReduced(Long id, String name)
  {
    this.id = id;
    this.name = name;
  }

  public long getId() {
    return id;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (int) (id ^ (id >>> 32));
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AccommodationReduced other = (AccommodationReduced) obj;
    if (id != other.id)
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "AccommodationReduced [id=" + id + ", " + (name != null ? "name=" + name : "") + "]";
  }
}
