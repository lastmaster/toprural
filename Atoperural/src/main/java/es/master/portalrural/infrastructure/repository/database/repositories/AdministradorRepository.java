package es.master.portalrural.infrastructure.repository.database.repositories;


import es.master.portalrural.infrastructure.repository.database.entities.Administrador;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AdministradorRepository extends JpaRepository<Administrador, Long> {

	
}
