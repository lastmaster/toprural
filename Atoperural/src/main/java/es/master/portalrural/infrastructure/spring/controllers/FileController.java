package es.master.portalrural.infrastructure.spring.controllers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;


@Controller
@RequestMapping("/controller")
public class FileController {

	
	ConcurrentLinkedQueue<FileMeta> files = new ConcurrentLinkedQueue<FileMeta>();
	//LinkedList<FileMeta> files = new LinkedList<FileMeta>();
	FileMeta fileMeta = null;
	/***************************************************
	 * URL: /upload  
	 * upload(): receives files
	 * @param request : MultipartHttpServletRequest auto passed
	 * @param response : HttpServletResponse auto passed
	 * @return ConcurrentLinkedQueue<FileMeta> as json format
	 ****************************************************/
	@RequestMapping(value="/upload", method = RequestMethod.POST)
	public @ResponseBody ConcurrentLinkedQueue<FileMeta> upload(MultipartHttpServletRequest request, HttpServletResponse response) {
		//TODO: Permitir la subida múltiple de fotos, que no funciona. Subes dos ficheros y te sube dos copias del mismo. 
		
		//0. Vaciar el contenedor de imágenes (porque sino no me vuelve a cargar todas).
		//   Si hago esto sólo puedo subir una foto a la vez
		//files.clear();
		//TODO: Jugar con el repositorio de imágenes desde aqui para ver como controlo esto
		//TODO: El FileMeta debería llevar los mismos campos que Fotografía (así en base de datos tendríamos todo) 
		
		//1. build an iterator
		 Iterator<String> itr =  request.getFileNames();
		 MultipartFile mpf = null;

		 //2. get each file
		 while(itr.hasNext()){
			 
			 //2.1 get next MultipartFile
			 mpf = request.getFile(itr.next()); 
			 System.out.println(mpf.getOriginalFilename() +" uploaded! "+files.size());

			 //2.2 if files > 10 remove the first from the list
			 if(files.size() >= 10)
			 {
				 //files.pop(); //Este para LinkedList
				 files.poll();
			 }
			 
			 //2.3 create new fileMeta
			 fileMeta = new FileMeta();
			 fileMeta.setFileName(mpf.getOriginalFilename());
			 fileMeta.setFileSize(mpf.getSize()/1024+" Kb");
			 fileMeta.setFileType(mpf.getContentType());
			 
			 try {
				fileMeta.setBytes(mpf.getBytes());
				
				// copy file to local disk (make sure the path "e.g. C:/temp/" exists)
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream("C:/Users/eseogaz/master/toprural/Atoperural/src/main/resources/static/pruebas/image/gallery/"+mpf.getOriginalFilename()));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 //2.4 add to files
			 files.add(fileMeta);
			 
		 }
		 
		// result will be like this
		// [{"fileName":"app_engine-85x77.png","fileSize":"8 Kb","fileType":"image/png"},...]
		 System.out.println("Files size: " + files.size());
		 Iterator<FileMeta> iter = files.iterator();
		 while(iter.hasNext()){
			 
		    System.out.println(iter.next().toString());

		 }
		return files;
 
	}
 
}
