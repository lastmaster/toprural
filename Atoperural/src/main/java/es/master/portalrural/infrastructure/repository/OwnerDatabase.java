package es.master.portalrural.infrastructure.repository;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.model.OwnerReduced;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;
import es.master.portalrural.infrastructure.repository.database.entities.Alojamiento;
import es.master.portalrural.infrastructure.repository.database.entities.Propietario;
import es.master.portalrural.infrastructure.repository.database.repositories.PropietarioRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class OwnerDatabase implements OwnerRepository
{
  private final PropietarioRepository propietarioRepository;
  private final AccommodationConverter accommodationConverter;

  public OwnerDatabase(PropietarioRepository propietarioRepository,
                       AccommodationConverter accommodationConverter)
  {
    this.propietarioRepository = propietarioRepository;
    this.accommodationConverter = accommodationConverter;
  }

  @Override
  public Collection<OwnerReduced> findAll()
  {
    Collection<Propietario> propietarios = propietarioRepository.findAll();
    List<OwnerReduced> owners = new ArrayList<>();
    for (Propietario propietario : propietarios)
    {
      owners.add(convertToOwnerReduced(propietario));
    }
    return owners;
  }

  @Override
  public OwnerReduced save(Owner owner)
  {
    Propietario propietario = propietarioRepository.save(convert(owner));
    return convertToOwnerReduced(propietario);
  }

  @Override
  public Owner get(Long ownerId)
  {
    return convertToOwner(propietarioRepository.getOne(ownerId));
  }

  @Override
  public OwnerReduced delete(long id)
  {
    Propietario propietarioToRemove = propietarioRepository.getOne(id);
    propietarioRepository.delete(id);
    return convertToOwnerReduced(propietarioToRemove);
  }

  private Owner convertToOwner(Propietario propietario)
  {
    return new Owner(propietario.getId(),
      propietario.getNombreYapellidos(),
      propietario.getEmail(),
      propietario.getContrasena(),
      propietario.getTelefono(),
      propietario.getDescripcion(),
      propietario.getDni(),
      propietario.getDireccion(),
      propietario.getEstadoSubscripcion(),
      propietario.getFechaInicio(),
      propietario.getFechaFin(),
      propietario.getTipoServicio(),
      accommodationConverter.from(propietario.getListaAlojamientos()));
  }

  private static Propietario convert(Owner owner)
  {
    Propietario propietario = new Propietario();
    propietario.setId(owner.getId());
    propietario.setNombreYapellidos(owner.getNombreYapellidos());
    propietario.setEmail(owner.getEmail());
    propietario.setContrasena(owner.getContrasena());
    propietario.setTelefono(owner.getTelefono());
    propietario.setDescripcion(owner.getDescripcion());
    propietario.setDni(owner.getDni());
    propietario.setDireccion(owner.getDireccion());
    propietario.setEstadoSubscripcion(owner.getEstadoSubscripcion());
    propietario.setFechaInicio(owner.getFechaInicio());
    propietario.setFechaFin(owner.getFechaFin());
    propietario.setTipoServicio(owner.getTipoServicio());
    return propietario;
  }

  private static OwnerReduced convertToOwnerReduced(Propietario propietario)
  {
    return new OwnerReduced(
      propietario.getId(),
      propietario.getNombreYapellidos()
    );
  }
}
