package es.master.portalrural.infrastructure.spring;

import es.master.portalrural.infrastructure.repository.database.entities.Caracteristica;
import es.master.portalrural.infrastructure.repository.database.repositories.CaracteristicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;

@Component
public class CaracteristicasPopulator implements ApplicationRunner
{
  @Autowired
  private CaracteristicaRepository caracteristicaRepository;

  @Override
  public void run(ApplicationArguments applicationArguments) throws Exception
  {
    Collection<Caracteristica> caracteristicas = Arrays.asList(
      new Caracteristica("Barbacoa"),
      new Caracteristica("Jardín"),
      new Caracteristica("Piscina"),
      new Caracteristica("Zona de aparcamiento"),
      new Caracteristica("Granja"),
      new Caracteristica("Precio fin de semana - Temporada baja"),
      new Caracteristica("Precio semana - Temporada baja"),
      new Caracteristica("Precio fin de semana - Temporada media"),
      new Caracteristica("Precio semana - Temporada media"),
      new Caracteristica("Precio fin de semana - Temporada alta"),
      new Caracteristica("Precio semana - Temporada alta")
    );

    caracteristicaRepository.save(caracteristicas);
  }
}
