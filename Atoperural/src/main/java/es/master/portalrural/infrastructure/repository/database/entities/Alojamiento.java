package es.master.portalrural.infrastructure.repository.database.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Alojamiento {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;
	private String numberTourism;
	private int kindAccommodation;
	private String web;
	private String facebook;
	private String twitter;
	private String instagram;
	private String youtube;

	private String province;
	private String village;
	private String address;
	private String postalCode;
	private String country;
	private String latitude;
	private String longitude;
	
	private String mobilePhone;
	private String landline;

	private String people;
	private String numberRooms;

	private String shortDescription;
	private String longDescription;
	private String activities;

	@ManyToOne
	private Propietario owner;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<PrecioCaracteristica> listFeatures;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "alojamiento", orphanRemoval = true)
	private Collection<Fotografia> listPictures;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "alojamiento", orphanRemoval = true)
	private Collection<BookedDate> bookedDates;

	public Alojamiento() {

	}

	public Alojamiento(String name) {
		super();
		this.name = name;
	}

	public Alojamiento(long id, String name, String numberTourism) {
		super();
		this.id = id;
		this.name = name;
		this.numberTourism = numberTourism;
	}

	public String getNumberTourism() {
		return numberTourism;
	}

	public void setNumberTourism(String numberTourism) {
		this.numberTourism = numberTourism;
	}

	public int getKindAccommodation() {
		return kindAccommodation;
	}

	public void setKindAccommodation(int kindAccommodation) {
		this.kindAccommodation = kindAccommodation;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getInstagram() {
		return instagram;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

	public String getYoutube() {
		return youtube;
	}

	public void setYoutube(String youtube) {
		this.youtube = youtube;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String provincia) {
		this.province = provincia;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getLandline() {
		return landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getPeople() {
		return people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getActivities() {
		return activities;
	}

	public void setActivities(String activities) {
		this.activities = activities;
	}

	public Propietario getOwner() {
		return owner;
	}

	public void setOwner(Propietario owner) {
		this.owner = owner;
	}

	public String getNumberRooms() {
		return numberRooms;
	}

	public void setNumberRooms(String numberRooms) {
		this.numberRooms = numberRooms;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Collection<PrecioCaracteristica> getListFeatures() {
		return listFeatures;
	}

	public void setListFeatures(Collection<PrecioCaracteristica> listFeatures) {
		this.listFeatures = listFeatures;
	}

	public Collection<Fotografia> getListPictures() {
		return listPictures;
	}

	public void setListPictures(Collection<Fotografia> listPictures) {
		for (Fotografia fotografia : listPictures) {
			fotografia.setAlojamiento(this);
		}
		this.listPictures = listPictures;
	}

	public Collection<BookedDate> getBookedDates() {
		return bookedDates;
	}

	public void setBookedDates(Collection<BookedDate> bookedDates) {
		this.bookedDates = bookedDates;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Alojamiento that = (Alojamiento) o;
		return kindAccommodation == that.kindAccommodation && Objects.equals(id, that.id)
				&& Objects.equals(name, that.name) && Objects.equals(numberTourism, that.numberTourism)
				&& Objects.equals(web, that.web) && Objects.equals(facebook, that.facebook)
				&& Objects.equals(twitter, that.twitter) && Objects.equals(instagram, that.instagram)
				&& Objects.equals(youtube, that.youtube) && Objects.equals(province, that.province)
				&& Objects.equals(village, that.village) && Objects.equals(address, that.address)
				&& Objects.equals(postalCode, that.postalCode) && Objects.equals(country, that.country)
				&& Objects.equals(latitude, that.latitude) && Objects.equals(longitude, that.longitude)
				&& Objects.equals(mobilePhone, that.mobilePhone) && Objects.equals(landline, that.landline)
				&& Objects.equals(people, that.people) && Objects.equals(numberRooms, that.numberRooms)
				&& Objects.equals(shortDescription, that.shortDescription)
				&& Objects.equals(longDescription, that.longDescription) && Objects.equals(activities, that.activities)
				&& Objects.equals(owner, that.owner) && Objects.equals(listFeatures, that.listFeatures)
				&& Objects.equals(listPictures, that.listPictures) && Objects.equals(bookedDates, that.bookedDates);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, numberTourism, kindAccommodation, web, facebook, twitter, instagram, youtube,
				province, village, address, postalCode, country, latitude, longitude, mobilePhone, landline, people, numberRooms, shortDescription,
				longDescription, activities, owner, listFeatures, listPictures, bookedDates);
	}
}
