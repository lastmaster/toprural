package es.master.portalrural.infrastructure.repository.database.entities;

import es.master.portalrural.domain.model.Picture;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Fotografia extends Picture
{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nombreFoto;
	private String tamano = "0";
	private String descripcion = "sin descripción";
	private String tipoFichero = "";
	private int orden;

	@ManyToOne
	private Alojamiento alojamiento;

	public Fotografia() {

	}

	public Fotografia(String nombreFoto) {
		super();
		this.nombreFoto = nombreFoto;
	}

	public Fotografia(String nombreFoto, String tamano, String descripcion) {
		super();
		this.nombreFoto = nombreFoto;
		this.tamano = tamano;
		this.descripcion = descripcion;
	}



	@Override
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getNombreFoto() {
		return nombreFoto;
	}


	public void setNombreFoto(String nombre) {
		this.nombreFoto = nombre;
	}

	@Override
	public String getTamano() {
		return tamano;
	}


	public void setTamano(String tamano) {
		this.tamano = tamano;
	}

	@Override
	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String getTipoFichero() {
		return tipoFichero;
	}

	public void setTipoFichero(String tipoFichero) {
		this.tipoFichero = tipoFichero;
	}

	@Override
	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public Alojamiento getAlojamiento()
	{
		return alojamiento;
	}

	public void setAlojamiento(Alojamiento alojamiento)
	{
		this.alojamiento = alojamiento;
	}

	@Override
	public String toString() {
		return "Fotografia [id=" + id + ", " + (nombreFoto != null ? "nombreFoto=" + nombreFoto + ", " : "")
				+ (tamano != null ? "tamano=" + tamano + ", " : "")
				+ (descripcion != null ? "descripcion=" + descripcion + ", " : "")
				+ (tipoFichero != null ? "tipoFichero=" + tipoFichero + ", " : "") + "orden=" + orden + "]";
	}
}


