package es.master.portalrural.infrastructure.webform;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AccommodationCalendarFromWebForm
{
  private Long id;

  private String bookedDate;
  
  private Date bookedDateFormatted;

  public void setId(Long id)
  {
    this.id = id;
  }

  public Long getId()
  {
    return id;
  }

  public String getBookedDate() 
  {
	return bookedDate;
  }

  public void setBookedDate(String bookedDate) 
  {      
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      Date dateToBook;
      try {
         dateToBook = dateFormat.parse(bookedDate);
         System.out.println(dateToBook);
         setBookedDateFormatted(dateToBook);
      } catch (ParseException e) {
         System.err.println("Unparseable using " + dateFormat);
      }
	  this.bookedDate = bookedDate;
  }
  
  public Date getBookedDateFormatted()
  {
	return bookedDateFormatted;
  }

  public void setBookedDateFormatted(Date bookedDateFormatted) 
  {
	this.bookedDateFormatted = bookedDateFormatted;
  }

@Override
  public String toString()
  {
    return "AccomodationCalendarFromWebForm{" +
      "id=" + id +
      ", bookedDate='" + bookedDate + '\'' +
      ", bookedDateFormatted='" + bookedDateFormatted + '\'' +
      '}';
  }
}
