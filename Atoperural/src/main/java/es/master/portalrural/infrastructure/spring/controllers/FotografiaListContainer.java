package es.master.portalrural.infrastructure.spring.controllers;

import es.master.portalrural.infrastructure.repository.database.entities.Fotografia;

import java.util.List;

public class FotografiaListContainer {
	private List<Fotografia> fotografias;
	
	public List<Fotografia> getFotografias() {
	    return fotografias;
	}
	
	public void setFotografias(List<Fotografia> fotografias) {
	    this.fotografias = fotografias;
	}
	
	public boolean isEmpty() {
		if (fotografias == null)
			return true;
		return fotografias.isEmpty();
	}
	
	public int size() {
		if (fotografias == null)
			return 0;
		return fotografias.size();
	}
}

