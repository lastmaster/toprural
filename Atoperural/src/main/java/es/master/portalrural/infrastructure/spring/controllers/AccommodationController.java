package es.master.portalrural.infrastructure.spring.controllers;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.domain.model.AccommodationReduced;
import es.master.portalrural.domain.model.Feature;
import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.ports.primary.AddAccommodationUseCase;
import es.master.portalrural.domain.ports.primary.DeleteAccommodationUseCase;
import es.master.portalrural.domain.ports.primary.ListFeaturesUseCase;
import es.master.portalrural.domain.ports.primary.ShowAccommodationUseCase;
import es.master.portalrural.domain.ports.primary.UpdateAccommodationUseCase;
import es.master.portalrural.infrastructure.webform.AccommodationConverter;
import es.master.portalrural.infrastructure.webform.AccommodationFromWebForm;
import es.master.portalrural.infrastructure.webform.FeatureToShow;
import es.master.portalrural.infrastructure.webform.FeaturesToShowConverter;
import es.master.portalrural.infrastructure.webform.SanitizedAccommodation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

@Controller
public class AccommodationController
{
  private final AddAccommodationUseCase addAccommodationUseCase;
  private final ShowAccommodationUseCase showAccommodationUseCase;
  private final DeleteAccommodationUseCase deleteAccommodationUseCase;
  private final UpdateAccommodationUseCase updateAccommodationUseCase;
  private final ListFeaturesUseCase listFeaturesUseCase;
  private final FeaturesToShowConverter featuresConverter;

  private final AccommodationConverter accommodationConverter = new AccommodationConverter();

  public AccommodationController(
    AddAccommodationUseCase addAccommodationUseCase,
    ShowAccommodationUseCase showAccommodationUseCase,
    DeleteAccommodationUseCase deleteAccommodationUseCase,
    UpdateAccommodationUseCase updateAccommodationUseCase,
    ListFeaturesUseCase listFeaturesUseCase,
    FeaturesToShowConverter featuresConverter)
  {
    this.addAccommodationUseCase = addAccommodationUseCase;
    this.showAccommodationUseCase = showAccommodationUseCase;
    this.deleteAccommodationUseCase = deleteAccommodationUseCase;
    this.updateAccommodationUseCase = updateAccommodationUseCase;
    this.listFeaturesUseCase = listFeaturesUseCase;
    this.featuresConverter = featuresConverter;
  }

  @GetMapping("/propietario/{idPropietario}/alojamiento/{idAlojamiento}/editar")
  public String editarAlojamiento(Model model, @PathVariable long idPropietario, @PathVariable long idAlojamiento)
  {

    Accommodation accommodation = showAccommodationUseCase.execute(idAlojamiento);

    Collection<FeatureToShow> featuresToShow = featuresConverter.convert(accommodation.getListFeatures(), "all");
    
    
    model.addAttribute("alojamiento", sanitize(accommodation));
    System.out.println("editarAlojamiento: " + accommodation.toString());
    model.addAttribute("listfeatures", featuresToShow);
    model.addAttribute("listpictures", accommodation.getListPictures());
    model.addAttribute("listAccommodationCalendar", sanitizeCalendar(accommodation.getListAccommodationCalendar()));
    model.addAttribute("idPropietario", idPropietario);
    return "editar_alojamiento";
  }


  @PostMapping("/propietario/{idPropietario}/alojamiento/{idAlojamiento}/borrar")
  public String borrarAlojamiento(Model model, @PathVariable long idPropietario, @PathVariable long idAlojamiento)
  {

    deleteAccommodationUseCase.execute(idAlojamiento);
    model.addAttribute("idPropietario", idPropietario);

    return "alojamiento_borrado";
  }

  @PostMapping("/propietario/{idPropietario}/alojamiento/{idAlojamiento}/actualizar")
  public String actualizarAlojamiento(Model model, 
		                              @PathVariable long idPropietario, 
		                              @PathVariable long idAlojamiento,
                                      AccommodationFromWebForm alojamiento)
  {
    alojamiento.setId(idAlojamiento);
    System.out.println("fechas ocupadas (actualizar): " + alojamiento.getCalendars());
    updateAccommodationUseCase.execute(
      accommodationConverter.convert(alojamiento)
    );
    
    Accommodation accommodation = showAccommodationUseCase.execute(idAlojamiento);

    Collection<FeatureToShow> featuresToShow = featuresConverter.convert(accommodation.getListFeatures(), "all");
    
    model.addAttribute("alojamiento", sanitize(accommodation));
    System.out.println("editarAlojamiento: " + accommodation.toString());
    model.addAttribute("listfeatures", featuresToShow);
    model.addAttribute("listpictures", accommodation.getListPictures());
    model.addAttribute("listAccommodationCalendar", sanitizeCalendar(accommodation.getListAccommodationCalendar()));
    model.addAttribute("idPropietario", idPropietario);
    
    return "alojamiento_actualizado";
  }

  @PostMapping("/propietario/{idPropietario}/alojamiento/nuevo")
  public String nuevoAlojamientoPropietario(Model model,
		  									AccommodationFromWebForm accommodationFromWebForm,
                                            @PathVariable long idPropietario)
  {
    System.out.println("features: " + accommodationFromWebForm.getFeatures());
    System.out.println("fechas ocupadas (nuevo): " + accommodationFromWebForm.getCalendars());
    AccommodationReduced accommodationReduced = addAccommodationUseCase.execute(
      accommodationConverter.convert(accommodationFromWebForm),
      idPropietario
    );
    
    Accommodation accommodation = showAccommodationUseCase.execute(accommodationReduced.getId());

    Collection<FeatureToShow> featuresToShow = featuresConverter.convert(accommodation.getListFeatures(), "all");
    model.addAttribute("alojamiento", sanitize(accommodation));
    System.out.println("editarAlojamiento: " + accommodation.toString());
    model.addAttribute("listfeatures", featuresToShow);
    model.addAttribute("listpictures", accommodation.getListPictures());
    model.addAttribute("listAccommodationCalendar", sanitizeCalendar(accommodation.getListAccommodationCalendar()));
    
    model.addAttribute("idPropietario", idPropietario);
    return "alojamiento_guardado";
  }

  @GetMapping("nuevo_alojamiento")
  public String formularioNuevoAlojamiento(Model model)
  {
    Collection<Feature> features = listFeaturesUseCase.execute();
    model.addAttribute("features", features);
    return "nuevo_alojamiento";
  }
  
  @GetMapping("/alojamiento/{id}")
  public String verAlojamiento(Model model, @PathVariable long id)
  {

    Accommodation accommodation = showAccommodationUseCase.execute(id);

    Collection<FeatureToShow> featuresToShowPrice = featuresConverter.convert(accommodation.getListFeatures(), "price");
    Collection<FeatureToShow> featuresToShowCharacteristics = featuresConverter.convert(accommodation.getListFeatures(), "characteristics");
    
    model.addAttribute("alojamiento", sanitize(accommodation));
    System.out.println("verAlojamiento (Huesped): " + accommodation.toString());
    model.addAttribute("listprices", featuresToShowPrice);
    model.addAttribute("listcharacteristics", featuresToShowCharacteristics);
    model.addAttribute("listpictures", accommodation.getListPictures());
    model.addAttribute("listAccommodationCalendar", sanitizeCalendar(accommodation.getListAccommodationCalendar()));
    return "ver_galeria_alojamiento";
  }

  private static SanitizedAccommodation sanitize(Accommodation accommodation)
  {
    return new SanitizedAccommodation(accommodation);
  }
  
  private static Collection<AccommodationCalendar> sanitizeCalendar(Collection<AccommodationCalendar> aCollection) 
  {
	  if (aCollection == null)
	  {
		  return Collections.<AccommodationCalendar>emptyList();
	  }
	  else
	  {
		  Collection<AccommodationCalendar> calendarConvert = new ArrayList<AccommodationCalendar>();
		  for (AccommodationCalendar it : aCollection) 
		  { 
			  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		      String dateBookedString = it.getBookedDay().substring(0, 10);
		      
		      System.out.println(dateBookedString);
			  
			  Date dateBooked = new Date();
		      try {
		         dateBooked = dateFormat.parse(it.getBookedDay());
		         System.out.println(dateBooked);
		      } catch (ParseException e) {
		         System.err.println("Unparseable using " + dateFormat);
		      }
		      
		  	  calendarConvert.add(new AccommodationCalendar(it.getId(), dateBooked, dateBookedString));
		  }
		  return calendarConvert;  
	  }
  }
}
