package es.master.portalrural.infrastructure.repository;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.infrastructure.repository.database.entities.Alojamiento;

import java.util.Collection;

public class AccommodationConverter
{
  private final AccommodationCalendarsConverter accommodationCalendarsConverter;

  public AccommodationConverter(AccommodationCalendarsConverter accommodationCalendarsConverter)
  {
    this.accommodationCalendarsConverter = accommodationCalendarsConverter;
  }

  public Accommodation from(Alojamiento alojamiento)
  {
    return new Accommodation(alojamiento.getId(),
      alojamiento.getName(),
      alojamiento.getNumberTourism(),
      alojamiento.getKindAccommodation(),
      alojamiento.getWeb(),
      alojamiento.getFacebook(),
      alojamiento.getTwitter(),
      alojamiento.getInstagram(),
      alojamiento.getYoutube(),
      alojamiento.getProvince(),
      alojamiento.getVillage(),
      alojamiento.getAddress(),
      alojamiento.getPostalCode(),
      alojamiento.getCountry(),
      alojamiento.getLatitude(),
      alojamiento.getLongitude(),
      alojamiento.getMobilePhone(),
      alojamiento.getLandline(),
      alojamiento.getPeople(),
      alojamiento.getNumberRooms(),
      alojamiento.getShortDescription(),
      alojamiento.getLongDescription(),
      alojamiento.getActivities(),
      alojamiento.getListFeatures(),
      alojamiento.getListPictures(),
      accommodationCalendarsConverter.from(alojamiento.getBookedDates()));
  }

  public Collection<Accommodation> from(Collection<Alojamiento> listaAlojamientos)
  {
    return new CollectionConverter<>(
      new ElementConverter<Alojamiento, Accommodation>()
      {
        @Override
        public Accommodation convert(Alojamiento element)
        {
          return from(element);
        }
      }
    ).convert(listaAlojamientos);
  }
}
