package es.master.portalrural.infrastructure.spring.config;

import es.master.portalrural.domain.ports.secondary.OwnerRepository;
import es.master.portalrural.infrastructure.repository.AccommodationConverter;
import es.master.portalrural.infrastructure.repository.OwnerDatabase;
import es.master.portalrural.infrastructure.repository.database.repositories.PropietarioRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OwnerRepositoryConfig
{
  @Bean
  public OwnerRepository ownerRepository(PropietarioRepository propietarioRepository,
                                         AccommodationConverter accommodationConverter)
  {
    return new OwnerDatabase(propietarioRepository, accommodationConverter);
  }
}
