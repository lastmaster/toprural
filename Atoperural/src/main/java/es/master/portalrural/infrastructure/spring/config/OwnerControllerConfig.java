package es.master.portalrural.infrastructure.spring.config;

import es.master.portalrural.domain.ports.primary.AddOwnerUseCase;
import es.master.portalrural.domain.ports.primary.DeleteOwnerUseCase;
import es.master.portalrural.domain.ports.primary.ShowOwnerUseCase;
import es.master.portalrural.domain.ports.primary.UpdateOwnerInfoUseCase;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;
import es.master.portalrural.infrastructure.spring.controllers.OwnerController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class OwnerControllerConfig
{
  @Bean
  public OwnerController ownerController(OwnerRepository ownerRepository)
  {
    return new OwnerController(
      new AddOwnerUseCase(ownerRepository),
      new ShowOwnerUseCase(ownerRepository),
      new DeleteOwnerUseCase(ownerRepository),
      new UpdateOwnerInfoUseCase(ownerRepository)
    );
  }
}
