package es.master.portalrural.infrastructure.repository.database.repositories;


import es.master.portalrural.infrastructure.repository.database.entities.Huesped;
import org.springframework.data.jpa.repository.JpaRepository;


public interface HuespedRepository extends JpaRepository<Huesped, Long> {

	
}
