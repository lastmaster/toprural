package es.master.portalrural.infrastructure.webform;

import es.master.portalrural.domain.model.AccommodationFeature;
import es.master.portalrural.domain.model.Feature;
import es.master.portalrural.domain.ports.secondary.FeatureRepository;

import java.util.ArrayList;
import java.util.Collection;

public class FeaturesToShowConverter
{
  private final FeatureRepository featureRepository;

  public FeaturesToShowConverter(FeatureRepository featureRepository)
  {
    this.featureRepository = featureRepository;
  }

  public Collection<FeatureToShow> convert(Collection<? extends AccommodationFeature> accommodationFeatures, String kind)
  {
    Collection<Feature> allFeatures = featureRepository.findAll();
    ArrayList<FeatureToShow> featuresToShow = new ArrayList<>();
    for (Feature feature : allFeatures)
    {
      AccommodationFeature featureInAccommodation = getFeatureInAccommodation(feature, accommodationFeatures);
      if (featureInAccommodation != null)
      {
        featuresToShow.add(flaggedFeatureToShowFrom(feature, featureInAccommodation));
      }
      else
      {
        featuresToShow.add(unflaggedFeatureToShowFrom(feature));
      }
    }
    
    if (!kind.equals("all")) {
    	ArrayList<FeatureToShow> featuresToShowByKind = new ArrayList<>();
        for (FeatureToShow feature : featuresToShow)
        {
        	if (feature.getKind().equals(kind))
        	{
        		featuresToShowByKind.add(feature);
        	}
        }
        return featuresToShowByKind;
    }
    else   	//return all features (owner intranet)
        return featuresToShow;
  }
  
  private static String getKind(Feature feature)
  {
	String kind = "characteristics";
	if (feature.getDescription().startsWith("Precio"))
	{
		kind = "price";
	}
    return kind;
  }

  private static FeatureToShow unflaggedFeatureToShowFrom(Feature feature)
  {
    return new FeatureToShow(false, feature.getId(), feature.getDescription(), "", getKind(feature));
  }

  private static FeatureToShow flaggedFeatureToShowFrom(Feature feature,
                                                        AccommodationFeature featureInAccommodation)
  {
    return new FeatureToShow(true, feature.getId(), feature.getDescription(),
      featureInAccommodation.getPrice().toString(), getKind(feature));
  }

  private static AccommodationFeature getFeatureInAccommodation(Feature feature,
                                                                Collection<? extends AccommodationFeature> accommodationFeatures)
  {
    for (AccommodationFeature accomodationFeature : accommodationFeatures)
    {
      if (accomodationFeature.getFeatureId().equals(feature.getId()))
      {
        return accomodationFeature;
      }
    }
    return null;
  }
}
