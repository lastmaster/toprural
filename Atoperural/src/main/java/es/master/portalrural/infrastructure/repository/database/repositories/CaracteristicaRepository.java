package es.master.portalrural.infrastructure.repository.database.repositories;


import es.master.portalrural.infrastructure.repository.database.entities.Caracteristica;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CaracteristicaRepository extends JpaRepository<Caracteristica, Long> {

	
}
