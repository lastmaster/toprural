package es.master.portalrural.infrastructure.repository.database.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Administrador {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	//Datos
	private String nombreYapellidos;
	private String email;
	private String contrasenia;
	private String telefono;
	private String notas;
	private String role;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Propietario> listaPropietarios;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Huesped> listaHuespedes;

	/*@OneToMany(cascade=CascadeType.ALL)
	private List<Zonas> listaGuiasDeViaje;*/
}
