package es.master.portalrural.infrastructure.spring.config;

import es.master.portalrural.domain.ports.secondary.AccommodationRepository;
import es.master.portalrural.infrastructure.repository.AccommodationConverter;
import es.master.portalrural.infrastructure.repository.AccommodationDatabase;
import es.master.portalrural.infrastructure.repository.database.repositories.AlojamientoRepository;
import es.master.portalrural.infrastructure.repository.database.repositories.BookedDateRepository;
import es.master.portalrural.infrastructure.repository.database.repositories.PropietarioRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AccommodationRepositoryConfig
{
  @Bean
  public AccommodationRepository accommodationRepository(
    AlojamientoRepository alojamientoRepository,
    PropietarioRepository propietarioRepository,
    BookedDateRepository bookedDateRepository,
    AccommodationConverter accommodationConverter)
  {
    return new AccommodationDatabase(alojamientoRepository, propietarioRepository, bookedDateRepository, accommodationConverter);
  }
}
