package es.master.portalrural.infrastructure.webform;

import es.master.portalrural.domain.model.AccommodationFeature;
import es.master.portalrural.domain.model.ScaledBigDecimal;

import java.math.BigDecimal;

public class AccommodationFeatureFromWebForm extends AccommodationFeature implements CheckableItem
{
  private Long id;
  private boolean checked;
  private Double price;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public void setChecked(boolean checked)
  {
    this.checked = checked;
  }

  @Override
  public Long getFeatureId()
  {
    return getId();
  }

  @Override
  public ScaledBigDecimal getPrice()
  {
    return ScaledBigDecimal.of(BigDecimal.valueOf(price));
  }

  public void setPrice(Double price)
  {
    this.price = price;
  }

  @Override
  public boolean isChecked()
  {
    return checked;
  }

  @Override
  public String toString()
  {
    return "AccommodationFeatureFromWebForm{" +
      "id=" + id +
      ", checked=" + checked +
      ", price=" + price +
      '}';
  }
}
