package es.master.portalrural.infrastructure.repository.database.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import java.util.Date;

@Entity
public class BookedDate
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private Date bookedDate;
	
	@ManyToOne
	private Alojamiento alojamiento;
	
	public BookedDate()
	{
	}
	
	public BookedDate(Long id, Date bookedDate) {
		super();
		this.id = id;
		this.bookedDate = bookedDate;
	}
	
	public Long getId()
	{
	  return id;
	}
	
	public void setId(Long id)
	{
	  this.id = id;
	}

	public Date getBookedDate() 
	{
		return bookedDate;
	}
	
	public void setBookedDate(Date bookedDate) 
	{
		this.bookedDate = bookedDate;
	}
	
	public Alojamiento getAlojamiento()
	{
		return alojamiento;
	}

	public void setAlojamiento(Alojamiento alojamiento)
	{
		this.alojamiento = alojamiento;
	}
}