package es.master.portalrural.infrastructure.repository.database.repositories;


import es.master.portalrural.infrastructure.repository.database.entities.Propietario;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PropietarioRepository extends JpaRepository<Propietario, Long> {

	
}
