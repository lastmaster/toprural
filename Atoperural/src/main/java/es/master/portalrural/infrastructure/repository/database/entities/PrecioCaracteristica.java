package es.master.portalrural.infrastructure.repository.database.entities;

import es.master.portalrural.domain.model.AccommodationFeature;
import es.master.portalrural.domain.model.ScaledBigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
public class PrecioCaracteristica extends AccommodationFeature
{

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private Long idCaracteristica;

  private double precio;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Long getIdCaracteristica()
  {
    return idCaracteristica;
  }

  public void setIdCaracteristica(Long idCaracteristica)
  {
    this.idCaracteristica = idCaracteristica;
  }

  public double getPrecio()
  {
    return precio;
  }

  public void setPrecio(double precio)
  {
    this.precio = precio;
  }

  @Override
  public Long getFeatureId()
  {
    return getIdCaracteristica();
  }

  @Override
  public ScaledBigDecimal getPrice()
  {
    return ScaledBigDecimal.of(BigDecimal.valueOf(getPrecio()));
  }
}
