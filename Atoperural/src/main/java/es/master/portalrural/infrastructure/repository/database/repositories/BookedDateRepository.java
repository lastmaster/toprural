package es.master.portalrural.infrastructure.repository.database.repositories;


import es.master.portalrural.infrastructure.repository.database.entities.BookedDate;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BookedDateRepository extends JpaRepository<BookedDate, Long> {

	
}
