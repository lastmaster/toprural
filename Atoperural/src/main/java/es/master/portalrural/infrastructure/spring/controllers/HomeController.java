package es.master.portalrural.infrastructure.spring.controllers;

import es.master.portalrural.domain.model.AccommodationReduced;
import es.master.portalrural.domain.model.OwnerReduced;
import es.master.portalrural.domain.ports.primary.ListAccommodationsUseCase;
import es.master.portalrural.domain.ports.primary.ListOwnersUseCase;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;

@Controller
public class HomeController
{
  private final ListOwnersUseCase listOwnersUseCase;
  private final ListAccommodationsUseCase listAccommodationsUseCase;

  public HomeController(ListOwnersUseCase listOwnersUseCase,
                        ListAccommodationsUseCase listAccommodationsUseCase)
  {
    this.listOwnersUseCase = listOwnersUseCase;
    this.listAccommodationsUseCase = listAccommodationsUseCase;
  }

  @GetMapping("/")
  public String portalrural(Model model, String id)
  {

    Collection<OwnerReduced> owners = listOwnersUseCase.execute();
    Collection<AccommodationReduced> accommodations = listAccommodationsUseCase.execute();

    model.addAttribute("owners", owners);
    model.addAttribute("accommodations", accommodations);

    return "home_portalrural";
  }
}
