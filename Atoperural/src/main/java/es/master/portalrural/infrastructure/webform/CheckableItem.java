package es.master.portalrural.infrastructure.webform;

public interface CheckableItem
{
  boolean isChecked();
}
