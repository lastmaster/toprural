package es.master.portalrural.infrastructure.repository;

import es.master.portalrural.domain.model.Feature;
import es.master.portalrural.domain.ports.secondary.FeatureRepository;
import es.master.portalrural.infrastructure.repository.database.entities.Caracteristica;
import es.master.portalrural.infrastructure.repository.database.repositories.CaracteristicaRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FeatureDatabase implements FeatureRepository
{
  private final CaracteristicaRepository caracteristicaRepository;

  public FeatureDatabase(CaracteristicaRepository caracteristicaRepository)
  {
    this.caracteristicaRepository = caracteristicaRepository;
  }

  @Override
  public Collection<Feature> findAll()
  {
    return convert(caracteristicaRepository.findAll());
  }

  @Override
  public Feature get(Long featureId)
  {
    Caracteristica caracteristica = caracteristicaRepository.findOne(featureId);
    return convert(caracteristica);
  }

  private static Collection<Feature> convert(List<Caracteristica> caracteristicas)
  {
    ArrayList<Feature> features = new ArrayList<>();
    for (Caracteristica caracteristica : caracteristicas) {
      features.add(convert(caracteristica));
    }
    return features;
  }

  private static Feature convert(Caracteristica caracteristica)
  {
    return new Feature(caracteristica.getId(), caracteristica.getDescripcion());
  }
}
