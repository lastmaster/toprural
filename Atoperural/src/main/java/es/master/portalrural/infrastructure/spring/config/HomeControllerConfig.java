package es.master.portalrural.infrastructure.spring.config;

import es.master.portalrural.domain.ports.primary.ListAccommodationsUseCase;
import es.master.portalrural.domain.ports.primary.ListOwnersUseCase;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;
import es.master.portalrural.domain.ports.secondary.OwnerRepository;
import es.master.portalrural.infrastructure.spring.controllers.HomeController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HomeControllerConfig
{
  @Bean
  public HomeController homeController(OwnerRepository ownerRepository,
                                       AccommodationRepository accommodationRepository)
  {
    return new HomeController(
      new ListOwnersUseCase(ownerRepository),
      new ListAccommodationsUseCase(accommodationRepository)
    );
  }
}
