package es.master.portalrural.infrastructure.repository.database.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Huesped {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	//Datos
	private String nombreYapellidos;
	private String email;
	private String contraseña;
	private String teléfono;
	private String notas;
	private String role;
	
	/*@OneToMany(cascade=CascadeType.ALL)
	private List<Opiniones> listaOpiniones;*/
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Alojamiento> listaAlojamientosFavoritos;

}
