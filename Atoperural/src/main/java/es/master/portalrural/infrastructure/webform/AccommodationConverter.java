package es.master.portalrural.infrastructure.webform;

import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.domain.model.AccommodationInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AccommodationConverter
{
  public Accommodation convert(AccommodationFromWebForm accommodationFromWebForm)
  {
    Collection<PictureFromWebForm> picturesToKeep = new CheckableItemCollectionManipulator<PictureFromWebForm>()
      .dropCheckedItems(sanitizePictureList(accommodationFromWebForm.getPictures()));

    Collection<AccommodationFeatureFromWebForm> featuresToKeep = new CheckableItemCollectionManipulator<AccommodationFeatureFromWebForm>()
    	      .preserveCheckedItems(sanitize(accommodationFromWebForm.getFeatures()));
    
    return new Accommodation(
      extractAccommodationInfo(accommodationFromWebForm),
      featuresToKeep,
      picturesToKeep,
      convertCalendar(accommodationFromWebForm.getCalendars()));
  }

  private static Collection<AccommodationFeatureFromWebForm> sanitize(List<AccommodationFeatureFromWebForm> features)
  {
    ArrayList<AccommodationFeatureFromWebForm> sanitizedFeatures = new ArrayList<>();
    for (AccommodationFeatureFromWebForm accommodationFeatureFromWebForm : features)
    {
      if (accommodationFeatureFromWebForm.getId() != null)
      {
        sanitizedFeatures.add(accommodationFeatureFromWebForm);
      }
    }
    return sanitizedFeatures;
  }

  private static Collection<PictureFromWebForm> sanitizePictureList(Collection<PictureFromWebForm> picturesFromWebForm)
  {
    ArrayList<PictureFromWebForm> pictures = new ArrayList<>();
    for (PictureFromWebForm pictureFromWebForm : picturesFromWebForm)
    {
      if (pictureFromWebForm.getId() != null)
      {
        pictures.add(pictureFromWebForm);
      }
    }
    return pictures;
  }
  
  private static Collection<AccommodationCalendar> convertCalendar(Collection<AccommodationCalendarFromWebForm> calendarsFromWebForm)
  {
    ArrayList<AccommodationCalendar> accommodationCalendars = new ArrayList<>();
    for (AccommodationCalendarFromWebForm accommodationCalendarFromWebForm : sanitizeCalendar(calendarsFromWebForm))
    {
      accommodationCalendars.add(convert(accommodationCalendarFromWebForm));
    }
    return accommodationCalendars;
  }

  private static Collection<AccommodationCalendarFromWebForm> sanitizeCalendar(Collection<AccommodationCalendarFromWebForm> calendarsFromWebForm)
  {
    ArrayList<AccommodationCalendarFromWebForm> calendars = new ArrayList<>();
    for (AccommodationCalendarFromWebForm accommodationCalendarFromWebForm : calendarsFromWebForm)
    {
      //if (accommodationCalendarFromWebForm.getId() != null)
      //{
        calendars.add(accommodationCalendarFromWebForm);
      //}
    }
    return calendars;
  }

  private static AccommodationInfo extractAccommodationInfo(AccommodationFromWebForm accommodationFromWebForm)
  {
    return new AccommodationInfo(
      accommodationFromWebForm.getId(),
      accommodationFromWebForm.getName(),
      accommodationFromWebForm.getNumberTourism(),
      accommodationFromWebForm.getKindAccommodation(),
      accommodationFromWebForm.getWeb(),
      accommodationFromWebForm.getFacebook(),
      accommodationFromWebForm.getTwitter(),
      accommodationFromWebForm.getInstagram(),
      accommodationFromWebForm.getYoutube(),
      accommodationFromWebForm.getProvince(),
      accommodationFromWebForm.getVillage(),
      accommodationFromWebForm.getAddress(),
      accommodationFromWebForm.getPostalCode(),
      accommodationFromWebForm.getCountry(),
      accommodationFromWebForm.getLatitude(),
      accommodationFromWebForm.getLongitude(),
      accommodationFromWebForm.getMobilePhone(),
      accommodationFromWebForm.getLandline(),
      accommodationFromWebForm.getPeople(),
      accommodationFromWebForm.getNumberRooms(),
      accommodationFromWebForm.getShortDescription(),
      accommodationFromWebForm.getLongDescription(),
      accommodationFromWebForm.getActivities()
    );
  }
  
  private static AccommodationCalendar convert(AccommodationCalendarFromWebForm accommodationCalendarFromWebForm)
  {
    return new AccommodationCalendar(
      accommodationCalendarFromWebForm.getId(),
      accommodationCalendarFromWebForm.getBookedDateFormatted(),
      accommodationCalendarFromWebForm.getBookedDate()      
    );
  }
}
