package es.master.portalrural.infrastructure.spring.config;

import es.master.portalrural.domain.ports.secondary.FeatureRepository;
import es.master.portalrural.infrastructure.repository.FeatureDatabase;
import es.master.portalrural.infrastructure.repository.database.repositories.CaracteristicaRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeatureRepositoryConfig
{
  @Bean
  public FeatureRepository featureRepository(CaracteristicaRepository caracteristicaRepository)
  {
    return new FeatureDatabase(caracteristicaRepository);
  }
}
