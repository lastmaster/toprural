package es.master.portalrural.infrastructure.webform;

public class OwnerInfoFromWebForm
{
	private Long id;

	//Datos personales
	private String nombreYapellidos;

	private String email;
	private String contrasena;
	private String telefono;
	private String descripcion;

	//Datos de facturación
	private String dni;
	private String direccion;

	//Datos de la subscripción
	private String estadoSubscripcion = "activa";
	private String fechaInicio = "01-01-2019";
	private String fechaFin = "31-12-2030";
	private String tipoServicio = "silver";

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getNombreYapellidos() {
		return nombreYapellidos;
	}

	public void setNombreYapellidos(String nombreYapellidos) {
		this.nombreYapellidos = nombreYapellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEstadoSubscripcion() {
		return estadoSubscripcion;
	}

	public void setEstadoSubscripcion(String estadoSubscripcion) {
		this.estadoSubscripcion = estadoSubscripcion;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contrasena == null) ? 0 : contrasena.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((estadoSubscripcion == null) ? 0 : estadoSubscripcion.hashCode());
		result = prime * result + ((fechaFin == null) ? 0 : fechaFin.hashCode());
		result = prime * result + ((fechaInicio == null) ? 0 : fechaInicio.hashCode());
		result = prime * result + ((nombreYapellidos == null) ? 0 : nombreYapellidos.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		result = prime * result + ((tipoServicio == null) ? 0 : tipoServicio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OwnerInfoFromWebForm other = (OwnerInfoFromWebForm) obj;
		if (contrasena == null) {
			if (other.contrasena != null)
				return false;
		} else if (!contrasena.equals(other.contrasena))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (estadoSubscripcion == null) {
			if (other.estadoSubscripcion != null)
				return false;
		} else if (!estadoSubscripcion.equals(other.estadoSubscripcion))
			return false;
		if (fechaFin == null) {
			if (other.fechaFin != null)
				return false;
		} else if (!fechaFin.equals(other.fechaFin))
			return false;
		if (fechaInicio == null) {
			if (other.fechaInicio != null)
				return false;
		} else if (!fechaInicio.equals(other.fechaInicio))
			return false;
		if (nombreYapellidos == null) {
			if (other.nombreYapellidos != null)
				return false;
		} else if (!nombreYapellidos.equals(other.nombreYapellidos))
			return false;
		if (telefono == null) {
			if (other.telefono != null)
				return false;
		} else if (!telefono.equals(other.telefono))
			return false;
		if (tipoServicio == null) {
			if (other.tipoServicio != null)
				return false;
		} else if (!tipoServicio.equals(other.tipoServicio))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OwnerInfoFromWebForm ["
				+ (nombreYapellidos != null ? "nombreYapellidos=" + nombreYapellidos + ", " : "")
				+ (email != null ? "email=" + email + ", " : "")
				+ (contrasena != null ? "contraseña=" + contrasena + ", " : "")
				+ (telefono != null ? "telefono=" + telefono + ", " : "")
				+ (descripcion != null ? "descripcion=" + descripcion + ", " : "")
				+ (dni != null ? "dni=" + dni + ", " : "") + (direccion != null ? "direccion=" + direccion + ", " : "")
				+ (estadoSubscripcion != null ? "estadoSubscripcion=" + estadoSubscripcion + ", " : "")
				+ (fechaInicio != null ? "fechaInicio=" + fechaInicio + ", " : "")
				+ (fechaFin != null ? "fechaFin=" + fechaFin + ", " : "")
				+ (tipoServicio != null ? "tipoServicio=" + tipoServicio + ", " : "") + "]";
	}
}
