package es.master.portalrural.infrastructure.webform;

import java.util.ArrayList;
import java.util.List;

public class AccommodationFromWebForm
{
  private Long id;

  private String name;
  private String numberTourism;
  private int kindAccommodation;
  private String web;
  private String facebook;
  private String twitter;
  private String instagram;
  private String youtube;

  private String province;
  private String village;
  private String address;
  private String postalCode;
  private String country;
  private String latitude;
  private String longitude;

  private String mobilePhone;
  private String landline;

  private String people; 
  private String numberRooms;

  private String shortDescription;
  private String longDescription;
  private String activities;

  private List<PictureFromWebForm> pictures = new ArrayList<>();

  private List<AccommodationCalendarFromWebForm> calendars = new ArrayList<>();

  private List<AccommodationFeatureFromWebForm> features = new ArrayList<>();


  public Long getId()
  {
    return id;
  }

  public String getNumberTourism()
  {
    return numberTourism;
  }

  public int getKindAccommodation()
  {
    return kindAccommodation;
  }

  public String getWeb()
  {
    return web;
  }

  public String getFacebook()
  {
    return facebook;
  }

  public String getTwitter()
  {
    return twitter;
  }

  public String getInstagram()
  {
    return instagram;
  }

  public String getYoutube()
  {
    return youtube;
  }

  public String getProvince()
  {
    return province;
  }

  public String getVillage()
  {
    return village;
  }

  public String getAddress()
  {
    return address;
  }

  public String getPostalCode() {
	return postalCode;
}

public void setPostalCode(String postalCode) {
	this.postalCode = postalCode;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public String getLatitude() {
	return latitude;
}

public void setLatitude(String latitude) {
	this.latitude = latitude;
}

public String getLongitude() {
	return longitude;
}

public void setLongitude(String longitude) {
	this.longitude = longitude;
}

public String getMobilePhone()
  {
    return mobilePhone;
  }

  public String getLandline()
  {
    return landline;
  }

  public String getPeople()
  {
    return people;
  }

  public String getShortDescription()
  {
    return shortDescription;
  }

  public String getLongDescription()
  {
    return longDescription;
  }

  public String getActivities()
  {
    return activities;
  }

  public String getNumberRooms()
  {
    return numberRooms;
  }

  public String getName()
  {
    return name;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public void setNumberTourism(String numberTourism)
  {
    this.numberTourism = numberTourism;
  }

  public void setKindAccommodation(int kindAccommodation)
  {
    this.kindAccommodation = kindAccommodation;
  }

  public void setWeb(String web)
  {
    this.web = web;
  }

  public void setFacebook(String facebook)
  {
    this.facebook = facebook;
  }

  public void setTwitter(String twitter)
  {
    this.twitter = twitter;
  }

  public void setInstagram(String instagram)
  {
    this.instagram = instagram;
  }

  public void setYoutube(String youtube)
  {
    this.youtube = youtube;
  }

  public void setProvince(String province)
  {
    this.province = province;
  }

  public void setVillage(String village)
  {
    this.village = village;
  }

  public void setAddress(String address)
  {
    this.address = address;
  }

  public void setMobilePhone(String mobilePhone)
  {
    this.mobilePhone = mobilePhone;
  }

  public void setLandline(String landline)
  {
    this.landline = landline;
  }

  public void setPeople(String people)
  {
    this.people = people;
  }

  public void setNumberRooms(String numberRooms)
  {
    this.numberRooms = numberRooms;
  }

  public void setShortDescription(String shortDescription)
  {
    this.shortDescription = shortDescription;
  }

  public void setLongDescription(String longDescription)
  {
    this.longDescription = longDescription;
  }

  public void setActivities(String activities)
  {
    this.activities = activities;
  }

  public List<PictureFromWebForm> getPictures()
  {
    return pictures;
  }

  public void setPictures(List<PictureFromWebForm> fotografias)
  {
    this.pictures = fotografias;
  }

  public List<AccommodationFeatureFromWebForm> getFeatures()
  {
    return features;
  }

  public void setFeatures(List<AccommodationFeatureFromWebForm> features)
  {
    this.features = features;
  }
  
  public List<AccommodationCalendarFromWebForm> getCalendars() 
  {
	return calendars;
  }

  public void setCalendars(List<AccommodationCalendarFromWebForm> calendars) 
  {
	this.calendars = calendars;
  }
}
