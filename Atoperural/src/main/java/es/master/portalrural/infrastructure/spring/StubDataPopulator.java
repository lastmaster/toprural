package es.master.portalrural.infrastructure.spring;

import es.master.portalrural.infrastructure.repository.database.entities.Alojamiento;
import es.master.portalrural.infrastructure.repository.database.entities.Fotografia;
import es.master.portalrural.infrastructure.repository.database.entities.Propietario;
import es.master.portalrural.infrastructure.repository.database.repositories.AlojamientoRepository;
import es.master.portalrural.infrastructure.repository.database.repositories.PropietarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;

@Component
public class StubDataPopulator implements ApplicationRunner
{
  @Autowired
  private PropietarioRepository repositoryPropietarios;

  @Autowired
  private AlojamientoRepository repositoryAlojamientos;

  @Override
  public void run(ApplicationArguments applicationArguments) throws Exception
  {
    /*Propietario p = new Propietario();
    p.setNombreYapellidos("John Doe");
    p.setEmail("jd@example.com");
    p.setContrasena("1234abc");
    p.setTelefono("600000000");
    p.setDescripcion("A description");
    p.setDni("00000000Z");
    p.setDireccion("Fake Street");
    p.setEstadoSubscripcion("activo");
    p.setFechaInicio("hoy");
    p.setFechaFin("hoy");
    p.setTipoServicio("basico");

    System.out.println(p);
    p = repositoryPropietarios.save(p);*/

//    Fotografia f = new Fotografia("foto1", "20", "la descripcion");
//    Fotografia f2 = new Fotografia("foto2", "40", "otra foto");


//    Alojamiento a = new Alojamiento("Casa Salva");
//    a.setListaFotografias(Arrays.asList(f, f2));
//    a.setPropietario(p);
//    repositoryAlojamientos.saveAssigningOwner(a);



		/*Caracteristica elem1 = new Caracteristica("azucar");
		Caracteristica elem2 = new Caracteristica("cafe");
		List<Caracteristica> listaElem = new ArrayList<Caracteristica>();
		listaElem.add(elem1);
		listaElem.add(elem2);

	    Alojamiento alojamientoInicial = new Alojamiento("Alojamiento 1");
	    pedidoInicial.setListaElementos(listaElem);
	    pedidoInicial.getListaElementos().add(new Caracteristica("Patatas"));
	    repositoryAlojamientos.saveAssigningOwner(alojamientoInicial);*/


  }
}
