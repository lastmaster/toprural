package es.master.portalrural.infrastructure.spring.config;

import es.master.portalrural.domain.ports.primary.AddAccommodationUseCase;
import es.master.portalrural.domain.ports.primary.DeleteAccommodationUseCase;
import es.master.portalrural.domain.ports.primary.ListFeaturesUseCase;
import es.master.portalrural.domain.ports.primary.ShowAccommodationUseCase;
import es.master.portalrural.domain.ports.primary.UpdateAccommodationUseCase;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;
import es.master.portalrural.domain.ports.secondary.FeatureRepository;
import es.master.portalrural.infrastructure.spring.controllers.AccommodationController;
import es.master.portalrural.infrastructure.webform.FeaturesToShowConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AccommodationControllerConfig
{
  @Bean
  public AccommodationController accommodationController(AccommodationRepository accommodationRepository,
                                                         FeatureRepository featureRepository)
  {
    return new AccommodationController(
      new AddAccommodationUseCase(accommodationRepository),
      new ShowAccommodationUseCase(accommodationRepository),
      new DeleteAccommodationUseCase(accommodationRepository),
      new UpdateAccommodationUseCase(accommodationRepository),
      new ListFeaturesUseCase(featureRepository),
      new FeaturesToShowConverter(featureRepository));
  }
}
