package es.master.portalrural.infrastructure.repository;

public interface ElementConverter<From, To>
{
  To convert(From element);
}
