package es.master.portalrural.infrastructure;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.session.hazelcast.config.annotation.web.http.EnableHazelcastHttpSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;

@EnableCaching
@SpringBootApplication
@EnableHazelcastHttpSession
@PropertySource(value = "classpath:application.properties")
@RestController
@EnableOAuth2Sso
public class Application extends WebSecurityConfigurerAdapter {

	private static ConfigurableApplicationContext app;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	private static final Log LOG = LogFactory.getLog(Application.class);

	@Bean
	public Config config() {
		Config config = new Config();

        String k8s = System.getProperty("k8s");
        if (k8s == "true") {
            /*
            JoinConfig joinConfig = config.getNetworkConfig().getJoin();
            joinConfig.getMulticastConfig().setEnabled(false);
            joinConfig.getKubernetesConfig().setEnabled(true).setProperty("namespace", "default");
            */
        }

		return config;
	}

    public static void start() {
        start(new String[] {});
    }

    private static void start(String[] args) {
        if(app == null) {
            app = SpringApplication.run(Application.class, args);
        } 
    }    
    
    public static void stop() {
        if(app != null) {
            app.close();
        }
    }
	
	
	@Bean
	public CacheManager cacheManager() {
		LOG.info("Activating cache...");
		// nombre de la cache
		return new ConcurrentMapCacheManager("atoperural");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
	    
		http.csrf().disable().antMatcher("/**").authorizeRequests().antMatchers("/**", "/login**", "/webjars/**")
				.permitAll().anyRequest().authenticated().and().
				logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/").deleteCookies("JSESSIONID")
				.invalidateHttpSession(true).clearAuthentication(true); 
		http.headers().frameOptions().disable();
		
	}

	@RequestMapping("/user")
	public Principal user(Principal principal) {
		return principal;
	}
}
