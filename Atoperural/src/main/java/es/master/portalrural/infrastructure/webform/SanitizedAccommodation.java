package es.master.portalrural.infrastructure.webform;

import es.master.portalrural.domain.model.AccommodationFeature;
import es.master.portalrural.domain.model.Picture;
import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationCalendar;

import java.util.Collection;
import java.util.Collections;

public class SanitizedAccommodation
{
  private final Accommodation accommodation;

  public SanitizedAccommodation(Accommodation accommodation)
  {
    this.accommodation = accommodation;
  }
  
  public Long getId() {
	return accommodation.getId();
  }

  public String getName() {
    return accommodation.getName();
  }
  
  public String getNumberTourism() {
    return sanitize(accommodation.getNumberTourism());
  }

  public int getKindAccommodation() {
    return accommodation.getKindAccommodation();
  }

  public String getWeb() {
    return sanitize(accommodation.getWeb());
  }

  public String getFacebook() {
    return sanitize(accommodation.getFacebook());
  }

  public String getTwitter() {
    return sanitize(accommodation.getTwitter());
  }

  public String getInstagram() {
    return sanitize(accommodation.getInstagram());
  }

  public String getYoutube() {
    return sanitize(accommodation.getYoutube());
  }

  public String getProvince() {
    return sanitize(accommodation.getProvince());
  }

  public String getVillage() {
    return sanitize(accommodation.getVillage());
  }

  public String getAddress() {
    return sanitize(accommodation.getAddress());
  }
  
  public String getPostalCode() {
    return sanitize(accommodation.getPostalCode());
  }

  public String getCountry() {
    return sanitize(accommodation.getCountry());
  }
  
  public String getLatitude() {
    return sanitize(accommodation.getLatitude());
  }
  
  public String getLongitude() {
    return sanitize(accommodation.getLongitude());
  }

  public String getMobilePhone() {
    return sanitize(accommodation.getMobilePhone());
  }

  public String getLandline() {
    return sanitize(accommodation.getLandline());
  }

  public String getPeople() {
    return sanitize(accommodation.getPeople());
  }

  public String getNumberRooms() {
    return sanitize(accommodation.getNumberRooms());
  }

  public String getShortDescription() {
    return sanitize(accommodation.getShortDescription());
  }

  public String getLongDescription() {
    return sanitize(accommodation.getLongDescription());
  }

  public String getActivities() {
    return sanitize(accommodation.getActivities());
  }

  public Collection<? extends AccommodationFeature> getListFeatures() {
    return sanitize(accommodation.getListFeatures());

  }

  public Collection<? extends Picture> getListPictures() {
    return sanitize(accommodation.getListPictures());

  }
  
  public Collection<AccommodationCalendar> getListCalendar() {
	return sanitize(accommodation.getListAccommodationCalendar());
  }

  private static String sanitize(String aString) {
    return aString != null ? aString : "";
  }

  private static <T> Collection<T> sanitize(Collection<T> aCollection) {
    return aCollection != null ? aCollection : Collections.<T>emptyList();
  }
}
