package es.master.portalrural.infrastructure.webform;

import es.master.portalrural.domain.model.Picture;

public class PictureFromWebForm extends Picture implements CheckableItem
{
  private Long id;

  private String nombreFoto;
  private String tamano;
  private String descripcion;
  private String tipoFichero;
  private int orden;

  private boolean checked;

  public void setId(Long id)
  {
    this.id = id;
  }

  public void setNombreFoto(String nombreFoto)
  {
    this.nombreFoto = nombreFoto;
  }

  public void setTamano(String tamano)
  {
    this.tamano = tamano;
  }

  public void setDescripcion(String descripcion)
  {
    this.descripcion = descripcion;
  }

  public void setTipoFichero(String tipoFichero)
  {
    this.tipoFichero = tipoFichero;
  }

  public void setOrden(int orden)
  {
    this.orden = orden;
  }

  public void setChecked(boolean checked)
  {
    this.checked = checked;
  }

  @Override
  public boolean isChecked()
  {
    return checked;
  }

  @Override
  public Long getId()
  {
    return id;
  }

  @Override
  public String getNombreFoto()
  {
    return nombreFoto;
  }

  @Override
  public String getDescripcion()
  {
    return descripcion;
  }

  @Override
  public String getTamano()
  {
    return tamano;
  }

  @Override
  public String getTipoFichero()
  {
    return tipoFichero;
  }

  @Override
  public int getOrden()
  {
    return orden;
  }

  @Override
  public String toString()
  {
    return "PictureFromWebForm{" +
      "id=" + id +
      ", nombreFoto='" + nombreFoto + '\'' +
      ", tamano='" + tamano + '\'' +
      ", descripcion='" + descripcion + '\'' +
      ", tipoFichero='" + tipoFichero + '\'' +
      ", orden=" + orden +
      ", checked=" + checked +
      '}';
  }
}
