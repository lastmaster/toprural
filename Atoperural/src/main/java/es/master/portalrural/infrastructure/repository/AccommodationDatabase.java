package es.master.portalrural.infrastructure.repository;

import es.master.portalrural.domain.model.AccommodationFeature;
import es.master.portalrural.domain.model.Picture;
import es.master.portalrural.domain.model.Accommodation;
import es.master.portalrural.domain.model.AccommodationReduced;
import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.domain.ports.secondary.AccommodationRepository;
import es.master.portalrural.infrastructure.repository.database.entities.Alojamiento;
import es.master.portalrural.infrastructure.repository.database.entities.BookedDate;
import es.master.portalrural.infrastructure.repository.database.entities.Fotografia;
import es.master.portalrural.infrastructure.repository.database.entities.PrecioCaracteristica;
import es.master.portalrural.infrastructure.repository.database.entities.Propietario;
import es.master.portalrural.infrastructure.repository.database.repositories.AlojamientoRepository;
import es.master.portalrural.infrastructure.repository.database.repositories.BookedDateRepository;
import es.master.portalrural.infrastructure.repository.database.repositories.PropietarioRepository;

import java.util.ArrayList;
import java.util.Collection;

public class AccommodationDatabase implements AccommodationRepository
{
  private final AlojamientoRepository alojamientoRepository;
  private final PropietarioRepository propietarioRepository;
  private final BookedDateRepository bookedDateRepository;
  private final AccommodationConverter accommodationConverter;
  
  public AccommodationDatabase(AlojamientoRepository alojamientoRepository,
                               PropietarioRepository propietarioRepository,
                               BookedDateRepository bookedDateRepository,
                               AccommodationConverter accommodationConverter)
  {
    this.alojamientoRepository = alojamientoRepository;
    this.propietarioRepository = propietarioRepository;
    this.bookedDateRepository = bookedDateRepository;
    this.accommodationConverter = accommodationConverter;
  }

  @Override
  public Collection<AccommodationReduced> findAll()
  {
    Collection<Alojamiento> alojamientos = alojamientoRepository.findAll();
    ArrayList<AccommodationReduced> accommodationsReduced = new ArrayList<>();
    for (Alojamiento alojamiento : alojamientos)
    {
      accommodationsReduced.add(convertToAccommodationReduced(alojamiento));
    }
    return accommodationsReduced;
  }

  @Override
  public Accommodation get(Long accommodationId)
  {
    Accommodation accommodation = null;
    Alojamiento alojamiento = alojamientoRepository.getOne(accommodationId);
    if (alojamiento != null)
    {
      accommodation = accommodationConverter.from(alojamiento);
    }
    return accommodation;
  }

  @Override
  public Accommodation savePreservingOwner(Accommodation accommodation)
  {
    Collection<BookedDate> bookedDates = bookedDateRepository.findAll();
    /*for (BookedDate bookedDate: bookedDates)
    {
    	System.out.println("Old date booked: " + bookedDate.getBookedDate());
    }*/
    Collection<AccommodationCalendar> accommodationNewBookedDates = accommodation.getListAccommodationCalendar();
    /*for (AccommodationCalendar accommodationNewBookedDate: accommodationNewBookedDates)
    {
    	System.out.println("New date to book (not converted): " + accommodationNewBookedDate.getBookedDay());
    }*/
    Collection<BookedDate> bookedDatesNew = convertToBookedDates(accommodation.getListAccommodationCalendar(), convert(accommodation));
    Collection<BookedDate> bookedDatesToRemove = new ArrayList<BookedDate>();
    for (BookedDate bookedDateNew: bookedDatesNew)
    {
    	 //System.out.println("New date to book: " + bookedDateNew.getBookedDate());
    	 for (BookedDate bookedDate: bookedDates)
		 {
    		 if (bookedDateNew.getBookedDate().compareTo(bookedDate.getBookedDate()) == 0)
    		 {
    			 System.out.println("This day is already save in the database, do nothing: " + bookedDateNew.getBookedDate());
    		 }
    		 else
    		 {
    			 System.out.println("This day is not booked, remove from the database: " + bookedDate.getBookedDate());
    			 bookedDatesToRemove.add(bookedDate);
    		 }
		 }
    }
    
    for (BookedDate bookedDateToRemove: bookedDatesToRemove)
    {
    	System.out.println("Next step is remove this from database: " + bookedDateToRemove.getBookedDate());
    	bookedDateRepository.delete(bookedDateToRemove);
    }
    	
    Alojamiento alojamientoToSave = convert(accommodation);
    Propietario propietario = alojamientoRepository.findOne(accommodation.getId()).getOwner();
    alojamientoToSave.setOwner(propietario);
    Alojamiento savedAlojamiento = alojamientoRepository.save(alojamientoToSave);
    return accommodationConverter.from(savedAlojamiento);
  }

  @Override
  public Accommodation saveAssigningOwner(Accommodation accommodation, Long ownerId)
  {
    Alojamiento alojamientoToSave = convert(accommodation);
    Propietario propietario = propietarioRepository.findOne(ownerId);
    alojamientoToSave.setOwner(propietario);
    Alojamiento savedAlojamiento = alojamientoRepository.save(alojamientoToSave);
    return accommodationConverter.from(savedAlojamiento);
  }

  @Override
  public AccommodationReduced delete(Long id)
  {
    AccommodationReduced accommodationReduced = null;
    Alojamiento alojamientoToRemove = alojamientoRepository.getOne(id);
    if (alojamientoToRemove != null)
    {
      alojamientoRepository.delete(id);
      accommodationReduced = convertToAccommodationReduced(alojamientoToRemove);
    }
    return accommodationReduced;
  }

  private Alojamiento convert(Accommodation accommodation)
  {
    Alojamiento alojamiento = new Alojamiento();
    alojamiento.setId(accommodation.getId());
    alojamiento.setName(accommodation.getName());
    alojamiento.setNumberTourism(accommodation.getNumberTourism());
    alojamiento.setKindAccommodation(accommodation.getKindAccommodation());
    alojamiento.setWeb(accommodation.getWeb());
    alojamiento.setFacebook(accommodation.getFacebook());
    alojamiento.setTwitter(accommodation.getTwitter());
    alojamiento.setInstagram(accommodation.getInstagram());
    alojamiento.setYoutube(accommodation.getYoutube());
    alojamiento.setProvince(accommodation.getProvince());
    alojamiento.setVillage(accommodation.getVillage());
    alojamiento.setAddress(accommodation.getAddress());
    alojamiento.setPostalCode(accommodation.getPostalCode());
    alojamiento.setCountry(accommodation.getCountry());
    alojamiento.setLatitude(accommodation.getLatitude());
    alojamiento.setLongitude(accommodation.getLongitude());
    alojamiento.setMobilePhone(accommodation.getMobilePhone());
    alojamiento.setLandline(accommodation.getLandline());
    alojamiento.setPeople(accommodation.getPeople());
    alojamiento.setNumberRooms(accommodation.getNumberRooms());
    alojamiento.setShortDescription(accommodation.getShortDescription());
    alojamiento.setLongDescription(accommodation.getLongDescription());
    alojamiento.setActivities(accommodation.getActivities());
    alojamiento.setListFeatures(
      convertToCollectionOfPrecioCaracteristica(accommodation.getListFeatures()));
    alojamiento.setListPictures(convertToFotografias(accommodation.getListPictures(), alojamiento));
    alojamiento.setBookedDates(convertToBookedDates(accommodation.getListAccommodationCalendar(), alojamiento));
    return alojamiento;
  }

  private static Collection<BookedDate> convertToBookedDates(Collection<AccommodationCalendar> listCalendar,
          Alojamiento alojamiento)
  {
    ArrayList<BookedDate> bookedDates = new ArrayList<>();
    for (AccommodationCalendar accommodationCalendar : listCalendar)
    {
      bookedDates.add(convertToBookedDate(accommodationCalendar, alojamiento));
    }
    return bookedDates;
  }

  private static BookedDate convertToBookedDate(AccommodationCalendar accommodationCalendar, Alojamiento alojamiento)
  {
	BookedDate bookedDate = new BookedDate();
    bookedDate.setAlojamiento(alojamiento);
    bookedDate.setBookedDate(accommodationCalendar.getBookedDayFormatted());
    return bookedDate;
 }
  
  private static Collection<Fotografia> convertToFotografias(Collection<? extends Picture> listaFotografias,
                                                             Alojamiento alojamiento)
  {
    ArrayList<Fotografia> fotografias = new ArrayList<>();
    for (Picture picture : listaFotografias)
    {
      fotografias.add(convertToFotografia(picture, alojamiento));
    }
    return fotografias;
  }

  private static Fotografia convertToFotografia(Picture picture, Alojamiento alojamiento)
  {
    Fotografia fotografia = new Fotografia();
    fotografia.setAlojamiento(alojamiento);
    fotografia.setDescripcion(picture.getDescripcion());
    fotografia.setId(picture.getId());
    fotografia.setNombreFoto(picture.getNombreFoto());
    fotografia.setOrden(picture.getOrden());
    fotografia.setTamano(picture.getTamano());
    fotografia.setTipoFichero(picture.getTipoFichero());
    return fotografia;
  }

  private static Collection<PrecioCaracteristica> convertToCollectionOfPrecioCaracteristica(
    Collection<? extends AccommodationFeature> accomodationFeatures)
  {
    ArrayList<PrecioCaracteristica> caracteristicas = new ArrayList<>();
    for (AccommodationFeature accomodationFeature : accomodationFeatures)
    {
      caracteristicas.add(convertToPrecioCaracteristica(accomodationFeature));
    }
    return caracteristicas;
  }

  private static PrecioCaracteristica convertToPrecioCaracteristica(AccommodationFeature accomodationFeature)
  {
    PrecioCaracteristica precioCaracteristica = new PrecioCaracteristica();
    precioCaracteristica.setIdCaracteristica(accomodationFeature.getFeatureId());
    precioCaracteristica.setPrecio(accomodationFeature.getPrice().doubleValue());
    return precioCaracteristica;
  }

  private static AccommodationReduced convertToAccommodationReduced(Alojamiento alojamiento)
  {
    return new AccommodationReduced(alojamiento.getId(), alojamiento.getName());
  }

}
