package es.master.portalrural.infrastructure.repository;

import es.master.portalrural.domain.model.AccommodationCalendar;
import es.master.portalrural.infrastructure.repository.database.entities.BookedDate;

import java.util.Collection;

public class AccommodationCalendarsConverter
{
  public Collection<AccommodationCalendar> from(Collection<BookedDate> bookedDates)
  {
    return new CollectionConverter<>(
      new ElementConverter<BookedDate, AccommodationCalendar>()
      {
        @Override
        public AccommodationCalendar convert(BookedDate element)
        {
          return convertToCalendar(element);
        }
      }).convert(bookedDates);
  }

  private static AccommodationCalendar convertToCalendar(BookedDate bookedDate)
  {
    return new AccommodationCalendar(
      bookedDate.getId(),
      bookedDate.getBookedDate(),
      bookedDate.getBookedDate().toString() //TODO: Ojo con esto que debería ser solo YYYY-MM-DD
    );
  }
}
