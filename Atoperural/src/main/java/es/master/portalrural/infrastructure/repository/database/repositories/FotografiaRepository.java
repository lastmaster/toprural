package es.master.portalrural.infrastructure.repository.database.repositories;


import es.master.portalrural.infrastructure.repository.database.entities.Fotografia;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FotografiaRepository extends JpaRepository<Fotografia, Long> {

	
}
