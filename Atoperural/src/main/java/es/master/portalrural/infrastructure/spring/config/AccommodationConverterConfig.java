package es.master.portalrural.infrastructure.spring.config;

import es.master.portalrural.infrastructure.repository.AccommodationCalendarsConverter;
import es.master.portalrural.infrastructure.repository.AccommodationConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AccommodationConverterConfig
{

  @Bean
  public AccommodationConverter accommodationConverter() {
    return new AccommodationConverter(
      new AccommodationCalendarsConverter());
  }
}
