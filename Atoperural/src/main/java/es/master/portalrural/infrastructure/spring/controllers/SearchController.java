package es.master.portalrural.infrastructure.spring.controllers;

import es.master.portalrural.infrastructure.repository.database.entities.Alojamiento;
import es.master.portalrural.infrastructure.repository.database.entities.BookedDate;
import es.master.portalrural.infrastructure.repository.database.repositories.AlojamientoRepository;
import es.master.portalrural.infrastructure.repository.database.repositories.PropietarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;


@Controller
public class SearchController
{

	@Autowired
	private PropietarioRepository repositoryPropietarios;

	@Autowired
	private AlojamientoRepository repositoryAlojamientos;


	//Filtros de búsqueda por provincia y por número de personas
	@GetMapping("/casas-rurales")
	public String filtros(Model model, @RequestParam String province, @RequestParam String people, @RequestParam String startDate, @RequestParam String endDate) {

		List<Alojamiento> alojamientosFiltrados = new ArrayList<Alojamiento>();
		List<Alojamiento> alojamientosConFechasNoDisponibles = new ArrayList<Alojamiento>();
	    if (!province.isEmpty() && !people.isEmpty())
		{
			List<Alojamiento> alojamientosPorProvincia = repositoryAlojamientos.findByProvince(province);
			System.out.println("Alojamientos en la provincia de " + province + ": "+ alojamientosPorProvincia.size());

			List<Alojamiento> alojamientosPorPlazas = repositoryAlojamientos.findByPeople(people);
			System.out.println("Alojamientos de " + people + " plazas: "+ alojamientosPorPlazas.size());
	
			for (Alojamiento elemAlojamiento : alojamientosPorProvincia)
			{
				if (alojamientosPorPlazas.contains(elemAlojamiento))
				{
					System.out.println("Alojamiento que cumple los criterios de búsqueda: " + province + ":"+ people + " plazas. Alojamiento: "+ elemAlojamiento.toString());
					alojamientosFiltrados.add(elemAlojamiento);
				}
				else
				{
					System.out.println("Alojamiento que no cumple los criterios de búsqueda");
				}
			}
		}
		else if (!province.isEmpty())
		{
			System.out.println("Alojamientos en la provincia de " + province + ": "+ repositoryAlojamientos.findByProvince(province).size());
			alojamientosFiltrados = repositoryAlojamientos.findByProvince(province);
		}
		else if (!people.isEmpty())
		{
			System.out.println("Alojamientos de " + people + " plazas: "+ repositoryAlojamientos.findByPeople(people).size());
			alojamientosFiltrados = repositoryAlojamientos.findByPeople(people);
		}
		else
		{
			//System.out.println("Todos los alojamientos porque no hay criterio de búsqueda");
			alojamientosFiltrados = repositoryAlojamientos.findAll();
		}
	    
	    if (!startDate.isEmpty() && !endDate.isEmpty())
		{
			System.out.println("Alojamientos libres entre estas fechas (" + startDate+ " -> " + endDate + ")???");
			// Creamos el SimpleDateFormat con nuestro patrón deseado E yyyy-MM-dd
	        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateToBook = new Date();
		    try {
		         startDateToBook = sdf.parse(startDate);
		         System.out.println(startDateToBook);
		    } catch (ParseException e) {
		         System.err.println("Unparseable using " + sdf);
		    }
			Date endDateToBook = new Date();
		    try {
		         endDateToBook = sdf.parse(endDate);
		         System.out.println(endDateToBook);
		    } catch (ParseException e) {
		         System.err.println("Unparseable using " + sdf);
		    }
	        
	        // Obtenemos la lista de fechas utilizando el método que está líneas más abajo
	        Collection<Date> listaEntreFechas = getListaEntreFechas(startDateToBook, endDateToBook);

			for (Alojamiento elemAlojamiento : alojamientosFiltrados)
			{
				Collection<BookedDate> bookedDatesCollection= elemAlojamiento.getBookedDates();
				for (BookedDate bookedDates : bookedDatesCollection)
				{
					//System.out.println("Fecha reservada en BBDD: " + sdf.format(bookedDates.getBookedDate()));
					for (Date date : listaEntreFechas)
					{
			            // Imprimimos en consola la fecha formateada
			            if(bookedDates.getBookedDate().compareTo(date) == 0)
			            {
			            	System.out.println("Fecha reservada: " + sdf.format(date));
			            	//Alojamiento no disponible
			            	alojamientosConFechasNoDisponibles.add(elemAlojamiento);
			            	break;
			            }
			            else
			            {
			            	System.out.println("Fecha no reservada: " + sdf.format(date));
			            }
			        }
					
				}
			}
		}
	    
	    alojamientosFiltrados.removeAll(alojamientosConFechasNoDisponibles);
	    for (Alojamiento elemAlojamiento : alojamientosFiltrados)
		{
	    	System.out.println("Alojamientos con fechas libres: " + elemAlojamiento.getName());
		}
		model.addAttribute("alojamientos", alojamientosFiltrados);
		model.addAttribute("propietarios", repositoryPropietarios.findAll()); //Dejo los propietarios de momento
		return "home_portalrural";
	}
    /**
     * Método para obtener una lista con fechas en el intervalo indicado
     * @param fechaInicio Fecha inicial del intervalo
     * @param fechaFin Fecha final del intervalo
     * @return Fecha final
     */
    public Collection<Date> getListaEntreFechas(Date fechaInicio, Date fechaFin) {
        // Convertimos la fecha a Calendar, mucho más cómodo para realizar
        // operaciones a las fechas
        Calendar c1 = Calendar.getInstance();
        c1.setTime(fechaInicio);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(fechaFin);
        
        // Lista donde se irán almacenando las fechas
        ArrayList<Date> listaFechas = new ArrayList<Date>();
        
        // Bucle para recorrer el intervalo, en cada paso se le suma un día.
        while (!c1.after(c2)) {
            listaFechas.add(c1.getTime());
            c1.add(Calendar.DAY_OF_MONTH, 1);
        }
        return listaFechas;
    }
}