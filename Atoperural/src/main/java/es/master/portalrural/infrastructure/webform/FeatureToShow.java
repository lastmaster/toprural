package es.master.portalrural.infrastructure.webform;

public class FeatureToShow
{
  private final boolean flagged;
  private final Long id;
  private final String description;
  private final String price;
  private final String kind;

  public FeatureToShow(boolean flagged, Long id, String description, String price, String kind)
  {
    this.flagged = flagged;
    this.id = id;
    this.description = description;
    this.price = price;
    this.kind = kind;
  }

  public Long getId()
  {
    return id;
  }

  public boolean isFlagged()
  {
    return flagged;
  }

  public String getDescription()
  {
    return description;
  }

  public String getPrice()
  {
    return price;
  }
  
  public String getKind()
  {
    return kind;
  }
}
