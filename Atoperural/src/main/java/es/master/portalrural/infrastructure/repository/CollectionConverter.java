package es.master.portalrural.infrastructure.repository;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionConverter<From, To>
{
  private final ElementConverter<From, To> elementConverter;

  public CollectionConverter(
    ElementConverter<From, To> elementConverter)
  {
    this.elementConverter = elementConverter;
  }

  public Collection<To> convert(Collection<From> elements) {
    ArrayList<To> convertedElements = new ArrayList<>();
    for (From element : elements) {
      convertedElements.add(elementConverter.convert(element));
    }
    return convertedElements;
  }
}
