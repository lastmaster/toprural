package es.master.portalrural.infrastructure.webform;

import java.util.ArrayList;
import java.util.Collection;

public class CheckableItemCollectionManipulator<T extends CheckableItem>
{
  public Collection<T> preserveCheckedItems(Collection<T> checkableItems)
  {
    ArrayList<T> checkedItems = new ArrayList<>();
    for (T checkableItem : checkableItems)
    {
      if (checkableItem.isChecked())
      {
        checkedItems.add(checkableItem);
      }
    }
    return checkedItems;
  }

  public Collection<T> dropCheckedItems(Collection<T> checkableItems)
  {
    ArrayList<T> checkedItems = new ArrayList<>();
    for (T checkableItem : checkableItems)
    {
      if (checkableItem.isChecked() == false)
      {
        checkedItems.add(checkableItem);
      }
    }
    return checkedItems;
  }
}
