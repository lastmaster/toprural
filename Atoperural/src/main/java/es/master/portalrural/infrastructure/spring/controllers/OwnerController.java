package es.master.portalrural.infrastructure.spring.controllers;

import es.master.portalrural.domain.model.Owner;
import es.master.portalrural.domain.model.OwnerInfo;
import es.master.portalrural.domain.model.OwnerReduced;
import es.master.portalrural.domain.ports.primary.AddOwnerUseCase;
import es.master.portalrural.domain.ports.primary.DeleteOwnerUseCase;
import es.master.portalrural.domain.ports.primary.ShowOwnerUseCase;
import es.master.portalrural.domain.ports.primary.UpdateOwnerInfoUseCase;
import es.master.portalrural.infrastructure.webform.OwnerInfoFromWebForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class OwnerController
{
  private final AddOwnerUseCase addOwnerUseCase;
  private final ShowOwnerUseCase showOwnerUseCase;
  private final DeleteOwnerUseCase deleteOwnerUseCase;
  private final UpdateOwnerInfoUseCase updateOwnerInfoUseCase;

  public OwnerController(AddOwnerUseCase addOwnerUseCase,
                         ShowOwnerUseCase showOwnerUseCase,
                         DeleteOwnerUseCase deleteOwnerUseCase,
                         UpdateOwnerInfoUseCase updateOwnerInfoUseCase)
  {
    this.addOwnerUseCase = addOwnerUseCase;
    this.showOwnerUseCase = showOwnerUseCase;
    this.deleteOwnerUseCase = deleteOwnerUseCase;
    this.updateOwnerInfoUseCase = updateOwnerInfoUseCase;
  }

  @PostMapping("/propietario/nuevo")
  public String nuevoPropietario(Model model, OwnerInfoFromWebForm ownerInfoFromWebForm) {

    OwnerReduced addedOwner = addOwnerUseCase.execute(convert(ownerInfoFromWebForm));
    ownerInfoFromWebForm.setId(addedOwner.getId());
    Owner updatedOwner = updateOwnerInfoUseCase.execute(convert(ownerInfoFromWebForm));
    
    model.addAttribute("propietario", updatedOwner);
    System.out.println("nuevoPropietario: " + updatedOwner.toString());
    return "propietario_guardado";
  }

  @GetMapping("/propietario/{idPropietario}")
  public String verPropietario(Model model, @PathVariable long idPropietario) {

    Owner owner = showOwnerUseCase.execute(idPropietario);

    model.addAttribute("propietario", owner);
    System.out.println("verPropietario: " + owner.toString());
    model.addAttribute("listaalojamientos", owner.getListaAlojamientos());
    return "ver_propietario";
  }


  @PostMapping("/propietario/{idPropietario}/borrar")
  public String borrarPropietario(Model model, @PathVariable long idPropietario) {

    OwnerReduced deletedOwner = deleteOwnerUseCase.execute(idPropietario);

    System.out.println("borrarPropietario: " + deletedOwner.toString());
    model.addAttribute("idPropietario", idPropietario);
    return "propietario_borrado";
  }

  @PostMapping("/propietario/{idPropietario}/actualizar")
  public String actualizarPropietario(Model model,
                                      OwnerInfoFromWebForm ownerInfoFromWebForm,
                                      @PathVariable long idPropietario) {

    ownerInfoFromWebForm.setId(idPropietario);
    Owner updatedOwner = updateOwnerInfoUseCase.execute(convert(ownerInfoFromWebForm));

    model.addAttribute("propietario", updatedOwner);
    model.addAttribute("listaalojamientos", updatedOwner.getListaAlojamientos());
    System.out.println("actualizarPropietario: " + updatedOwner.toString());

    return "propietario_actualizado";
  }


  private static OwnerInfo convert(OwnerInfoFromWebForm ownerInfoFromWebForm)
  {
    return new OwnerInfo(ownerInfoFromWebForm.getId(),
      ownerInfoFromWebForm.getNombreYapellidos(),
      ownerInfoFromWebForm.getEmail(),
      ownerInfoFromWebForm.getContrasena(),
      ownerInfoFromWebForm.getTelefono(),
      ownerInfoFromWebForm.getDescripcion(),
      ownerInfoFromWebForm.getDni(),
      ownerInfoFromWebForm.getDireccion(),
      ownerInfoFromWebForm.getEstadoSubscripcion(),
      ownerInfoFromWebForm.getFechaInicio(),
      ownerInfoFromWebForm.getFechaFin(),
      ownerInfoFromWebForm.getTipoServicio());
  }
}
