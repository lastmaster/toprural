package es.master.portalrural.infrastructure.repository.database.repositories;


import java.util.Date;
import java.util.List;

import es.master.portalrural.infrastructure.repository.database.entities.Alojamiento;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AlojamientoRepository extends JpaRepository<Alojamiento, Long> {

	List<Alojamiento> findByPeople(String people);
	List<Alojamiento> findByProvince(String province);
}
