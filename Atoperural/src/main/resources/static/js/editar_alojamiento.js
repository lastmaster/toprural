$(document).ready(function() {
	 $('#borrar').submit(function() {
		   return confirm("¿deseas borrar el alojamiento? Clicka en Ok o Cancel");
	 });
});

jQuery.fn.pintarCampos = function(){
	   $(this).each(function(){
		      elem = $(this);			    			      
		      elem.click(function(e){
		         e.preventDefault();
		         elem = $(this);
		         $("#name").prop("disabled", false);
		         $("#numbertourism").prop("disabled", false);
		         $("#kindaccommodation").prop("disabled", false);
		         $("#web").prop("disabled", false);
		         $("#facebook").prop("disabled", false);
		         $("#twitter").prop("disabled", false);
		         $("#instagram").prop("disabled", false);
		         $("#youtube").prop("disabled", false);
		         $("#province").prop("disabled", false); //No se debería editar al igual que el pueblo
		         $("#village").prop("disabled", false);
		         $("#address").prop("disabled", false);
		         $("#postalcode").prop("disabled", false);
		         $("#country").prop("disabled", false);
		         $("#latitude").prop("disabled", false);
		         $("#longitude").prop("disabled", false);
		         $("#mobilephone").prop("disabled", false);
		         $("#landline").prop("disabled", false);
		         $("#numberrooms").prop("disabled", false);
		         $("#people").prop("disabled", false);
		         $("#shortdescription").prop("disabled", false);
		         $("#longdescription").prop("disabled", false);
		         $("#fileupload").prop("disabled", false);
		         /*$("#descripcion").prop("disabled", false); //TODO: Estos tres parece que solo lee el primero
		         $("#orden").prop("disabled", false);
		         $("#botonborrarfoto").each(show());*/
		         //$("#uploaded-files :input").attr("enabled", "enabled");
		         $(".feature-input").prop("disabled", false);
		         $("#uploaded-files :input").prop("disabled", false);
		         $("#guardar").show();	
		         $("#actualizaralojamiento").hide();
		      });
		   });
		   return this;
		} 
$(document).ready(function(){ 
	$("#actualizaralojamiento").pintarCampos(); 
}); 


