var calendars = {};

$(document).ready( function() {

    // Assuming you've got the appropriate language files,
    // clndr will respect whatever moment's language is set to.
    moment.locale('es');

	// Make sure that your locale is Working correctly
	console.log(moment().calendar())

    // Here's some magic to make sure the dates are happening this month.
    var thisMonth = moment().format('YYYY-MM');
    
    // =========================================================================
    calendars.selectedDate = $('#selected-date').clndr({
    	//weekOffset: (1), //if locale is working this is not needed
    	//daysOfTheWeek: ['D', 'L', 'M', 'X', 'J', 'V', 'S'], //if locale is working this is not needed
    	/*clickEvents: {
            click: function (target) {
          
            }
        },*/
        // If you want to prevent the user from navigating the calendar outside
        // of a certain date range (e.g. if you are making a datepicker), specify
        // either the startDate, endDate, or both in the constraints option. You
        // can change these while the calendar is on the page... See documentation
        // below for more on this!
        constraints: {
            startDate: moment(),
            endDate: moment().add(12, 'months') //only booking of one year
        },
        template: $('#clndr-template').html()
    });

});
