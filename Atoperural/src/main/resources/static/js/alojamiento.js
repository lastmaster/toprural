$(document).ready(function () {
 $("input.mandatory").on("keyup",function () {
  option=false;
  $("input.mandatory").each(function () {
   if (!this.value) {
    option=true;
   }
  });
  $("input[type=submit]").attr("disabled",option);
 });
});

function Valida(formulario) {
    /* Validación de campos NO VACÍOS */
    /*if ((formulario.campo1.value.length == 0) || (formulario.campo2.value.length ==0) || (formulario.cpostal.value.length ==0) || (formulario.dni.value.length ==0) || (formulario.email.value.length ==0)) {
        alert('Falta información');
        return false;
    }*/
    if (isNaN(formulario.landline.value)) {
        alert('El teléfono fijo debe ser un número');
        return false;
    }
    if (isNaN(formulario.mobilePhone.value)) {
        alert('El teléfono móvil debe ser un número');
        return false;
    }
    if (isNaN(formulario.numberRooms.value)) {
        alert('El número de habitaciones debe ser un número');
        return false;
    }
    /* validación del CÓDIGO POSTAL*/
    var ercp=/(^([0-9]{5,5})|^)$/;
    if (!(ercp.test(formulario.postalCode.value))) {
        alert('Contenido del código postal no es un código postal válido');
        return false; }
    /* validación del e-mail */
    /*var ercorreo=/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/;        
    if (!(ercorreo.test(formulario.email.value))) {
        alert('Contenido del email no es un correo electrónico válido.');
        return false; }*/
    /* si no hemos detectado fallo devolvemos TRUE */
    return true;
}

var calendars = {};

$(document).ready( function() {

    // Assuming you've got the appropriate language files,
    // clndr will respect whatever moment's language is set to.
    moment.locale('es');

	// Make sure that your locale is Working correctly
	console.log(moment().calendar())

    // Here's some magic to make sure the dates are happening this month.
    var thisMonth = moment().format('YYYY-MM');
    
    // =========================================================================
    calendars.selectedDate = $('#selected-date').clndr({
    	//weekOffset: (1), //if locale is working this is not needed
    	//daysOfTheWeek: ['D', 'L', 'M', 'X', 'J', 'V', 'S'], //if locale is working this is not needed
    	trackSelectedDate: true,
        clickEvents: {
            click: function (target) {
                console.log('calendarAccomodation clicked: ', target);
                console.log('calendarAccomodation clicked on day: ', target.date._i); // Only YYYY-MM-DD format
                console.log('calendarAccomodation clicked on day: ', target.date._d); // Full date format
                //$(target.element).closest( "input" ).val(target.date._i);
            }
        },
        // If you want to prevent the user from navigating the calendar outside
        // of a certain date range (e.g. if you are making a datepicker), specify
        // either the startDate, endDate, or both in the constraints option. You
        // can change these while the calendar is on the page... See documentation
        // below for more on this!
        constraints: {
            startDate: moment(),
            endDate: moment().add(12, 'months') //only booking of one year
        },
        template: $('#clndr-template').html()
    });

});



