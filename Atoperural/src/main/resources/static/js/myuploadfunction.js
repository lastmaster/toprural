$(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        
        done: function (e, data) {
        	$("#uploaded-files tr:has(td)").remove();
            $.each(data.result, function (index, file) {
                $("#uploaded-files").append(
	        		$('<tr/>')
	        		.append($('<td><input name="fotografias[' + index + '].nombreFoto" value="' + file.fileName + '" readonly="true"/></td>'))
	                .append($('<td><input name="fotografias[' + index + '].descripcion" value=""/></td>'))
	                .append($('<td><input name="fotografias[' + index + '].orden" value=' + index + ' /></td>'))
	                .append($('<td><input type="button" id="botonborrarfoto" class="botonborrarfoto" value="Borrar" /></td>'))
	                .append($('<td><input name="fotografias[' + index + '].tamano" value="' + file.fileSize + '" type="hidden"/></td>'))
	                .append($('<td><input name="fotografias[' + index + '].tipoFichero" value="' + file.fileType + '" type="hidden"/></td>'))
	        		)
           }); 
        },
        
        progressall: function (e, data) {
	        var progress = parseInt(data.loaded / data.total * 100, 10);
	        $('#progress .bar').css(
	            'width',
	            progress + '%'
	        );
   		},
   		
		dropZone: $('#dropzone')
    });
});