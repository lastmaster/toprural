$(document).ready(function () {
 $("input[type=text]").on("keyup",function () {
  option=false;
  $("input[type=text]").each(function () {
   if (!this.value) {
    option=true;
   }
  });
    $("input[type=submit]").attr("disabled",option);
 });
});


function nif(dni) {
  var numero
  var letr
  var letra
  var expresion_regular_dni
 
  expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
 
  if(expresion_regular_dni.test (dni) == true){
     numero = dni.substr(0,dni.length-1);
     letr = dni.substr(dni.length-1,1);
     numero = numero % 23;
     letra='TRWAGMYFPDXBNJZSQVHLCKET';
     letra=letra.substring(numero,numero+1);
    if (letra!=letr.toUpperCase()) {
       alert('Dni erroneo, la letra del NIF no se corresponde');
       return false;
     }else{
       return true;
     }
  }else{
     alert('Dni erroneo, formato no válido');
     return false;
   }
}

function Valida(formulario) {
    /* validación del e-mail */
    var ercorreo=/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/;        
    if (!(ercorreo.test(formulario.email.value))) {
        alert('Contenido del email no es un correo electrónico válido.');
        return false; }
    /* validación numérica*/
    if (isNaN(formulario.telefono.value)) {
        alert('El teléfono debe ser un número');
        return false;  }
    /* validación del DNI */
    if (!nif(formulario.dni.value))	{
   	 return false; }
    /* Validación de campos NO VACÍOS */
    /*if ((formulario.campo1.value.length == 0) || (formulario.campo2.value.length ==0) || (formulario.cpostal.value.length ==0) || (formulario.dni.value.length ==0) || (formulario.email.value.length ==0)) {
        alert('Falta información');
        return false;
    }*/
    /* validación del CÓDIGO POSTAL*/
    /*var ercp=/(^([0-9]{5,5})|^)$/;
    if (!(ercp.test(formulario.cpostal.value))) {
        alert('Contenido del código postal no es un código postal válido');
        return false; }*/ 
    /* si no hemos detectado fallo devolvemos TRUE */
    return true;
}