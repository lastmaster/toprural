---
book: true
title: 'Proyecto Fin de Master: AToperural'
subtitle: Diseño, implementación, verificación y despliegue de una aplicación web de gestión de reservas
author:
    - Carlos Godoy Trapero
    - Daniel Crespo Ramirez
    - Jose Luis Pastor Rodriguez
    - Sergio García González
date: 10 de Junio 2019
lang: es
logo: "logo.pdf"
logo-width: 300
titlepage: true
link-citations: true
toc-title: Indice de contenidos
csl: style.csl
numbersections: true
classoption: 
   - oneside 
   - openany
top-level-division: part
references:
- type: article-journal
  id: chrisFidaoHexagonalArch
  author:
  - family: Fidao
    given: Chris
  issued:
    date-parts:
    - - 2014
  title: 'Hexagonal Architecture'
  URL: https://fideloper.com/hexagonal-architecture
  language: en-GB
- type: article-journal
  id: uncleBobCleanCode
  author:
  - family: Martin
    given: Robert C.
  issued:
    date-parts:
    - - 2012
      - 8
      - 13
  title: 'The Clean Architecture'
  URL: https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html
  language: en-GB
...