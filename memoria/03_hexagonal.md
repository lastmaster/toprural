# Arquitectura hexagonal

La asignación de responsabilidades a módulos de la aplicación y las relaciones entre ellos se han desarrollado siguiendo los principios propuestos por la arquitectura hexagonal. En este capítulo describimos cuáles son estos principios a nivel teórico.

## Introducción

La arquitectura hexagonal también es conocida como arquitectura de puertos y adaptadores. Fue introducida por Alistair Cockburn, y sintetizada por Robert C. Martin junto con otras propuestas similares de otros autores en el artículo [-@uncleBobCleanCode].

La arquitectura hexagonal propone una serie de reglas a seguir en el desarrollo de los módulos, con el objetivo de que la lógica de negocio de la aplicación funcione independiente los elementos ajenos a ella. Algunos ejemplos de elementos ajenos a la lógica de negocio de una aplicación son su interfaz de usuario, la tecnología de persistencia de datos, las comunicaciones con otras aplicaciones, o la infraestructura sobre la que se ejecuta el software.

Haciendo que la nuestra lógica de negocio funcione independientemente de todo lo demás conseguimos que sobre ésta puedan actuar diferentes agentes, y que el comportamiento exhibido sea el mismo en todos los casos. Como ejemplos de  diferentes agentes que pueden interactuar con la lógica de negocio podemos citar la interfaz de usuario, otros sistemas, o una batería automática de pruebas.

## Conceptos

La explicación de los diferentes conceptos de este patrón arquitectural parte de un diagrama en el que se representa a la aplicación como un polígono, dividido en diferentes capas concéntricas (ver figura \ref{hexagon}). Cada lado del polígono representa un elemento externo a la aplicación con el que puede existir una interacción, en cualquier sentido. Es común que este polígono sea un hexágono en  muchas representaciones, de ahí el nombre de la arquitectura. No obstante, no es estrictamente necesario que sean 6 los elementos externos a la aplicación, siempre podrá haber más o menos según corresponda.

![Representación de una aplicación que sigue el patrón de arquitectura hexagonal\label{hexagon}](https://cdn-images-1.medium.com/max/1600/1*yR4C1B-YfMh5zqpbHzTyag.png)

Las diferentes capas concéntricas representan diferentes ámbitos del software. El número de capas en las que estructurar la aplicación tampoco debe tomarse en sentido estricto, podrían definirse tantas capas como sea conveniente. Lo importante es que el nivel de abstracción de cada capa crece de fuera hacia dentro.

El diagrama mostrado en la figura \ref{hexagon} representaría una aplicación estructurada en tres capas:

1. **Dominio, o lógica de negocio.** Encapsula las reglas de negocio a alto nivel de abstracción. Esta debería ser la parte más estable de la aplicación, la menos afectada por cambios externos. _Ejemplo: una aplicación de banca no debe permitir retirar más dinero que el disponible. Esta regla no debe cambiar en ningún caso, por tanto no debe verse afectada por ningún cambio externo, ha de pertenecer al ámbito más interno._
3. **Casos de uso.** Encapsula reglas de negocio que son específicas a la aplicación. Esta capa sí puede verse afectada por cambios en la operativa de la aplicación. No obstante, los cambios en la lógica de negocio no deben afectar en ningún caso al dominio. _Ejemplo: en una aplicación de banca, la decisión sobre cómo actuar ante un usuario que intenta retirar más dinero del que tiene disponible puede no ser la misma en función de diferentes circunstancias. Puede querer mostrarse un mensaje de error, u ofrecer al cliente un préstamo, si éste es solvente. Al ser una lógica que depende de más factores, puede ser más inestable, por tanto tiene sentido que forme parte de una capa más externa. En este caso tendríamos dos casos de uso: retirada de dinero para cliente solvente y para cliente insolvente._
4. **Infraestructura.** En esta capa encontraríamos todos los detalles de menor nivel de abstracción del software: implementación de la interfaz de usuario, del almacenamiento de datos, etc. Esta sería la capa más susceptible de sufrir cambios frecuentes. _Ejemplo: en una aplicación de banca, necesitaríamos una base de datos con las cuentas de los clientes, una web para que éstos puedan acceder a operar, etc._

### Regla de las dependencias

Las dependencias entre elementos de las diferentes capas deben apuntar siempre hacia el interior del hexágono. En una determinada capa no puede nombrarse absolutamente nada que pertenezca al ámbito de una capa más externa. Cuando desde una capa sea necesario interactuar con el exterior utilizaremos el principio de inversión de dependencias: dentro de dicha se emplearán interfaces, cuya implementación pertenecerá al ámbito de una capa más externa.

El dominio de nuestra aplicación, la lógica de negocio, es el centro de todas las capas. Ningún elemento del dominio puede estar acoplado a ningún otro elemento que no pertenezca al propio dominio.

Tampoco está permitido que desde un lado del hexágono se acceda a lógica de otro lado del hexágono sin pasar por el dominio. Por ejemplo, supongamos una clase responsable enviar notificaciones a los usuarios por correo electrónico. Esta clase pertenecería a la capa de infraestructura, pues el correo electrónico es un detalle de implementación de las notificaciones. Esta clase no tendría permitido acceder directamente a la base de datos de la aplicación, si no es a través del dominio.

### Puertos

Los puertos son las definiciones de las interfaces públicas de la lógica de negocio de la aplicación. Vamos a distinguir entre dos tipos de puertos:

* **Puertos primarios**: las interfaces que hacen de puntos de entrada hacia la lógica de negocio. Recogen unos determinados datos de entrada, y ofrecen a la salida los datos resultado del procesamiento.
* **Puertos secundarios**: las interfaces a través de las cuales la lógica de negocio interactúa con elementos ajenos a ella durante su ejecución. Estas interfaces reciben el nombre de repositorios.

### Adaptadores

Un adaptador es una implementación de un puerto para un contexto concreto.

Por ejemplo, una lógica de negocio que necesita recuperar datos de un sistema de almacenamiento, sin importarle si dicho sistema de almacenamiento es un fichero de texto plano o una base de datos, va a interactuar con un puerto secundario para obtener los datos. Dicho puerto secundario pertenecerá al dominio de la aplicación, a la propia lógica de negocio. Para dicho puerto secundario existirá al menos un adaptador que implementará los detalles sobre cómo acceder a los datos, en función de la tecnología de almacenamiento. Los adaptadores quedan fuera del dominio de la aplicación.

### Cruce de fronteras

Como hemos discutido, la regla de las dependencias impone que el dominio de la aplicación no se acople bajo ningún concepto a ningún otro ámbito, y para interactuar desde el dominio con el exterior del hexágono utilizaremos el principio inversión de dependencias. Puede llegar a darse el caso de que la lógica de negocio deba ceder el control a la capa de infraestructura, pasándole una serie de datos para que opere con ellos. Por ejemplo, cuando sea necesario guardar un dato en un sistema de almacenamiento. En este caso, los datos serán entidades propias del dominio. En el punto de entrada a la capa de infraestructura desde el dominio tendremos entidades propias de el dominio, y será responsabilidad de la infraestructura transformar estas entidades del dominio a entidades propias de la tecnología seleccionada para la implementación de la infraestructura.

En sentido contrario, desde la infraestructura hace el dominio, tendremos una situación parecida. La infraestructura también debe responsabilizarse de la conversión de entidades propias de la infraestructura a entidades propias del dominio, pues si la responsabilidad recayera en el dominio estaríamos violando el la regla de las dependencias. Por tanto, la infraestructura debe ser capaz de convertir entidades también en el sentido desde la infraestructura hacia el dominio. No obstante, en este sentido la conversión podría omitirse: si la infraestructura fuera capaz de generar y operar directamente entidades del dominio, no se violaría la regla de las dependencias y se ahorraríamos una conversión.

## Ventajas

El hecho de que la lógica de negocio no esté acoplada a ningún elemento externo hace que ésta presente una serie de propiedades muy deseables para un sistema de software.

* Será una lógica más fácil de evolucionar, al no presentar ninguna dependencia contra tecnologías concretas y tener un alto nivel de abstracción.
* Será posible escribir pruebas automatizadas que verifiquen el correcto funcionamiento de la lógica, empleando dobles, stubs o mocks para las interfaces de las que depende.
* Será relativamente fácil portar la lógica a otra infraestructura diferente, pues la frontera entre dominio e infraestructura queda claramente delimitada por los puertos.

## Inconvenientes\label{inconvenientes}

El principal inconveniente de este tipo de arquitectura tiene que ver con las transformaciones de entidades necesarias para el cruce de fronteras. Tener que mantener lógica de conversión de entidades en ambos sentidos hace que se produzca una cascada de cambios cuando queremos hacer modificaciones sobre las entidades del dominio. Al añadir o eliminar atributos de estas entidades tendremos que modificar también aquellas clases responsables de las conversiones.

## Arquitectura hexagonal en Atoperural

Para el desarrollo de la aplicación Atoperural hemos decidido basarnos en los principios de la arquitectura hexagonal. La principal motivación es tener una aplicación cuyas fronteras entre lógica de negocio e infraestructura estén perfectamente delimitadas. Creemos que esto es fundamental para el desarrollo de una aplicación web, ya que en este campo con frecuencia emergen nuevas tecnologías y herramientas. Muchas de ellas son proyectos de código abierto, mantenidos por una comunidad de desarrolladores, por lo que no hay garantía a largo plazo del soporte que vaya a ofrecer la comunidad a dichos proyectos. Tener una lógica de negocio totalmente desacoplada de la tecnología hará que podemos re-evaluar periódicamente las decisiones tomadas al respecto de la misma, pues la aplicación estará lista para ser portada a otra infraestructura si fuera necesario. Consideramos que este beneficio compensa el coste a pagar en términos de mantenimiento de la lógica necesaria para la conversión de entidades, que hemos visto en el apartado de Inconvenientes.

# Backend de Atoperural

Como hemos discutido, el backend de la aplicación se ha desarrollado según los principios propuestos por la arquitectura hexagonal. En este capítulo describimos los principales elementos del backend en relación con los conceptos de la arquitectura.

## Modelo de datos del dominio

En el paquete `es.master.portalrural.domain.model` se encuentran las clases que representan el modelo de datos sobre el que va a operar la lógica de negocio de nuestra aplicación. En el estado actual de implementación encontramos las siguientes clases.

* **`Owner`**. Representa al propietario de uno o más alojamientos.
* **`Accommodation`**. Representa a un alojamiento.
* **`Picture`**. Representa una imagen perteneciente a la galería de imágenes de un alojamiento.
* **`Feature`**. Representa una característica que puede presentar algún alojamiento.
* **`AccommodationFeature`**. Representa la asociación entre una característica y el precio al que ésta se ofrece en un alojamiento determinado.
* **`AccommodationCalendar`**. Representa un día en el que un alojamiento está reservado y, por tanto, no debe ser mostrado como disponible en el calendario de disponibilidad.

La figura \ref{datamodel} muestra las relaciones entre las clases del modelo de datos. En el diagrama se han omitido algunos atributos que no son relevantes para las relaciones entre las clases.

![Modelo de datos de la aplicación\label{datamodel}](https://www.plantuml.com/plantuml/svg/ZLB1QiCm3BtxAqIEXHnsxWZTMWOzRNHVeCbgCNFiO3kCCVRlSsARB24ByqcMdjzxadpm0LteMoq4QVGUdZyCE_WI4CyOdytLWYtHVYmShTPCGLbp_q1aszPA76uDuE-htullmZl3PVvPRjJ1PMPRxuanz8uRkAR0RyEFgDb8T0syMVlEyeIVVldWHL6Yx5BWfnPoQRMZx2GtzafHIJwgsub9jQYXSueucIl8R7cYNr04fymD97jogXia9kIubmlBdbYkcIabaPPPtLMmgskeJ3N7YVbI_iHbPnlmCff_4fP5YDKP3d1Ro_Zdwf6Yo8j3V1I_wWy0)

Las clases `AccommodationReduced` y `OwnerReduced` también representan un alojamiento y un propietario, respectivamente, aunque portando únicamente un subconjunto de sus atributos. Esto será conveniente para aquellas circunstancias en las que no se quieran mostrar en la interfaz de usuario los valores de absolutamente todos los atributos de una instancia de `Accommodation` u `Owner`.

## Puertos primarios

En el paquete `es.master.portalrural.domain.ports.primary` se encuentran las clases que encapsulan los puntos de entrada a la lógica de negocio desde la capa externa a ella. De esta forma vamos a definir inequívocamente como queremos que se interactúe con nuestra lógica de negocio. Las clases en este paquete representan los _casos de uso_ de nuestra lógica de negocio. Como se discute en [@chrisFidaoHexagonalArch], el propósito de estos casos de uso es simplemente existir, pues su mera existencia sirve al propósito de definir cómo la lógica debe ser usada. Es por ello que no es necesario que implementen una interfaz común. Además es tolerable un alto nivel de acoplamiento de estos casos de uso con las clases de datos del dominio. Las clases que representan los casos de uso aportan su nombre como descripción de la operación que representan, y los tipos de datos que aceptan como parámetro y que ofrecen como salida.

La figura \ref{usecases} muestra los casos de uso implementados actualmente en Atoperural.

![Casos de uso implementados\label{usecases}](https://www.plantuml.com/plantuml/svg/fL912i8m4Bpd5Jdg7qHQ544e29O-82Ih5jgiD0aLnB_RXZOchMq5RydkpEmCkv7IhDAcB0ZX1LEAnaB4d6DPec0wHvafs345z4aeJNAbDq7t3C9m45iA3-16msfDNfrG2pxL4Yerah3b6MhZmK9wecNw9CVnYnz-UiDwCaJGS489oYlDFOqz5A3XNvNMIH359LcQ_3khesNtnioSuIz-mnJC_tdAxSu2v7YZoqSHkBHPZlA2qm4TX6B_6bWw0DEcWh6PhZ6O7u4KpPs_0G00)

En el estado actual de la implementación, la responsabilidad de estas clases se limita a delegar en los puertos secundarios para almacenamiento y recuperación de datos. Algunas de ellas efectúan la misma interacción con un puerto secundario, diferenciándose en las precondiciones que han de cumplirse. A medida que la aplicación crezca, se irán asignando más responsabilidades a estas clases, como por ejemplo, comprobar que el usuario está autorizado a ejecutar el caso de uso en cuestión.

## Puertos secundarios

El paquete `es.master.portalrural.domain.ports.secondary` contiene los repositorios de la lógica de negocio, esto es, las interfaces a través de las cuales la lógica de negocio actúa con elementos fuera del dominio.

La figura \ref{repos} muestra los repositorios de los que se hace uso en el dominio de Atoperural.

![Repositorios o puertos secundarios\label{repos}](https://www.plantuml.com/plantuml/svg/SoWkIImgAStDuUBAp2j9BKfBJ4vLS4nEpi_DpKz9BCdCpmjABSWlpYp9Bwf4LkAMcbXIMfIANTA_F2-r25dA8JKl1MW70000)

Los repositorios implementados actualmente son interfaces que exponen funciones para que el código de los casos de uso pueda ordenar persistir o recuperar instancias del modelo de datos del dominio. Recordemos que las implementaciones de estas interfaces forman parte de la infraestructura.

## Infraestructura

En los paquetes de infraestructura de la aplicación Atoperural encontramos las clases que implementan los detalles de la interactuación entre la lógica de negocio y los elementos externos. Dichos elementos externos son fundamentalmente dos:

* La solución de persistencia de datos.
* La interfaz de usuario.

En estos paquetes utilizamos intensivamente funcionalidades proporcionadas por el framework Spring MVC. Este framework nos abstrae de un gran número de detalles de bajo nivel, como la gestión de los protocolos de comunicación o la gestión de hilos.

### Persistencia de datos

Para la persistencia de los datos de la aplicación hemos elegido como solución una base de datos relacional. La comunicación con la base de datos se lleva a cabo empleando funcionalidades de alto nivel de abstracción ofrecidas por Spring. Estas funcionalidades, a su vez, se apoyan en el framework _Java Persistence API (JPA)_. JPA permite interaccionar con tablas de bases de datos relacionales a través de objetos Java.

El uso de estas abstracciones en el código hace que éste no esté acoplado a una implementación de base de datos concreta. Esto nos permite seleccionar la base de datos a utilizar a través de un parámetro de configuración a la hora de arrancar la aplicación. Es por ello que, sin tocar una línea de nuestro código, podemos seleccionar diferentes bases de datos, según nos convenga:

* Para la ejecución de la aplicación durante el desarrollo de la misma utilizamos _Hibernate_, una base de datos en memoria que apenas requiere configuración. Esto nos ayuda a centrarnos puramente en el desarrollo, pudiendo conocer rápidamente cómo han afectado nuestros cambios a la interacción con la base de datos y depurar el código fácilmente.
* Para la ejecución de la aplicación en entorno de producción utilizamos _MySQL_, que ofrece last prestaciones de rendimiento y escalabilidad adecuadas.

En el paquete `es.master.portalrural.infrastructure.repository.database.entities` se encuentran las clases cuyas instancias serán convertidas en filas de tablas de la base de datos. En términos de JPA, estas clase se denominan entidades.

![Entidades persistibles a través de JPA\label{entities}](https://www.plantuml.com/plantuml/svg/XPBD3i8W48JlF0NfqOI3Putn_wg9Jq1XDKJADa3J6EExIwsrfK7jgKk-6MQdNJhFhQyAJOZGt3bwg0tOdV7At-YTqF1iK6iGNc4nNmc1HO6IXycqq8BouQ6RaKTdbQ37bYQst0FtbONE-ToEmNCaEYenvCfsT6DiZNW5kUK1oRjNr-UBNBh3ZoQvHHJmlSmMgrm3BQqIq2QDgVSM_RqfavEtoboe12UiAfkh6dxuCR9fHYUCqSnagMP9gkslcYZ2NsbSr3-wR_17TGs8J69js4iS1o9BC3Byb4y0)

Nótese que las clases que se muestran en los diagramas de las figuras \ref{datamodel} y \ref{entities} representan lo mismo, pero en diferentes ámbitos. Por ejemplo, tanto `Accommodation` en la figura figuras \ref{datamodel} como `AccommodationEntity` en la figura \ref{entities} representan un alojamiento. Ambas tienen los mismos atributos. La diferencia es que la primera pertenece al dominio de la aplicación, y la segunda es un detalle de implementación de la infraestructura de persistencia. `AccommodationEntity` está fuertemente acoplada a la tecnología de almacenamiento que hemos escogido, base de datos relacional accedida a través de JPA. Si decidiéramos cambiar de tecnología de almacenamiento, `AccommodationEntity` podría cambiar, o incluso desaparecer. No obstante eso no afectaría a nuestra lógica de negocio, que seguiría operando sobre la clase `Accommodation`, que no está acoplada a la infraestructura.

En el paquete `es.master.portalrural.infrastructure.repository` se encuentran las implementaciones de las interfaces mostradas en la figura \ref{repos}.

![Implementaciones de los repositorios\label{repoimpls}](https://www.plantuml.com/plantuml/svg/TT1H2i8m30RWzvwY5zWDV9WWlWhU86QP5DTcf2aYphjRtT2jkjVyV_cQr551DFdEc1xm0cUo3NjmmTwD2qhI0fBT8hBtt80w3aVgEJfbkPbPPK-WIUX_UBW6abdq-6ekj09H9U7hULQnWnYNuWuKJX39JE5abUEtyXbcO-NRTZDKrOhmiyPOBiLYeR6qz4rDeSc7VG80)

La figura \ref{repoimpls} muestra las relaciones entre las interfaces del dominio y las clases de la infraestructura. La responsabilidad de estas clases de la infraestructura será convertir una instancia de una clase del modelo de datos del dominio (figura \ref{datamodel}) en una entidad (figura \ref{entities}) y persistirla a través de las abstracciones ofrecidas por Spring y JPA. En sentido inverso, estas clase también son responsables de recuperar una entidad y transformarla en una instancia del modelo de datos del dominio.

En el paquete `es.master.portalrural.infrastructure.repository.database.repositories` encontramos interfaces que extienden la interfaz de Spring `JPARepository`. Éstas son las abstracciones que efectivamente nos permiten persistir y recuperar entidades, totalmente acopladas a la tecnología, a diferencia de los repositorios de la figura \ref{repos}.

### Interacción con la interfaz de usuario

La interfaz de usuario de Atoperural es una aplicación web. Los datos se presentan al usuario formateados en HTML. La entrada de datos por parte del usuario se realiza mediante formularios web.

El framework Spring ofrece funcionalidad para transformar en objetos Java los datos de entrada introducidos en los formularios web. Para ello es necesario definir en el backend clases que dispongan de campos cuyos nombres coincidan con los nombres de los campos de los formularios. Las clases que hemos definido a tal efecto se encuentran en el paquete `es.master.portalrural.infrastructure.webform`. La figura \ref{webformclasses} muestra estas clases y sus relaciones.

![Clases para la recogida de datos de los formularios web\label{webformclasses}](https://www.plantuml.com/plantuml/svg/dP912y8m38Nl_HLXHy63vubC78E1e3VFNPVfi6r6so4Y_dUhcqpPniGUN_8bUOz6rd7ZQYKP4v9R2xkBHfFgWX93we1PGKQzQsiXI2dAkIj9VzJXni2_BUaZq8lF6w6qRlaD8NUrmGzs1KMZsR1ZzgNejLMDPaScRxX4dNFJCicBoOZEcCVyQjdzRQ8_kptVDqla6iG9XQSwOiB12FxKOgepYL2PKc3OPTGwJiTs37XXR3JyO170R3w7G0TJf_usf9_G7yi7FKHUyd_j0G00)

La figura \ref{webformclasses} nos muestra un diagrama de clases muy similar a los ya presentados en las figuras \ref{datamodel} y \ref{entities}. De nuevo tenemos clases que representan lo mismo (alojamientos, propietarios, etc.), pero en esta ocasión pertenecen al ámbito de la interfaz de usuario. Es por ello que en el diagrama podemos ver que algunas de las clases contienen el atributo `checked`, que permite comprobar si el usuario ha marcado determinados checkboxes en el formulario. Obviamente éste es un atributo que tiene razón de ser en ámbito de la interfaz, pero no en el modelo de datos del dominio. Si reemplazaremos los formularios web por otro tipo de interfaz, éstas clases desaparecerían, pero no se alterarían para nada las clases del dominio, ni las entidades persistibles.

En sentido contrario, el backend se encargará de producir el código HTML que se mostrará en el navegador del usuario. Para esta tarea hemos utilizado [_Mustache_](https://mustache.github.io/), un motor de renderizado de plantillas. Las plantillas son esencialmente ficheros HTML con huecos a rellenar. _Mustache_ se encarga de rellenar los huecos con valores de atributos de objetos Java. Los objetos a utilizar a este efecto pueden ser perfectamente de clases del modelo de datos del dominio, ya que no hay ninguna restricción para acceder al dominio desde la infraestructura. No obstante es necesario hacer mínimas adaptaciones sobre ellos para evitar que en las páginas web se muestre la palabra `null` cuando un campo no tiene valor.

En este punto podemos apreciar la desventaja de la arquitectura hexagonal discutida en la sección \ref{inconvenientes}. En el backend existen tres clases para representar un mismo concepto. Por ejemplo, para el caso de un alojamiento, tenemos:

* `Accommodation`: Representa al alojamiento en el dominio de la aplicación.
* `AccommodationEntity`: Representa al alojamiento ante la infraestructura de persistencia de datos.
* `AccommodationFromWebForm`: Representa los datos del alojamiento que ha introducido el usuario en un formulario.

Esto hace que tengamos que mantener lógica para transformar instancias de una clase a instancias de otra. Además, los alojamientos tienen relaciones de composición con sus características y sus fotografías. Las relaciones entre las clases también se replican entre los diferentes ámbitos y eso puede ofuscar la lógica de conversión entre los diferentes ámbitos.

### Controladores

El framework Spring se encarga de configurar en nuestra aplicación todo lo necesario para atender peticiones HTTP. Cuando se recibe una petición, Spring delega en nuestro código la elaboración de la respuesta a través de los controladores.

![Controladores\label{controllers}](https://www.plantuml.com/plantuml/svg/ROvH2e0m34F_TmgFeWZYX-A79oXRrKAtGbVn-YeC4VGloKiWTJPGsoCxvnbohXhl9KO9O2IfbMGgpAY5TiGuea6nlKJyT6O4zTllz1kAoQgm40oKxSP0wQ7JSUdNnjMOmlNr1000)

Spring se encarga de invocar métodos de estos controladores, pasando como parámetros datos procedentes de la petición HTTP a responder. Los controladores contienen la configuración necesaria para que Spring resuelva qué método llamar para cada petición y cómo extraer los parámetros de la misma. Es por ello que son clases fuertemente acopladas al framework Spring.

En nuestra aplicación, los controladores se encuentran en el paquete `es.master.portalrural.infrastructure.spring.controllers`. La figura \ref{controllers} muestra los controladores implementados actualmente. A estas clases les hemos asignado la responsabilidad de invocar a los casos de uso de nuestro dominio (figura \ref{usecases}). Para ello cada controlador tendrá como colaboradores instancias de los casos de uso que deba invocar. Para cumplir esta función, los controladores tendrán que transformar los datos de entrada a los tipos esperados por los casos de uso. Spring se va a encargar de extraer datos de la petición HTTP y ofrecérselos al controlador como instancias de las clases del paquete `es.master.portalrural.infrastructure.webform` (figura \ref{webformclasses}). Será responsabilidad de los controladores transformar estas instancias a otras de tipos del dominio de la aplicación (figura \ref{datamodel}).

Cuando los casos de uso terminan su ejecución, los controladores se encargan de pasar los datos de salida a Spring, a través de la clase `org.springframework.ui.Model`. Aquí se harán las transformaciones necesarias para la correcta visualización de los datos en la web, como por ejemplo, el reemplazo de valores `null` por cadenas vacías. Posteriormente Spring delegará en el motor de Mustache para renderizar las páginas HTML.
