
# Single Sign On

El Single Sign On nos permite logarnos en una aplicación web con los credenciales de otro sitio web sin conocer el usuario y password. Eso por un lado facilita al cliente registrarse en nuestra aplicación y le evita tener que memorizar otro dato de acceso más (usuario/password).

Por otro lado también facilita el desarrollo de nuestra aplicación web ya que nos evitamos las pantallas de recogida de la información básica de un usuario. Ciertamente no nos libera de tener que modelar un usuario interno con el que vincular por ejemplo reservas, métodos de pagos, etc.

Actualmente hay varios protocolos que se usan para SSO como por ejemplo OpenID Connect, OAuth2, JOSE, y SCIM (por nombrar algunos).

Para hacer una pequeña prueba hemos elegido OAuth2.

## OAuth2

OAuth 2.0 es el protocolo estándar industrial para autorización. Lo utilizan la mayoría de sitios web importantes tales como:

* Amazon
* Bitbucket
* Facebook
* Github
* Google

De hecho aprovechamos esto en nuestro entorno de integración continua para enganchar Bitbucket y Circleci entre ellos.

## SSO con Facebook

Para enganchar nuestra aplicación web mediante OAuth2 con Facebook primero tenemos que crear una cuenta en *`developers.facebook.com`*. Una vez creada tenemos que crear una aplicación nueva (`atoperural` ver imagen \ref{facebook1}).

![Aplicación Facebook\label{facebook1}](facebook1.PNG)

Luego hay que indicarle desde que sitios está autorizado logarse a la aplicación. Eso nos lleva a dos workarounds que hicimos para poder probar la aplicación:

* **HTTPS** OAuth2 con Facebook solo permite accesos desde páginas con *https*
* **`https://www.atoperural.es`** No podemos poner `localhost` ya que los servicios kubernetes se generan con direcciones ip aleatorias. Para simular un *dns* en el que `www.atoperural.es` hemos implementado un script que nos *publica* la ip del servicio en esa url.

&nbsp;

```{caption="publish" label=publish_script .bash}
    #!/bin/bash
    sed -i '/atoperural/d' /etc/hosts
    echo `kubectl get svc | grep 'atoperural ' | tr -s ' ' | cut -d' ' -f 3` " www.atoperural.es" >> /etc/hosts
```

&nbsp;

```{caption="Invocar publish" label=publish_call .bash}
    chmod +x publish
    sudo ./publish
```

![Aplicación Facebook\label{facebook2}](facebook2.PNG)

&nbsp;
```{caption="application.yml" label=sso .yaml}
    security:
    oauth2:
        client:
        clientId: 798792063854060
        clientSecret: dc6d8276b9fe2604dc9e22c67c0ba116
        accessTokenUri: https://graph.facebook.com/oauth/access_token
        userAuthorizationUri: https://www.facebook.com/dialog/oauth
        tokenName: oauth_token
        authenticationScheme: query
        clientAuthenticationScheme: form
        resource:
        userInfoUri: https://graph.facebook.com/me
```
## Cambios en el código y páginas

Para logarse con un Facebook hay que añadir un recurso de `login` que se conecta con Facebook y devuelve el usuario [ver \ref{login}].

&nbsp;


```{caption="Application.java" label=login .java}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	    
		http.csrf().disable().antMatcher("/**").authorizeRequests().antMatchers( "/", "/login**", "/webjars/**")
				.permitAll().anyRequest().authenticated().and().
				logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/").deleteCookies("JSESSIONID")
				.invalidateHttpSession(true).clearAuthentication(true); 
		http.headers().frameOptions().disable();
		
	}

	@RequestMapping("/user")
	public Principal user(Principal principal) {
		return principal;
	}
```

Y en las páginas HTML hay que añadir lo siguiente para recuperar el usuario [ver \ref{auth_javascript}]:

&nbsp;

```{caption="Authenticated HTML Javascript" label=auth_javascript .javascript}
$.get("/user", function(data) {
    $("#user").html(data.userAuthentication.details.name);
    $(".unauthenticated").hide()
    $(".authenticated").show()
  });

var logout = function() {
    $.post("/logout", function() {
      $("#user").html('');
      $(".unauthenticated").show();
      $(".authenticated").hide();
    });
    return true;
  }

if (window.location.hash == '#_=_'){
      history.replaceState 
          ? history.replaceState(null, null, window.location.href.split('#')[0])
          : window.location.hash = '';
  }
```

El botón de *`Login`* y *`Logout`* en HTML sería así [ver \ref{auth_html}]:

&nbsp;

```{caption="Authenticated HTML" label=auth_html .html}
  <div id="wrapper">
    <div class="unauthenticated">
      <a class="btn" href="/login">Login</a>
    </div>
    <div class="authenticated" style="display: none">
      Logged in as: <span id="user"></span>
      <div>
        <button onClick="logout()" class="btn">Logout</button>
      </div>
    </div>
  </div>
```