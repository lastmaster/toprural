
# Instalación

## Generación de imágenes Docker

### Imagen frontend web

Se genera con el siguiente comando:

&nbsp;

```bash
docker build -t jpastorr/atoperural:latest . -f docker/Dockerfile.atoperural
```

### Imagen Mysql

Se genera con el siguiente comando:

&nbsp;

```bash
docker build -t jpastorr/atoperural-db:latest docker -f docker/Dockerfile.mysql
```

### Docker Registry

* atoperural front-end y back-end:
   * https://cloud.docker.com/u/jpastorr/repository/docker/jpastorr/atoperural
* e2e entorno:
   * https://cloud.docker.com/u/cgodoytrapero/repository/docker/atoperural-e2e



## Ejecución de los tests

*Unit Test* y *Component Test* se ejecutarían con el siguiente comando:

&nbsp;

```Bash
$ mvn test -Dspring.profiles.active=prod -f Atoperural/pom.xml
```

Mientras que el *End To End Test* se ejecutarían con el siguiente *script*:

&nbsp;

```bash
$ ./e2e/e2e.sh
```


## Despliegue de la aplicación sobre Kubernetes

Se realiza el despliegue y la interacción mediante un *browser*, como puede ser *Firefox*, con los siguientes comandos:

&nbsp;

```bash
helm install helm/atoperural -n atoperural
sudo ./publish
firefox https://www.atoperural.es &
```
