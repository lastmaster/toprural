
# Tests en Atoperural

## Unit Tests

Se han escrito pruebas unitarias para cada clase del backend que exhibe comportamiento. Esto deja fuera a las clases de datos, que son las clases del modelo de datos del dominio, las entidades persisistibles, y las clases que recogen los datos de entrada desde los formularios web. Estas clases no tienen comportamiento, simplemente ofrecen _setters_ y _getters_ para establecer y recuperar los valores de sus campos. Esto es una funcionalidad trivial, por ello consideramos que no aportaría valor escribir pruebas para ella.

Para probar clases que colaboran con otras clases necesitamos mocks de las clases colaboradoras. Hemos utilizado el framework de mocks para Java [_Mockito_](https://site.mockito.org/), que nos permite definir el comportamiento de los mocks y verificar las interacciones del código sujeto de pruebas con los mocks. Así, hemos escrito pruebas para cada una de las clases de manera aislada a las demás.

Las pruebas unitarias de cada clase tratan de ejercitar sus métodos públicos para verificar que éstos hacen lo que se espera de ellos bajo una serie de circunstancias. Cada caso de prueba consiste en:

1. Configurar los mocks para que su comportamiento establezca las circunstancias bajo las que queremos probar el método en cuestión.
2. Invocar al método con unos datos de entrada
3. Verificar que los datos de salida son los esperados.

Para el paso de verificación de los datos de retorno de los métodos hemos utilizado la librería de matchers para Java [_Hamcrest_](http://hamcrest.org/).

## Component Tests

Para el *Component Test* de la aplicación *Atoperural* se ha considerado la tecnología *Selenium* y *JUnit*, que permite emular la interacción con los distintos browsers de forma automática y descriptiva de cada uno de los escenarios a considerar.

Para ello, se ha considerado que estas pruebas probarán ĺa funcionalidad del frontend y backend pero en un entorno de desarrollo, esto es:

* La aplicación será instanciada por el propio test definido en JUnit.

* La aplicación se instanciará en modo developer:
    - Escuchará en el puerto *8081*.
    - Escuchará sobre el protocolo *HTTP* en vez de *HTTPS*.
    - Desplegará sobre una base de datos *H2* de *Spring* en vez de *MySql*.
    - No desplegará funcionalidad relacionada con *K8S*.

![CT flows](ct.png)

Para conseguir dicho comportamiento se han definido dos perfiles de compilación/ejecución a nivel de Maven que son configurados por argumento mediante  el valor de *spring.profiles.active* a los valores de *dev* o *prod*.

En el siguiente bloque de código se puede ver el detalle del perfil de developer definido en el fichero *application-dev.properties*:

&nbsp;

```bash
spring.jpa.hibernate.ddl-auto=update
spring.jpa.database-platform=org.hibernate.dialect.MySQL5Dialect
security.basic.enabled=false
spring.datasource.url=jdbc:h2:mem:testdb
spring.datasource.driver-class-name=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=

server.port = 8081
```

Además, se ha considerado utilizar la librería Java de *testcontainers*, la cuál embebe los drivers y browsers de Selenium en forma de containers.

En nuestro caso, en estos momentos, tenemos pruebas únicamente sobre el browser de *Chrome*, pudiéndose expandir a *Internet Explorer, Firefox, Opera, Safari*,... sin un esfuerzo considerable.

En el caso de esta librería, cuando el objeto *Java BrowserWebDriverContainer* levantará los containers necesarios para la prueba:

&nbsp;

```bash
quay.io/testcontainers/vnc-recorder:1.1.0
selenium/standalone-chrome-debug:2.53.1
quay.io/testcontainers/sshd:1.0.0
quay.io/testcontainers/ryuk:0.2.3
```

Los cuales levantan un browser *Chrome* mediante el comando *xvfb-run* y lo exponen mediante *x11vnc* para que puedan ser grabadas las interacciones con el explorador para una posible depuración posterior.

Para configuración de los tests hemos usado la librería *Java Faker*. Esta librería es el resultado del portado de la librería de *Ruby _faker gem_* (o de la librería de *Perl _Data::Faker_*) que genera datos *fake*. Nos ha sido útil porque conseguimos datos reales y aleatorios sobre los atributos que necesitamos.

Para ello, en el *pom.xml*, se ha añadido la siguiente dependencia.

&nbsp;

```{caption="Dependencia de Faker en el pom.xml" label=fakerdependency .xml}
<dependency>
    <groupId>com.github.javafaker</groupId>
    <artifactId>javafaker</artifactId>
    <version>0.18</version>
</dependency>
```

Desde el código de test de *Java* se instancia así:

&nbsp;

```{caption="Uso de Faker en el código de tests" label=fakeruse .java}
Faker faker = new Faker(new Locale("es")); // Seteamos el locale para que nos de datos de nuestro idioma
String name = faker.name().fullName(); // Sr Antonio Pérez Pepito
String emailValid = faker.bothify("????##@gmail.com"); // Email con 4 letras y 2 números y con la @
String phoneValid = faker.numerify("#########"); //Número de teléfono con 9 dígitos numéricos
String streetAddress = faker.address().streetAddress(); // Calle La Sierra 20
```

Dicho esto, la ejecución de los tests se realiza con el siguiente comando:

&nbsp;

```bash
$ mvn clean test '-Dtest=*.componenttest.*' -Dspring.profiles.active=dev -f Atoperural/pom.xml
```


### End To End Tests

Para el *End To End Test* de la aplicación Atoperural se ha considerado la tecnología Selenium, JUnit, Docker, Helm y K8S, de forma que el testeo de la aplicación se hace en un despliegue de producción:

* La aplicación será *dockerizada* y desplegada en *Kubernetes* a través de *Helm*.

* La base de datos será *MySQL* en un entorno *dockerizado* y desplegado en *K8S* con los recursos necesarios de disco para ello.

* La aplicación escuchará el protocolo *HTTPS* en el puerto *8543*.

* Toda la funcionalidad *K8S* se considerará en el despliegue.

* El despliegue de la aplicación constará de dos instancias del *front-end Atoperural* y una instancia de *MySQL*.

El perfil de *production* es el seteado para la generación de las imágenes de *Atoperural* en un despliegue real, que será el considerado en este tipo de pruebas. A continuación se puede ver el detalle de dicho perfil de despliegue:

&nbsp;

```bash
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://atoperural-db:3306/atoperural_db?verifyServerCertificate=false&useSSL=true
spring.datasource.username=atoperural
spring.datasource.password=secret
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5Dialect

server.port = 8543
server.ssl.key-store = classpath:keystore.jks
server.ssl.key-store-password = secret
server.ssl.key-password = secret
server.ssl.keyStoreType = JKS
server.ssl.keyAlias = server-alias
```

En este caso, se utilizará el *WebDriver* de *Selenium* directamente en vez de los *test containers* utilizados en el *Component Test*. No obstante, dicho entorno de test será *dockerizado* para evitar una instalación del *browser* y el *driver* en cada entorno.

Al igual que en el *Component Test*, se ha considerado en estos momentos únicamente el *Chrome* como *browser* en el *testing*.

&nbsp;

```Dockerfile
FROM selenium/standalone-chrome:3.4.0

USER root

RUN apt update

# java
RUN apt install -y openjdk-8-jdk

# maven because of apt java dependencies  
RUN wget http://apache.mirrors.spacedump.net/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.zip
RUN unzip apache-maven-3.6.1-bin.zip
RUN mv apache-maven-3.6.1/ /usr/local/bin/
RUN rm apache-maven-3.6.1-bin.zip
ENV PATH=$PATH:/usr/local/bin/apache-maven-3.6.1/bin/

# source
COPY . /code
WORKDIR /code

RUN echo "#!/bin/bash \n mvn verify -Dskip.surefire.tests -Dspring.profiles.active=prod -f Atoperural/pom.xml" > ./entrypoint.sh
RUN chmod +x ./entrypoint.sh

ENTRYPOINT [ "./entrypoint.sh" ]
```

Además, se ha considerado un script sencillo para automatizar la generación de las imágenes, despliegue, test y desinstalación de la aplicación en el *e2e.sh*:

&nbsp;

```bash
#!/bin/bash

repoPath=`git rev-parse --show-toplevel`

# setup
docker build -t jpastorr/atoperural:latest -f ${repoPath}/docker/Dockerfile.atoperural ${repoPath}
docker build -t jpastorr/atoperural-db:latest -f ${repoPath}/docker/Dockerfile.mysql ${repoPath}
helm install ${repoPath}/helm/atoperural -n atoperural

# test
atoperuralIP=`kubectl describe svc atoperural  | grep -w IP | tr -s ' ' | cut -d' ' -f2`
echo "www.atoperural.es IP ${atoperuralIP}"

docker build -t cgodoytrapero/atoperural-e2e -f ${repoPath}/docker/Dockerfile.e2e ${repoPath}
docker run --rm --add-host="www.atoperural.es:${atoperuralIP}" -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v ~/.Xauthority:/root/.Xauthority --network=host cgodoytrapero/atoperural-e2e

# cleanup
helm delete --purge atoperural
```

Hay que considerar que el entorno de pruebas, es decir, el *container cgodoytrapero/atoperural-e2e*, necesita un servicio *DNS*, pero en estos momentos, el entorno de *K8S* es un *Minikube* en un servidor *Ubuntu* aislado. Con lo que emulamos el servicio *DNS* sobre *Docker* a través de añadir la *IP* del *cluster* al */etc/host* del *container*, como se puede ver en el script anterior.

![E2E flows](e2e.png)

Por tanto, para lanzar el *End To End Test*, se debería lanzar el siguiente comando:

&nbsp;

```bash
$ ./e2e/e2e.sh
```
