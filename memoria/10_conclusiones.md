# Conclusiones

Hemos sido capaces de escribir un backend para la aplicación web en el que la lógica de negocio está totalmente desacoplada de la infraestructura de persistencia de datos, de la interfaz de usuario, y de Spring, framework que abstrae nuestro código de detalles como la gestión de hilos o la configuración de los sockets. Los límites del dominio de la aplicación están perfectamente definidos a través de los puertos primarios (casos de uso) y secundarios (repositorios). Gracias a esto, podemos realizar pruebas automatizadas de la lógica de negocio de manera aislada, utilizando mocks de los elementos externos necesarios. Además, en caso de querer cambiar alguno de los elementos externos (infraestructura de persistencia, interfaz o framework), los paquetes que sufrirían los cambios estarían perfectamente acotados, y en ningún caso los cambios llegarían a nuestra lógica de negocio.

Gracias a las pruebas unitarias que hemos escrito, podemos conocer cómo cada cambio que hacemos en cualquier clase del backend, por pequeño que sea, afecta a su comportamiento observable. Conociendo el impacto de nuestros cambios podremos efectuar dichos cambios con seguridad, y gracias a ello no será complicado refactorizar el código para mejorar su calidad siempre que sea necesario.

Almacenar las posibles características en la base de datos nos ofrece la posibilidad de añadir nuevas características para los alojamientos, sin necesidad de redesplegar la aplicación. Lo mismo ocurre con los precios. Éste dinamismo es básico de cara al tiempo de respuesta a los propietarios, que serán los que propondrán las características de los alojamientos así como sus precios.

El uso de la herramienta como _Selenium_ combinada con _Javafaker_ nos ha dado una gran versatilidad a la hora de saber si la web funciona correctamente. Esto nos protege ante los posibles bugs que se introduzcan en la web en futuras releases.

## Trabajo futuro

Las funcionalidades que se han quedado fuera de esta versión dadas las limitaciones de tiempo y las prioridades establecidas son las siguientes:

* Gestión de las opiniones de los alojamientos.
* Mejoras en el framework de galería de imágenes.
* Incluir una vista de huésped, con un listado de casas favoritas y la posibilidad de comunicarse con los propietarios.
* Finalización de la funcionalidad de log in. Se ha hecho una prueba de concepto de log in a través de Facebook, pero la idea es que éste log in con las redes sociales sólo aplique a los huéspedes y no a los propietarios, a los que es más fácil entrar con su mail.
* Integración vía ical con otros channel manager del tipo Avaibook, Airbnb o Booking.
* Integración de la información de usuario (Propietario o Huésped) con la información de SSO.
* Integrar con más servicios OAuth2 como Google o Amazon.
