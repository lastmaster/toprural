# Despliegue Atoprural


Para el despliegue hemos optado por una plataforma basada en contenedores. *Kubernetes (K8S)* es una plataforma opensource, portable y extensible para gestionar tareas y servicios basados en contenedores que facilita una configuración declarativa y automatización.

## Helm

Helm es un *package manager* que trabaja por encima de *Kubernetes*. Helm nos permite definir el despliegue de un sistema basado en micro-servicios. Para ello podemos definir *charts* que se forman por plantillas o *templates* y sus correspondientes valores.

Helm además proporciona la gestión de los charts en un registro (similar al de docker) llamado *Helm Workspace*. Para este proyecto no hemos hecho uso de ese registro porque el chart solo va a ser usado por nuestra aplicación.

## Helm Chart de Atoperural

El chart de helm consiste de siguientes elementos [ver imagen \ref{helm}]:

* **Deployments**
	* **atoperural**: Despliega la aplicación SpringBoot principal. Tenemos configurado dos replicas del POD.
	* **atoperural-db**: Despliega la base de datos mysql con una sola instancia.
* **Services**
	* **atoperural**: Servicio que balancea entre las instancias de atoperural y lo expone en el puerto 443.
	* **atoperural**: Servicio expone la base de datos MySQL en el puerto 3306.
	* **hazelcast-service**\label{hazelcast_service}: Servicio interno que usa hazelcast para distribuir sesiones entre los pods de atoperural.
* **Volumes**
	* **atoperural-pv-volume**: Volumen de tipo *`HostPath`* con tamaño de 10G.
	* **atoperural-pv-claim**: Volume claim de tamaño 2G para ser usado como disco de MySql.

![Estructura Helm\label{helm}](Helm.svg)

### Estructura de directorios Helm


El chart de helm tiene la siguiente estructuro de directorio:

&nbsp;

```{caption="Estructura de directorios Helm"}
| atoperural
| |-- Chart.yaml
| |-- templates
| |   |-- atoperural_deployment.yaml
| |   |-- hazelcast_service.yaml
| |   |-- _helpers.tpl
| |   |-- ingress.yaml
| |   |-- mysql_deployment.yaml
| |   |-- mysql_service.yaml
| |   |-- NOTES.txt
| |   |-- pvc.yaml
| |   |-- pv.yaml
| |   |-- service.yaml
| |-- values.yaml
```
