# Arquitectura de Integración Continua

## Servicios involucrados

La arquitectura de integración continua consiste de cuatro servicios cloud:

* **Bitbucket**: Aquí guardamos nuestro código fuente y recursos de la aplicación web. El repositorio de esta aplicación se encuentra en:

	 **`https://bitbucket.org/lastmaster/toprural/src/atoperural/`**

* **circleci**: Cada vez que se publica un commit se lanza un *workflow* de `circleci` (ver \ref{circleci}). El workflow de CI se encuentra en: 

	**`https://circleci.com/bb/lastmaster/workflows/toprural/tree/atoperural`**

* **sonarcloud**: Uno de los jobs de circleci (`sonar`) analiza de forma estática el código y envía los resultados a `sonarcloud`. Los informes de sonarcloud se pueden ver en:

	**`https://sonarcloud.io/organizations/lastmaster/projects`**

* **dockerhub**: Las imágenes de `atoperural` y `atoperural-db`, tanto las temporales que se están probando en circleci como la `latest` (una vez pasen con éxito todas las pruebas), se guardan aquí. Las urls donde se guardan las imágenes son las siguientes:
	* **`atoperural`**:

	**`https://cloud.docker.com/u/jpastorr/repository/docker/jpastorr/atoperural`**

	* **`atoperural-db`**: 

	**`https://cloud.docker.com/u/jpastorr/repository/docker/jpastorr/atoperural-db`**

![Arquitectura Integración continua\label{ci_arch}](CI_Arch.svg)

## Circle CI \label{circleci} 

Circle CI es un servicio de integración continua en la nube. La principal ventaja sobre usar Jenkins es que nos permite usar un servidor común y gratuito[^3], de tal manera que evitamos tener cada integrante del equipo una copia local de Jenkins o tener que pagar por un alojamiento en un servidor dedicado.
Otra ventaja que tiene es que soporta de forma nativa (sin uso de plugins y configuraciones complejas) la ejecución de jobs en contenedores *docker*.

### Conectar CircleCI con el repositorio BitBucket

Cuando creamos una cuenta en **circleci** nos pregunta si nos queremos logar con **Github** o con **Bitbucket** [ver \ref{circleci_login}]. 
Seleccionamos **Bitbucket** y nos ofrecerá la opción de añadir proyectos **`Add projects`** y elegimos el nuestro de **`lastmaster/toprural`**[^4] [ver \ref{circleci_webhook}]


[^4]: `https://circleci.com/add-projects/bb/lastmaster`

![Creación de cuenta en circleci\label{circleci_login}](circleci_login.PNG)

![Creación de webhook con Bitbucket\label{circleci_webhook}](circleci_webhook.PNG)

### Workflow y Jobs de CircleCI

El workflow lo hemos llamado **`build_test_publish`** y consiste de los siguientes jobs:

* **unittests**: Se lanzan los unit tests y las pruebas de Selenium
* **build**: En este job se compila, empaqueta y se genera la imagen docker de `jpastorr/atoperural:<hash_current_commit>`. 
   Se pública en **dockerhub** con un tag correspondiente al commit actual.
* **mysql**: Se general la imagen docker `jpastorr/atoperural-db:latest`.
* **test**: En este job se lanza el helm chart de nuestra aplicación web y es donde se lanzarían las pruebas **e2e**.
* **sonar**: Se analiza de forma estática el código y se envían los resultados a **Sonarcloud**
* **tag**: Si todo ha ido bien se re-etiqueta la imagen a `jpastorr/atoperural:latest` y se publica en **dockerhub**

![Workflow CI\label{ci_workflow}](CircleCI.svg)
 
[^3]: Gratuito para el tamaño y uso que le damos. 

&nbsp;

~~~ {caption="circleci/config.yml" label=circleci_config .yaml}
version: 2
jobs:
  memoria:
    working_directory: /app
    docker:
      - image: jpastorr/pandoc
    steps:
      - checkout
      - run:
          name: Pdf generation PFM
          command: |
            apt-get install texlive-xetex
            cd memoria
            ./pandoc.sh
      - store_artifacts:
          path: /app/memoria/atoperural.pdf

  test:
    machine:
      image: circleci/classic:201808-01
    steps:
      - checkout
      - run:
          name: maven installation
          command: |
            wget http://apache.mirrors.spacedump.net/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.zip
            unzip apache-maven-3.6.1-bin.zip
            sudo mv apache-maven-3.6.1/ /usr/local/bin/
            rm apache-maven-3.6.1-bin.zip
            export PATH=$PATH:/usr/local/bin/apache-maven-3.6.1/bin/
      - run:
          name: ut and ct is launched
          command: |
            mvn clean test -Dspring.profiles.active=dev -f Atoperural/pom.xml 

  build:
    working_directory: /app
    docker:
      - image: docker:17.05.0-ce-git
    steps:
      - checkout
      - setup_remote_docker
      - run:
          name: Install dependencies
          command: |
              apk add --no-cache \
              py-pip=9.0.0-r1
              pip install 
      - restore_cache:
          keys:
            - v1-{{ .Branch }}
          paths:
            - /caches/app.tar
      - run:
          name: Load Docker image layer cache
          command: |
            set +o pipefail
            docker load -i /caches/app.tar | true
      - run:
          name: Build application Docker image
          command: |
            docker build --cache-from=atoperural -t atoperural . -f docker/Dockerfile.atoperural
      - run:
          name: Save Docker image layer cache
          command: |
            mkdir -p /caches
            docker save -o /caches/app.tar atoperural
      - save_cache:
          key: v1-{{ .Branch }}-{{ epoch }}
          paths:
            - /caches/app.tar
      - deploy:
          name: Push application Docker image
          command: |
              docker login -u $DOCKER_USER -p $DOCKER_PASS
              docker tag atoperural "jpastorr/atoperural:${CIRCLE_SHA1}"
              docker push "jpastorr/atoperural:${CIRCLE_SHA1}"
  mysql:
    working_directory: /app
    docker:
      - image: docker:17.05.0-ce-git
    steps:
      - checkout
      - setup_remote_docker
      - run:
          name: Install dependencies
          command: |
              apk add --no-cache \
              py-pip=9.0.0-r1
              pip install
      - restore_cache:
          keys:
            - v1-{{ .Branch }}
          paths:
            - /caches/mysql.tar
      - run:
          name: Load Docker image layer cache
          command: |
            set +o pipefail
            docker load -i /caches/mysql.tar | true
      - run:
          name: Build application Docker image
          command: |
            docker build --cache-from=mysql -t mysql docker -f docker/Dockerfile.mysql
      - run:
          name: Save Docker image layer cache
          command: |
            mkdir -p /caches
            docker save -o /caches/mysql.tar mysql
      - save_cache:
          key: v1-{{ .Branch }}-{{ epoch }}
          paths:
            - /caches/mysql.tar
      - deploy:
          name: Push application Docker image
          command: |
              docker login -u $DOCKER_USER -p $DOCKER_PASS
              docker tag mysql "jpastorr/atoperural-db:${CIRCLE_SHA1}"
              docker push "jpastorr/atoperural-db:${CIRCLE_SHA1}"
              docker tag  "jpastorr/atoperural-db:${CIRCLE_SHA1}" "jpastorr/atoperural-db:latest"
              docker push "jpastorr/atoperural-db:latest"

  sonar:
    
    working_directory: ~/toprural/Atoperural

    docker:
      - image: circleci/openjdk:8-jdk-browsers

    steps:

      - checkout:
          path: ~/toprural

      - restore_cache:
          key: Atoperural-{{ checksum "pom.xml" }}
      
      - run: mvn dependency:go-offline
      
      - save_cache:
          paths:
            - ~/.m2
          key: Atoperural-{{ checksum "pom.xml" }}
      
      - run: 
          name: Run maven sonar plugin
          command: |
            mvn package sonar:sonar -DskipTests  \
             -Dsonar.projectKey=es.master.portalrural:Atoperural \
             -Dsonar.organization=lastmaster \
             -Dsonar.host.url=https://sonarcloud.io \
             -Dsonar.login=9a4a00f92cc4017fb0e0cd07403146a114131f19

  deployment:
    machine:
      image: circleci/classic:201808-01
    environment:
      K8S_VERSION: v1.10.0
      KUBECONFIG: /home/circleci/.kube/config
      MINIKUBE_VERSION: v0.30.0
      MINIKUBE_WANTUPDATENOTIFICATION: false
      MINIKUBE_WANTREPORTERRORPROMPT: false
      MINIKUBE_HOME: /home/circleci
      CHANGE_MINIKUBE_NONE_USER: true
    steps:
      - checkout
      - run:
          name: setup kubectl
          command: |
            curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kubectl && chmod +x kubectl && sudo mv kubectl /usr/local/bin/
            mkdir -p ${HOME}/.kube
            touch ${HOME}/.kube/config
      - run:
          name: setup minikube
          command: |
            curl -Lo minikube https://github.com/kubernetes/minikube/releases/download/${MINIKUBE_VERSION}/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
      - run:
          name: setup helm
          command: curl https://raw.githubusercontent.com/helm/helm/master/scripts/get | bash
      - run:
          name: start minikube
          command: |
            sudo -E minikube start --vm-driver=none --cpus 2 --memory 2048 --kubernetes-version=${K8S_VERSION} &> $HOME/minikube.log 2>&1 < /dev/null
      - run:
          name: wait for minikube
          command: |
            JSONPATH='{range .items[*]}{@.metadata.name}:{range @.status.conditions[*]}{@.type}={@.status};{end}{end}';
            until kubectl get nodes -o jsonpath="$JSONPATH" 2>&1 | grep -q "Ready=True"; do
              sleep 1;
            done
      - run:
          name: fix RBAC
          command: |
            # make default account cluster-admin
            kubectl create clusterrolebinding add-on-cluster-admin --clusterrole cluster-admin --serviceaccount=kube-system:default
      - run:
          name: dump cluster-info
          command: |
            kubectl cluster-info
            kubectl get po --all-namespaces
      - run:
          name: install helm in cluster
          command: |
            kubectl -n kube-system create sa tiller
            kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
            helm init --wait --service-account tiller
      - run:
          name: deploy atoperural
          command: helm install -n atoperural helm/atoperural --set volume.enabled=false,image.tag=${CIRCLE_SHA1},mysql_image.tag=${CIRCLE_SHA1}

      - run:
          name: dump pods & services
          command: |
            # wait for all pods to start
            sleep 60
            # helm status
            helm status atoperural
            # dump pods
            kubectl get po  --all-namespaces | grep -vE '(kube-sys|docker)'
            echo
            # dump services
            kubectl get svc  --all-namespaces | grep -vE '(kube-sys|docker)'
            

  tag:
    working_directory: /app
    docker:
      - image: docker:17.05.0-ce-git
    steps:
      - setup_remote_docker
      - restore_cache:
          keys:
            - v1-{{ .Branch }}
          paths:
            - /caches/app.tar
      - run:
          name: Load Docker image layer cache
          command: |
            set +o pipefail
            docker load -i /caches/app.tar | true
      - run:
          name: Tag Docker images
          command: |
              docker login -u $DOCKER_USER -p $DOCKER_PASS
              docker tag  atoperural "jpastorr/atoperural:latest"
              docker push "jpastorr/atoperural:latest"
workflows:
  version: 2
  build_test_publish:
    jobs:
    - memoria
    - test
    - build:
        requires:
          - test
          - sonar
    - mysql:
        requires:
          - test
    - sonar
    - deployment:
        requires:
          - build
          - test
    - tag:
        requires:
          - test
~~~
