
# Dockerizar Atoperural

El objetivo de este máster es desplegar la aplicación en una plataforma cloud basada en contenedores. 

*Docker* ofrece contenedores ligeros y portables para las aplicaciones software que puedan ejecutarse en cualquier máquina con Docker instalado, independientemente del sistema operativo que la máquina tenga por debajo, facilitando así también los despliegues.

## Imagen Docker Atoperural

Atoperural es una aplicación web SpringBoot. El desarrollo inicial se hizo usando la IDE STS.

El primer paso para meter la aplicación dentro de un contenedor es que se pueda compilar desde la
consola con el comando mvn package. Para ello había que añadir dos plugins al `pom.xml` (`spring-boot-maven-plugin` y 
`maven-compiler-plugin`)

El siguiente paso es generar la imagen docker. Para ello se ha creado un multistage *Dockerfile* llamado `Dockerfile.atoperural`
donde tenemos tres fases.
En la primera se copia el código fuente y se descargan todas las dependencias de plugins y librerías [^1]  partiendo de la imagen `maven:3.5-jdk-8-alpine`.
En la segunda fase se usa la imagen anterior y es donde se compila y empaqueta el proyecto SpringBoot de *Atoperural*.
En la tercera fase, partiendo de la imagen `openjdk:8-jre-alpine` le copiamos el `jar` generado y le indicamos el comando de arranque.

[^1]: Esto lo hacemos para *cachear* las dependencias de plugins y librerías y así tarde menos la compilación. 

&nbsp;


```{caption="Dockerfile.atoperural" label=docker_atope .dockerfile}
FROM maven:3.5-jdk-8-alpine as build
COPY settings.xml /src/code/settings.xml
COPY Atoperural/pom.xml /src/code/pom.xml

WORKDIR /src/code

RUN mvn -s settings.xml 
    org.apache.maven.plugins:maven-dependency-plugin:2.8:go-offline
RUN mvn -s settings.xml dependency:resolve-plugins
#---------------------------------------------------------
FROM build as jar

COPY Atoperural /src/code
RUN mvn -s settings.xml -DskipTests package
#--------------------------------------------------------
FROM openjdk:8-jre-alpine

WORKDIR /app

COPY --from=jar /src/code/target/Atoperural-0.0.1-SNAPSHOT.jar /app
CMD java -jar Atoperural-0.0.1-SNAPSHOT.jar
```
&nbsp;

## Imagen Docker de MySql

El proyecto de desarrollo de Atoperural hace uso de una base de datos *H2* en memoria, para una solución más escalable hemos sustituido esta
base de datos por una MySQL que correrá en su propio contendedor.
En vez de tirar directamente de una imagen MySql sin modificar es crearnos una con la base de datos y usuario ya precreados.

&nbsp;

``` {caption="Dockerfile.mysql" label=docker_mysql .dockerfile}
FROM mysql:5.7.25 as builder

ENV MYSQL_ROOT_PASSWORD=secret

COPY setup.sql /docker-entrypoint-initdb.d/
```
&nbsp;

``` {caption="setup.sql" label=docker_atope .sql}
CREATE DATABASE IF NOT EXISTS atoperural_db;
CREATE USER IF NOT EXISTS 'atoperural'@'%' identified by 'secret';
GRANT ALL ON atoperural_db.* to 'atoperural'@'%';
```
&nbsp;

## Cambios en la configuración SpringBoot

Para que la aplicación se conecte con MySQL hay que añadir las siguientes properties en `application.properties`:

* **`spring.datasource.driver-class-name`**: Hay que cambiar el driver para que use el driver de MySQL Jdbc. 
* **`spring.datasource.url`**: Indicarle la URL donde está ubicada nuestra base de datos [^2].
* **`spring.datasource.username`**: El nombre de usuario que hemos creado junto con la bbdd.
* **`spring.datasource.password`**: El password de usuario que hemos creado junto con la bbdd.

Para que la página vaya sobre *https*, que necesitaremos para el single sign on (SSO) con facebook (OAuth2), hay que añadir
las siguientes propiedades:

* **`server.port`**: Ponemos internamente el 8543 para ir sobre https (en vez de 8080). Pero el servicio escuchará en el 443 (ver siguiente capitulo).
* **`server.ssl.*`**: Propiedades de nuestro certificado 
* **`security.basic.enabled`**: Desactivamos seguridad básica porque usaremos SSO.

&nbsp;

```{caption="application.properties" label=properties .bash}
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://atoperural-db:3306/atoperural_db?
		verifyServerCertificate=false&useSSL=true
spring.datasource.username=atoperural
spring.datasource.password=secret
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5Dialect
security.basic.enabled=false
server.port = 8543
server.ssl.key-store = classpath:keystore.jks
server.ssl.key-store-password = secret
server.ssl.key-password = secret
server.ssl.keyStoreType = JKS
server.ssl.keyAlias = server-alias
```

## Configuración Hazelcast

Para despliegues con más de una instancia de frontend web necesitamos una cache de sesiones que nos permita mantener
la sesión independientemente de la instancia donde caigamos. Para ello usaremos *Hazelcast* para *Kubernetes*. Estas son las dependencias *Maven* que hay que añadir al *`pom.xml`*


&nbsp;


```{caption="Dependencies Hazelcast Kubernetes en pom.xml" label=hazelcast_deps .xml}
    <dependency>
      <groupId>com.hazelcast</groupId>
      <artifactId>hazelcast</artifactId>
      <version>3.11.2</version>
    </dependency>
    <dependency>
      <groupId>com.hazelcast</groupId>
      <artifactId>hazelcast-spring</artifactId>
      <version>3.11.2</version>
    </dependency>
    <dependency>
      <groupId>com.hazelcast</groupId>
      <artifactId>hazelcast-wm</artifactId>
      <version>3.8.3</version>
    </dependency>
    <dependency>
      <groupId>com.hazelcast</groupId>
      <artifactId>hazelcast-kubernetes</artifactId>
      <version>1.4</version>
    </dependency>
    <dependency>
```

Los cambios de configuración que hay que hacer en la clase *`Application`* son los siguientes:

&nbsp;

```{caption="Application.java" label=hazelcast .java}
	@Bean
	public Config config() {
		Config config = new Config();
		JoinConfig joinConfig = config.getNetworkConfig().getJoin();
		joinConfig.getMulticastConfig().setEnabled(false);
		joinConfig.getKubernetesConfig().setEnabled(true).
			setProperty("namespace", "default");

		return config;
	}
``` 
Al tratarse de un despliegue en Kubernetes (K8s) hay que desplegar también un servicio de **hazelcast** que se expone en el puerto *5701* [ver \ref{hazelcast_service}] 

[^2]: `spring.datasource.url` apunta al servicio `atoperural-db` que veremos en el siguiente capitulo.

