# Introducción

## General
El proyecto consistirá en una aplicación web que permitirá la gestión de reservas en alojamientos rurales. Habrá tres roles, el administrador del sistema, el propietario del alojamiento y el huésped. Cada uno de estos tres roles se podrá logar en el sistema y podrá hacer las acciones correspondientes a su perfil.

Por ejemplo, el propietario podrá dar de alta un alojamiento con todos los detalles del mismo y tendrá que meter una serie de datos obligatorios para que quede grabada la propiedad. Una vez que ha introducido los datos básicos y si tiene algún tipo de modificación podrá actualizar los datos. Dentro de los datos que podrá introducir están los datos identificativos del alojamiento, licencia de turismo, características tanto interiores como exteriores, precios, descripciones, fotografías del alojamiento, ubicación, actividades en los alrededores y datos de contacto.
Además, para el tema del calendario de ocupación se le dará la posibilidad de añadir la sincronización con el servicio ical.

Estos datos sobre los alojamientos se guardarán en una base de datos, bien para mostrarlos en el portal web así como para que el propietario los actualice a su conveniencia.
El huésped podrá guardarse una lista con las casas rurales favoritas que desea ir, sus lugares preferidos y tendrá la posibilidad de dejar opiniones sobre sus estancias.
Por último, el administrador del sistema tendrá la capacidad de dar de baja las casas rurales (el propietario tendrá que solicitarlo previamente), además de dar de baja a los propietarios y a los huéspedes. Puede añadir información sobre guías de viaje al portal.

Los futuros huéspedes podrán buscar alojamientos por fechas libres, zona geográfica y capacidad del alojamiento. Una vez que se le han ofrecido los alojamientos de ésta búsqueda, podremos dejarle la opción de ordenarlos según los criterios de precio, valoración, los más relevantes o los que han actualizado el calendario más recientemente. Es recomendable que se puedes afinar la búsqueda aplicando nuevos filtros para elegir ciertas opciones como puede ser que el alojamiento tenga piscina, chimenea o que la casa se encuentre aislada.

## Objetivos

Dar un servicio web de alojamientos turísticos, que sea escalable, mantenible y robusto.

Pare ello pondremos en práctica conocimientos adquiridos durante el Máster en las siguientes áreas:

* Uso de *Spring* como *framework* para el *backend* de servidor.
* Uso de *Javascript* y *Jquery* para implementación de la lógica en el lado de cliente.
* Uso de plantilla *CSS Bootstrap*.
* Principios de arquitectura de software: arquitectura hexagonal.
* Principios de diseño orientado a objetos (*SOLID*).
* Uso de Sonar para la cobertura de código.
* Interacción con *APIs REST*.
* Verificación de software: pruebas unitarias (JUnit, Mockito, Hamcrest), pruebas de integración, pruebas de usuario (*Selenium*, *Selenium Grid*). Pirámide de *testing*.
* Uso de *Jenkins* o similar (*TravisCI*, *CircleCI*) como servidor de integración continua.
* Tecnologías *Cloud* para puesta en producción: *Docker, Kubernetes, Helm, AWS*.

## Metodología

La metodología a usar para esta aplicación Web constará de las siguientes etapas:


* Diseño Conceptual: en esta sección se abarca temas relaciones a la especificación del dominio del problema, a través de su definición y las relaciones que contrae.
* Diseño Navegacional: está enfocado en lo que respecta al acceso y forma en la que los datos son visibles.
* Diseño de la presentación o diseño de interfaz: se centra en la forma en la que la información va a ser mostrada a los usuarios, cabe mencionar en esta sección intervienen mayormente el cliente definiendo los requerimientos y lo usuarios defiendo como quieren interactuar con el sistema.
* Implantación: es la construcción del software a partir de los artefactos generados en las etapas previas.
* Pruebas o test: se comprueba la lógica de negocios aplicada en el sistema, y se verifican las entradas y salidas de datos con el fin de descubrir errores de funcionalidad, comportamiento o rendimiento.
* Evaluación de cliente: permite corregir errores gracias a las iteraciones realizadas con el fin de ir puliendo la aplicación en comparación a las iteraciones anteriores.


## Forma de trabajo

Para la forma de trabajo hemos creado un proyecto en *Bickbucket*, de *Atlassian*. Este proyecto nos ha permitido tener un repositorio privado y remoto de *git* para el versionado del código, así como de *wiki* para compartir la información que se ha ido generando en el proyecto en *MarkDown*, como se puede ver en la siguiente Figura.

![Bickbucket home](bitbucket.png)

Se puede ver, además, los últimos *jobs* de *CircleCi* asociados al proyecto a la izquierda, tal y como está señalado sobre la Figura.

*Bickbucket* nos permite también abrir *tickets* o mantener un *Board* con las tareas que el equipo va realizando y su estado. Para ello, se ha tenido que sincronizar con *Trello*. Ambas funcionalidades se pueden observar en las siguientes Figuras.

![Bickbucket issues](bitbucket.issues.png)

![Bickbucket board](board.png)
