#!/bin/bash

repoPath=`git rev-parse --show-toplevel`

# setup
docker build -t jpastorr/atoperural:latest -f ${repoPath}/docker/Dockerfile.atoperural ${repoPath}
docker build -t jpastorr/atoperural-db:latest -f ${repoPath}/docker/Dockerfile.mysql ${repoPath}
helm delete --purge atoperural
# sleep 5
helm install ${repoPath}/helm/atoperural -n atoperural

# test
sleep 10
atoperuralIP=`kubectl describe svc atoperural  | grep -w IP | tr -s ' ' | cut -d' ' -f2`
echo "www.atoperural.es IP ${atoperuralIP}"

docker build -t cgodoytrapero/atoperural-e2e -f ${repoPath}/docker/Dockerfile.e2e ${repoPath}
docker run --rm --add-host="www.atoperural.es:${atoperuralIP}" -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v ~/.Xauthority:/root/.Xauthority --network=host cgodoytrapero/atoperural-e2e

# cleanup
helm delete --purge atoperural
