#!/bin/bash

pandoc --verbose  --filter pandoc-citeproc \
  -V geometry:margin=1in \
  -V lang=es \
  --variable papersize=a4paper \
  --template eisvogel.latex \
  --from=markdown \
  --pdf-engine=xelatex \
  --listings \
  --toc --top-level-division=chapter -s *.md -o atoperural.pdf
