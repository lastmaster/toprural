CREATE DATABASE IF NOT EXISTS atoperural_db;
CREATE USER IF NOT EXISTS 'atoperural'@'%' identified by 'secret';
GRANT ALL ON atoperural_db.* to 'atoperural'@'%';